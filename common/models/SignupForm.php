<?php
namespace common\models;
use yii;
use yii\base\Model;
use common\models\User;
use yii\web\Session;
use backend\models\B2cAddress;
use backend\models\SchemeJoins;
use backend\models\SchemeOffers;
use backend\models\Packages;
use backend\models\InvestmentJoins;


/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $level;
    public $user_type;
    public $c_password;
    public $l_name;
    public $f_name;
    public $mobile;
    public $address;
    public $location;
    public $pincode;
    public $scheme;
    public $period;
    public $offer;
    public $valid_from;
    public $startup_payment;
    publiC $user_id;
    // investment input params
    public $is_investment;
    public $investment_amount;
    public $reccoring_type;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_name','l_name','password','c_password','mobile','address','pincode','valid_from'], 'required'],
            [['f_name','l_name'], 'string', 'max'=>12],
            [['level','user_type','user_id','created_by'],'safe'],
            [['email'],'email'],
            ['email', 'trim'],
            [['pincode'],'string', 'min'=>6, 'max'=>6],
            ['email', 'string', 'max' => 255],
            [['created_by','username'],'safe'],
            ['password', 'string', 'min' => 6],
            [['mobile','pincode'],'integer'],
            [['mobile'],'string', 'min'=>10,'max'=>10],
            [['plan_label','is_investment','investment_amount','reccoring_type','location'],'safe'],
            [['investment_amount','reccoring_type'],'required', 'when'=> function($model) {
                if($model->is_investment == true) {
                    return true;
                } else {
                    return false;
                }
            } ],
             [['email','period','scheme','offer','startup_payment'],'required', 'when'=> function($model) {
                if($model->is_investment != true) {
                    return true;
                } else {
                    return false;
                }
            } ],
            ['mobile', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            
            ['c_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match"],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */

    public function investment_signup() {
        if (!$this->validate()) {
            return null;
        }
        $user = new User();
        $user->username = $this->generateTempUsername();
        $user->f_name = $this->f_name;
        $user->l_name = $this->l_name;
        $user->mobile = $this->mobile;
        $user->email = $this->email;
        $user->level = $this->level;
        $user->user_type = $this->user_type;
        $user->created_by = $this->getActiveUserID();
        $user->setPassword($this->password);
        $user->generateAuthKey();

         if($user->save()) {
            $b2cAddress = new B2cAddress();
            $key = $this->SecureWay($this->c_password,$user->id);
            $user->username = yii::$app->params['codeName'].'@'.$user->id;
            
            $b2cAddress->u_id = $user->id;
            $b2cAddress->type = '0';
            $b2cAddress->address = $this->address;
            $b2cAddress->location = $this->location;
            $b2cAddress->pincode = $this->pincode;
            $b2cAddress->status = '0';
            $user->save();
            $b2cAddress->save();
            // here the investment creation

            $investmentJoin = new InvestmentJoins();
            $investmentJoin->u_id = $user->id;
            $investmentJoin->plan_amount = $this->investment_amount;
            $investmentJoin->valid_from = $this->valid_from;
            $investmentJoin->plan_label = 'Investment of '.$investmentJoin->plan_amount;
            $investmentJoin->reccoring_type = $this->reccoring_type;
            if($investmentJoin->save()) {

            } else {
                echo '<pre>'; print_r($investmentJoin->getErrors()); echo '</pre>'; die();
            }
        }
        
        return $user;

    }


    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->generateTempUsername();
        $user->f_name = $this->f_name;
        $user->l_name = $this->l_name;
        $user->mobile = $this->mobile;
        $user->email = $this->email;
        $user->level = $this->level;
        $user->user_type = $this->user_type;
        $user->created_by = $this->getActiveUserID();
        $user->setPassword($this->password);
        $user->generateAuthKey();

    
        if($user->save()) {
            $b2cAddress = new B2cAddress();
            $key = $this->SecureWay($this->c_password,$user->id);
            $user->username = yii::$app->params['codeName'].'@'.$user->id;
            
            $b2cAddress->u_id = $user->id;
            $b2cAddress->type = '0';
            $b2cAddress->address = $this->address;
            $b2cAddress->pincode = $this->pincode;
            $b2cAddress->status = '0';
            $user->save();
            $b2cAddress->save();

            $schemeJoins = new SchemeJoins();
            $schemeOffers = new SchemeOffers();
            $packages = Packages::findOne($this->scheme);
            $schemeOffer = $schemeOffers->findOne($this->offer);
            
            $schemeJoins->u_id = $user->id;
            $schemeJoins->guid = yii::$app->controller->getGuid();
            $schemeJoins->plan_amount = $packages->package_value;
            $schemeJoins->plan_label = $packages->package_label;
            $schemeJoins->duration = $this->period;
            $schemeJoins->created_at = $user->getNowTime();
            $schemeJoins->valid_from = $this->valid_from;
            if($schemeOffer->bonus_month!=null && $schemeOffer->bonus_month>0) {
                $totalPeriod = $schemeJoins->duration + $schemeOffer->bonus_month;
                $schemeJoins->valid_true = date('Y-m-d', strtotime("+".$totalPeriod." months",strtotime($schemeJoins->valid_from)));
            }
            else if ($schemeOffer->bonus_month==null || $schemeOffer->bonus_month==0) {
                $totalPeriod = $schemeJoins->duration;
                $schemeJoins->valid_true = date('Y-m-d', strtotime("+".$totalPeriod." months",strtotime($schemeJoins->valid_from)));
            }
            $schemeJoins->offer_type = $schemeOffer->offer_type;
            $schemeJoins->offer_label = $schemeOffer->offer_label;
            $schemeJoins->bonus_month = $schemeOffer->bonus_month;
            $schemeJoins->bonus_cash = $schemeOffer->bonus_cash;
            $schemeJoins->bonus_gold = $schemeOffer->bonus_gold;
            $schemeJoins->offer_valid_from = $schemeOffer->valid_from;
            $schemeJoins->offer_valid_true = $schemeOffer->valid_true;
            $schemeJoins->offer_availing_on = $schemeOffer->offer_availing_on;
            if($this->startup_payment==0) {
                $schemeJoins->startup_payment= 'paid';
            }
            else if ($this->startup_payment==1) {
                $schemeJoins->startup_payment=null;
            }
            $schemeJoins->status='active';
            $schemeJoins->save();
            


        }
        
        return $user;

    }


    public function newSchemeJoin() {

        $schemeJoins = new SchemeJoins();
        $schemeOffers = new SchemeOffers();
        $user = new User();
        $packages = Packages::findOne($this->scheme);
        $schemeOffer = $schemeOffers->findOne($this->offer);

        $schemeJoins->guid = yii::$app->controller->getGuid();
        $schemeJoins->u_id = $this->user_id;
        $schemeJoins->plan_amount = $packages->package_value;
        $schemeJoins->plan_label = $packages->package_label;
        $schemeJoins->duration = $this->period;
        $schemeJoins->created_at = $user->getNowTime();
        $schemeJoins->valid_from = $this->valid_from;
        if($schemeOffer->bonus_month!=null && $schemeOffer->bonus_month>0) {
            $totalPeriod = $schemeJoins->duration + $schemeOffer->bonus_month;
            $schemeJoins->valid_true = date('Y-m-d', strtotime("+".$totalPeriod." months",strtotime($schemeJoins->valid_from)));
        }
        else if ($schemeOffer->bonus_month==null || $schemeOffer->bonus_month==0) {
            $totalPeriod = $schemeJoins->duration;
            $schemeJoins->valid_true = date('Y-m-d', strtotime("+".$totalPeriod." months",strtotime($schemeJoins->valid_from)));
        }
        $schemeJoins->offer_type = $schemeOffer->offer_type;
        $schemeJoins->offer_label = $schemeOffer->offer_label;
        $schemeJoins->bonus_month = $schemeOffer->bonus_month;
        $schemeJoins->bonus_cash = $schemeOffer->bonus_cash;
        $schemeJoins->bonus_gold = $schemeOffer->bonus_gold;
        $schemeJoins->offer_valid_from = $schemeOffer->valid_from;
        $schemeJoins->offer_valid_true = $schemeOffer->valid_true;
        $schemeJoins->offer_availing_on = $schemeOffer->offer_availing_on;
        if($this->startup_payment==0) {
            $schemeJoins->startup_payment= 'paid';
        }
        else if ($this->startup_payment==1) {
            $schemeJoins->startup_payment=null;
        }
        $schemeJoins->status='active';
        return $schemeJoins->save() ? 'true':'false';

    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f_name' => 'First Name',
            'l_name' => 'Last Name',
            'email' => 'Email ID',
            'password' => 'Password',
            'c_password' => 'Confirm Password',
            'mobile' => 'Mobile Number',
            'pincode' => 'Pincode',
            'address' => 'Full Address',
            'scheme' => 'Joning Scheme',
            'period' => 'Scheme Period',
            'offer' => 'Offer',

        ];
    }

    // for account creator log entry
    protected function getActiveUserID() {
        
        if(isset(yii::$app->user->id)) {

            $ActiveUser = User::find()->where(['id' =>yii::$app->user->id])->one();
            $ActiveUserID = $ActiveUser->id;
            return $ActiveUserID;

        }
        
    }

    protected function generateTempUsername() {
        return rand(0, 99999999999);
    }

    protected function SecureWay($data,$userId) {
        $session = new Session;
        $session->open();
        $session['lastPasswdUser'] = ['userId' =>$userId, 'cacheP' =>$data];
        return 'success';
    }


}
