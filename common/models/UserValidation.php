<?php
namespace common\models;
use yii;
use yii\base\Model;
use common\models\User;
use yii\web\Session;
use backend\models\B2cAddress;
use backend\models\SchemeJoins;
use backend\models\SchemeOffers;
use backend\models\Packages;


/**
 * Signup form
 */
class UserValidation extends Model
{
    
    public $email;
    public $mobile;
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'],'required'],
            
            ['mobile', 'required', 'when' => function ($model) {
                                                return $model->mobile == '0';},
                                        'whenClient' => "function (attribute, value) {
                                        return $('#uservalidation-type').val() == '0'; }"
            ],
            ['mobile','string','min'=>10,'max'=>10],
            ['mobile', 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'Not a valid mobile number.'],
            [['email'],'email'],
            ['email', 'required', 'when' => function ($model) {
                                                return $model->mobile == '1';},
                                        'whenClient' => "function (attribute, value) {
                                        return $('#uservalidation-type').val() == '1'; }"
            ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function validateUser()
    {
        if (!$this->validate()) {
            return null;
        }
        else {

        return '';
        }
        
        

    }

    public function attributeLabels()
    {
        return [
            'mobile' => 'Mobile Number',
            'email'=> 'Email Account',
            'type' => 'Validation Type',

        ];
    }

    // for account creator log entry
    protected function getActiveUserID() {
        
        if(isset(yii::$app->user->id)) {

            $ActiveUser = User::find()->where(['id' =>yii::$app->user->id])->one();
            $ActiveUserID = $ActiveUser->id;
            return $ActiveUserID;

        }
        
    }

    

}
