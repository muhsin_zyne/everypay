<?php

namespace console\controllers;

use yii\console\Controller;

use Yii;
use yii\helpers\Json;
use yii\web\Response;
use backend\models\SchemeJoins;
use backend\models\BillingGenerateLog;
use backend\models\PaymentRecords;
use backend\models\PaymentOnline;
use backend\models\PaymentOffline;

use backend\models\InvestmentBillWeeklyLog;
use backend\models\InvestmentBillDailyLog;
use backend\models\InvestmentDueAccounts;
use backend\models\InvestmentJoins;
use backend\models\SmsNotifications;
use backend\models\Configurations;
/**
 * Test controller
 */
class CronController extends Controller {

	public function actionWeeklyBillGenerate() {

    $send = Yii::$app->mailer->compose(
        [
            'html' => 'test-html'
        ],
        [
            'model'=> ['data'=>'abic']
        ]
    )
    ->setTo(['aws.everypay.2019@gmail.com'])
    ->setReplyTo('muhsin@rangetech.in')
    ->setSubject('Weekly Bill Generate Cron')
    ->send();
  
        $findLatest = InvestmentBillWeeklyLog::find()
        	->where(['status'=>'success'])
        	->orderBy("exicuted_date DESC")
        	->one();
        if($findLatest) {
            $currentTime = date_create(date('Y-m-d',strtotime($this->getNowTime())));
            $lastExicution = date_create($findLatest->exicuted_date);
            $interval = date_diff($currentTime, $lastExicution);
            if($interval->d >=7 /*&& $interval->d <= 9*/) {
                $investmentObj = new InvestmentJoins();
                $loadactiveInvestments = $investmentObj->findJoins('active','week');
                foreach ($loadactiveInvestments as $key => $item) {
                    $newDue = new InvestmentDueAccounts();
                    $newDue->u_id = $item->u_id;
                    $newDue->investment_id = $item->id;
                    $newDue->amount = $item->plan_amount;
                    $newDue->payment_for = $this->getNowTime();
                    $newDue->generated_at = $this->getNowTime();
                    $newDue->status = 'pending';
                    $newDue->save();
                }
                $new = new InvestmentBillWeeklyLog();
                $new->exicuted_date = $this->getNowTime();
                $new->status = 'success';
                $new->exicuted_at = $this->getNowTime();
                $new->save();
                echo "\n \n";
            	echo "=================================================== \n";
            	echo "        Weekly Bill Cron Worked Sucessfully \n";
            	echo "=================================================== \n ";
            	echo "\n \n ";
            } else {
            	echo "\n \n";
            	echo "====================================================== \n";
            	echo "       Cron has been worked before ".$interval->d." days \n \n";
            	echo "  to change this control against please remove the logs for database.\n";
            	echo "====================================================== \n ";
            	echo "\n \n ";
            }
        }
    }

    public function actionDailyBillGenerate() {
        $findLatest = InvestmentBillDailyLog::find()
        	->where(['status'=>'success'])
        	->orderBy("exicuted_date DESC")
        	->one();
        if($findLatest) {
            $currentTime = date_create(date('Y-m-d',strtotime($this->getNowTime())));
            $lastExicution = date_create($findLatest->exicuted_date);
            $interval = date_diff($currentTime, $lastExicution);
            if($interval->d >=1 /*&& $interval->d <= 9*/) {
                $investmentObj = new InvestmentJoins();
                $loadactiveInvestments = $investmentObj->findJoins('active','day');
                foreach ($loadactiveInvestments as $key => $item) {
                    $newDue = new InvestmentDueAccounts();
                    $newDue->u_id = $item->u_id;
                    $newDue->investment_id = $item->id;
                    $newDue->amount = $item->plan_amount;
                    $newDue->payment_for = $this->getNowTime();
                    $newDue->generated_at = $this->getNowTime();
                    $newDue->status = 'pending';
                    $newDue->save();
                }
                $new = new InvestmentBillDailyLog();
                $new->exicuted_date = $this->getNowTime();
                $new->status = 'success';
                $new->exicuted_at = $this->getNowTime();
                $new->save();
                echo "\n \n";
            	echo "=================================================== \n";
            	echo "        Daily Bill Cron Worked Sucessfully \n";
            	echo "=================================================== \n ";
            	echo "\n \n ";
            } else {
            	echo "\n \n";
            	echo "====================================================== \n";
            	echo "       Cron has been worked at ".$findLatest->exicuted_date."  \n \n";
            	echo " exicution works only after a day next to the log ". date('M d, Y h:i A', strtotime($findLatest->exicuted_date)) ."\n";
            	echo "====================================================== \n ";
            	echo "\n \n ";
            }
        } else {
    		echo "\n \n";
        	echo "================================================================= \n";
        	echo "     Error: DB dose not have atleast a log record to start \n";
        	echo "================================================================= \n ";
        	echo "\n \n ";
        }
    }

    public function actionTest() {
        echo '<pre>'; print_r("Ssdd"); echo '</pre>'; die();
    }

    
    public function actionSmsSent() {
        $config = new Configurations();
        $smsCredentials = $config->loadSMSconfigurations();
        $smsData = SmsNotifications::find()
            ->where([
                'sms_type' => 'transactional',
                'current_status' => 'pending'
            ])
            ->andWhere([
                '<', 'attempt', '5'
            ])
            ->all();
        foreach ($smsData as $key => $item) {
            $ch = curl_init("http://smshorizon.co.in/api/sendsms.php?user=".$smsCredentials['username']."&apikey=".$smsCredentials['api_key']."&mobile=".$item->to_mobile."&senderid=".$smsCredentials['sender_id']."&message=".urlencode($item->content)."&type=txt"); 
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            if($output) {
                $item->refrence_id = $output;
                $item->attempt = 1;
                $item->current_status = 'sent';
            } else {
                $item->attempt = $item->attempt + 1;
            }
            if($item->save()) {

            } else {
            }
            curl_close($ch);
        }

        echo "\n \n";
        echo "================================================================= \n";
        echo "     SMS Sent  \n";
        echo "================================================================= \n ";
        echo "\n \n ";
        

    }


    public function actionPaymentGenerate() {
    	

    	$automationConf = yii::$app->params['automation'];
    	$billingDate = $automationConf['billingDate'];
    	$nowDate = $this->getNowTime();
    	$nowDateOnly = date('d', strtotime($nowDate));

    	if($nowDateOnly==$billingDate) {

            //initializing responce details
            $onlinePaymentGeneratedCount=0;
            $billingStatus = 'not found';
            $advanceBillingCount = 0;
            //./

    		$permission = BillingGenerateLog::getlogData('payment', date('Y-m-d', strtotime($nowDate)));
    		if($permission=='true') {
                // valid only on fixed billing date of each month
    			$billingSchemesArray = SchemeJoins::find()
    				->where(['<=', 'valid_from', date('Y-m-d', strtotime($nowDate))])
    				->andWhere(['>=', 'valid_true', date('Y-m-d', strtotime($nowDate))])
                    ->andWhere(['startup_payment'=>null])
                    ->orWhere(['startup_payment'=>'paid_updated'])
                    ->andWhere(['status'=>'active'])
    				->all();

    			$billingStatus = $this->bindPaymentRecords($billingSchemesArray);
                if($billingStatus!='empty') {
                    $paymentOnlinesArray = PaymentRecords::find()
                    ->where(['status'=>'pending'])
                    ->all();
                    $onlinePaymentGeneratedCount = $this->generateOnlinePayment($paymentOnlinesArray);
                }

    			$advancedBillsArray = $this->checkAdvancedPayment();
                    if(!empty($advancedBillsArray)) {
                        $advanceBillingCount = $this->generateBillConfimationforAdvacedPayment($advancedBillsArray);
                        if($advanceBillingCount>0) {
                            $billingDate = date('Y-m-d',strtotime($this->getNowTime()));
                            if($billingStatus=='empty') {
                                $this->generateBillingGenerateLog('payment','success',$billingDate);  
                            }
                        }
                    }

                
    		}

            $responce = [
                'accesPermission'=>$permission,
                'paymentBindStatus'=>$billingStatus,
                'onlinePayments' => $onlinePaymentGeneratedCount,
                'advancedBilling'=>$advanceBillingCount,
                'billingDate'=> $billingDate,
                'requstedOn' => date('d-M-Y H:i A',strtotime($this->getNowTime())),

            ];  

            $data = Json::encode($responce,false);


                    echo'<pre>';
                    print_r($data); die();
    				
    	}
    			

    }

    protected function generateBillConfimationforAdvacedPayment($advancedBillsArray) {
        $count = 0;
        foreach ($advancedBillsArray as $advancedBill) {
            $advancedBill->total_investment = $advancedBill->total_investment + $advancedBill->plan_amount;
            $advancedBill->paid_duration = $advancedBill->paid_duration + 1;
            if($advancedBill->save()) {
                $advancedBill->startup_payment='paid_updated';
                if($advancedBill->save()) {
                    $paymentRecordModel = new PaymentRecords();
                    $paymentRecordModel->guid = $this->getGuid();
                    $paymentRecordModel->u_id = $advancedBill->u_id;
                    $paymentRecordModel->scheme_join_id = $advancedBill->id;
                    $paymentRecordModel->billing_for = date('Y-m-d',strtotime($this->getNowTime()));
                    $paymentRecordModel->amount = $advancedBill->plan_amount;
                    $paymentRecordModel->created_at = $this->getNowTime();
                    $paymentRecordModel->collection_type = 'offline';
                    $paymentRecordModel->status = 'success';
                    $paymentRecordModel->is_startup_payment = 1;
                    $paymentRecordModel->periode = $advancedBill->paid_duration.'/'.$advancedBill->duration;
                    $paymentRecordModel->payment_received_at = $this->getNowTime();
                    if($paymentRecordModel->save()) {
                        $paymentOfflineModel = new PaymentOffline();
                        $paymentOfflineModel->u_id = $paymentRecordModel->u_id;
                        $paymentOfflineModel->type = 'c2b';
                        $paymentOfflineModel->payment_record_id = $paymentRecordModel->id;
                        $paymentOfflineModel->amount = $paymentRecordModel->amount;
                        $paymentOfflineModel->gst = '0.00';
                        $paymentOfflineModel->grand_total = $paymentRecordModel->amount;
                        $paymentOfflineModel->discount =  '0.00';
                        $paymentOfflineModel->collected_amount = $paymentRecordModel->amount;
                        $paymentOfflineModel->return_balance = '0.00';
                        $paymentOfflineModel->balance_due = '0.00';
                        $paymentOfflineModel->status = 'success';
                        $paymentOfflineModel->save(false); 
                        $paymentRecordModel->offline_payment_id  = $paymentOfflineModel->id;
                        $paymentRecordModel->save(); 
                        $count++;
                    }
                    else {
                        echo'<pre>';  print_r($paymentRecordModel->getErrors()); die(); 
                    }
                }

            }
            
        }
        return $count;
    } 


    protected function checkAdvancedPayment() {
        $schemeRecords = SchemeJoins::find()
            ->where(['startup_payment'=>'paid'])
            ->andWhere(['status'=>'active'])
            ->all();
        return $schemeRecords;
    }



    protected function generateOnlinePayment($paymentOnlinesArray) {
        $count=0;
    	foreach ($paymentOnlinesArray as $paymentOnlineByOne) {

    		$paymentOnlineModel = new PaymentOnline();
            $paymentOnlineModel->guid =$this->getGuid();
    		$paymentOnlineModel->type='c2b';
    		$paymentOnlineModel->u_id = $paymentOnlineByOne->u_id;
    		$paymentOnlineModel->payment_record_id = $paymentOnlineByOne->id;
    		$paymentOnlineModel->amount = $paymentOnlineByOne->amount;
    		$paymentOnlineModel->gst = $this->calculateGst($paymentOnlineByOne->amount);
    		$paymentOnlineModel->grand_total = $paymentOnlineModel->amount + $paymentOnlineModel->gst;
    		$paymentOnlineModel->status = 'pending';
    		$paymentOnlineModel->label = $this->findSchemeLabelById($paymentOnlineByOne->scheme_join_id);
    		$paymentOnlineModel->created_at = $this->getNowTime();
    		
    		if($paymentOnlineModel->save()) {
    			
    			$paymentRecordModel = PaymentRecords::findOne($paymentOnlineModel->payment_record_id);
    			$paymentRecordModel->status = 'init';
    			$paymentRecordModel->save();
                $count++;
    		}
    		
    	}
       return $count; 
    }

   

    protected function bindPaymentRecords($billingSchemesArray) {
    	
    	
    	$billingDate = date('Y-m-d',strtotime($this->getNowTime()));
    	$count = 0;
    	foreach ($billingSchemesArray as $billingSchemeByOne) {

    		if($billingSchemeByOne->paid_duration < $billingSchemeByOne->duration) {
    		    $paymentRecordModel = new PaymentRecords();
                $paymentRecordModel->guid = $this->getGuid();
    		    $paymentRecordModel->u_id = $billingSchemeByOne->u_id;
    		    $paymentRecordModel->scheme_join_id = $billingSchemeByOne->id;
    		    $paymentRecordModel->billing_for = $billingDate;
    		    $paymentRecordModel->amount = $billingSchemeByOne->plan_amount;
    		    $paymentRecordModel->created_at = $this->getNowTime();
    		    $paymentRecordModel->collection_type = 'not_set';
    		    $paymentRecordModel->status = 'pending';
    		    $paymentRecordModel->save();


    		    $count++;


    		}
    		else {
    		}
    	}
    	if($count>0) {
    		
            $countCreation = $this->generateBillingGenerateLog('payment','success',$billingDate);
    		return $countCreation;
    	}
        if($count==0) {
            return 'empty';
        }

    }

    protected function generateBillingGenerateLog($type,$status,$billingDate) {

        $billingGenerateLogModel = new BillingGenerateLog();
        $billingGenerateLogModel->exicuted_date = date('d',strtotime($billingDate));
        $billingGenerateLogModel->exicuted_month = date('m',strtotime($billingDate));
        $billingGenerateLogModel->exicuted_year = date('Y',strtotime($billingDate));
        $billingGenerateLogModel->status = $status;
        $billingGenerateLogModel->type = $type;
        $billingGenerateLogModel->exicuted_at = $this->getNowTime();
        if($billingGenerateLogModel->save()) {
            return 'success';
        }
        else {
            return 'faild';
        }
    }

     public function getNowTime() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('Y-m-d H:i:s');
        return $dateTime;
    }

    public function findSchemeLabelById($id) {
    	$schemeJoins = SchemeJoins::findOne($id);
    	return $schemeJoins->plan_label;
    }

    public function calculateGst($amount) {
    	
    	$gst = yii::$app->params['automation']['gst'];
    	$value = ($amount / 100) * $gst;
    	
    	return $value;
 	}


    public function getGuid(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                    .substr($charid, 0, 8).$hyphen
                    .substr($charid, 8, 4).$hyphen
                    .substr($charid,12, 4).$hyphen
                    .substr($charid,16, 4).$hyphen
                    .substr($charid,20,12)
                    .chr(125);// "}"
            return trim($uuid, '{}');
            //return $uuid;
        }
    }


}