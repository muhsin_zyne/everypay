<?php 
use yii\helpers\Url;
use yii\helpers\Html;

$currentPage = yii::$app->controller->id.'/'.yii::$app->controller->action->id;
$controllerId = Yii::$app->controller->id;
$actionId = Yii::$app->controller->action->id;
$requestUrl = yii::$app->request->url;

$activeUser = yii::$app->controller->getActiveUser();


?>

<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="http://admin.demo.everypay.local/drive/uploads/avatar/avatar-01.png" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<?=$activeUser->f_name.' '.$activeUser->l_name ?>
					</div>
					<div class="profile-usertitle-job">
						User
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<?= Html::a('<i class="fa fa-sign-out"></i> Logout', ['/site/logout'], ['class'=>'btn btn-primary']) ?>
					
					
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="<?php if($actionId=='index') echo'active';?>"> <?= Html::a('<i class="fa fa-home" aria-hidden="true"></i> Account Dashboard',['/my-account/index'],['class'=>'','target'=>'_self']) ?> 
						</li>
						<li class="<?php if($actionId=='pending-payment') echo'active';?>"> <?= Html::a('<i class="fa fa-money" aria-hidden="true"></i> Pending payment',['/my-account/pending-payment'],['class'=>'','target'=>'_self']) ?> 
						</li>
						<li class="<?php if($actionId=='payment-history') echo'active';?>"> <?= Html::a('<i class="fa fa-money" aria-hidden="true"></i> Payment History',['/my-account/payment-history'],['class'=>'','target'=>'_self']) ?> 
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>

<!-- <div class="my-account-menu-container">
	<div class="block-title">
		<strong><span>My Account</span></strong>
	</div>
	<ul class="nav my-account-menu">
		<li class="<?php if($actionId=='index')echo'active'?>">
			<?= Html::a('Account Dashboard',['/my-account/index'],['class'=>'','target'=>'_self']) ?>
		</li>
		<li class="<?php if($actionId=='payment-logs')echo'active'?>">
			<?= Html::a('Recent Payments',['/my-account/payment-logs'],['class'=>'','target'=>'_self']) ?>
		</li>
		<li class="<?php if($actionId=='checkout')echo'active'?>">
			<?= Html::a('Checkout',['/my-account/checkout'],['class'=>'','target'=>'_self']) ?>
		</li>
		
	</ul>
</div> -->