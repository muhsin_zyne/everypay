<?php


use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
?>
<div class="row my-account">
	<div class="col-md-3 menu-area">
		<?= $this->render('_my-account-menu',[]) ?>
	</div>
	<div class="col-md-9">
        <div class="profile-content">

      <?php if ($dataProvider->totalCount > 0) { ?>  
		<?php Pjax::begin(); ?>
		<?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{summary}\n{items}\n <div class='pull-right'>{pager}</div> <div class='clearfix'></div>",
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
            'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
        ],
        'itemOptions' => ['class' => 'item'],
        'itemView'=>'_list-pending-payment',
    	]) ?>
		<?php Pjax::end(); ?>
        <?php } else {echo'<h1 class="text-center"> <i class="fa fa-check"> </i> no data </h1>';} ?>
       

        </div>
	</div>
</div>