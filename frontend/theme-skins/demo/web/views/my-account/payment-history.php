<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\models\SchemeJoins;

/* @var $this yii\web\View */
?>
<div class="row my-account">
	<div class="col-md-3 menu-area">
		<?= $this->render('_my-account-menu',[]) ?>
	</div>
	<div class="col-md-9">
        <div class="profile-content">

       <?php Pjax::begin(); ?>    
       <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'header'=>'Scheme Name',
                'value'=>'schemeJoins.plan_label',
            ],
            [
                'header'=>'Period',
                'value'=>'periode',
            ],
            [
                'header'=>'Billing Date',
                'value'=> function($model) {
                    return date('M d, Y',strtotime($model->billing_for));
                },
            ],
            [
                'header'=>'collection type',
                'value'=>'collection_type',
            ],
            [
                'header'=>'Paid Amount',
                'value'=> function($model) {
                    return money_format('%i',$model->amount);
                },
                'contentOptions' => ['class' => 'text-right'],

            ],
            [
                'header'=>'Payment Recived on',
                'value'=>function($model) {
                    return date('d-m-Y h:i A',strtotime($model->payment_received_at));
                },
            ],
            //'amount',
            // 'created_at',
            // 'collection_type',
            // 'collection_agent_id',
            // 'collection_agent_note:ntext',
            // 'online_payment_id',
            // 'status',
            // 'payment_received_at',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?>

        </div>
	</div>
</div>