<?php 
use yii\helpers\Url;
?>


<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        
            <div class="card">
                <a href="<?=Url::to(['/schemes/detail-view','scheme_id'=>$model->id])?>">
                    <div class="header bg-cyan waves-effect waves-block">
                        <h2>
                            <?= money_format('%i',$model->plan_amount); ?> <small><?= $model->plan_label ?></small>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right" style="z-index: 999999999 !important">
                                    <li><a href="javascript:void(0);" class=" waves-effect waves-block">View</a></li>
                                    <!-- <li><a href="javascript:void(0);" class=" waves-effect waves-block">Pending Payment</a></li> -->
                                    <!-- <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                </a>
                <div class="body">
                   <div class="list-group">
                        <a href="<?=Url::to(['/schemes/detail-view','scheme_id'=>$model->id])?>" class="list-group-item waves-effect waves-block">
                            <span class="badge bg-pink"><?= $model->paid_duration.'/'.$model->duration; ?></span> Duration
                        </a>
                        <a href="javascript:void(0);" class="list-group-item">
                            <span class="badge bg-cyan"> <?= money_format('%i',$model->plan_amount) ?> </span> Scheme Amount
                        </a>
                        <a href="javascript:void(0);" class="list-group-item">
                            <span class="badge bg-teal"> <?= money_format('%i',$model->total_investment); ?> </span>Total Investment
                        </a>
                        <a href="javascript:void(0);" class="list-group-item">
                            <span class="badge bg-orange"><?= date('d-m-Y h:i A',strtotime($model->created_at)); ?></span>Joined on
                        </a>
                    </div>
                </div>
            </div>
        
    </div>

