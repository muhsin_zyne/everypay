<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SchemeJoinsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->plan_label;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
   <div class="col-xs-12 col-sm-4 ">
        <div class="scheme-summary-box">
            <div class="summary-box-header">
                <h4 class="text-center">Scheme Details </h4>
            </div>
            <div class="summary-box-content">
                <ul>
                    <li> Scheme Label<span class="pull-right"> <?=$model->plan_label?> </span> </li>
                    <li> Scheme User<span class="pull-right"> <?=$activeUser->f_name.' '.$activeUser->l_name?> </span> </li>
                    <li> Monthly Plan <span class="pull-right"> <?= money_format('%i',$model->plan_amount);  ?></span> </li>
                    <li> Total Investment <span class="pull-right"> <?= money_format('%i',$model->total_investment);  ?></span> </li>
                    <li> Duration <span class="pull-right">  <?=$model->duration?> </span> </li>
                    <li> Paid Duration <span class="pull-right">  <?=$model->paid_duration?> </span> </li>
                    <li> Valid from <span class="pull-right">  <?=date('M d, Y',strtotime($model->valid_from))?> </span> </li>
                    <li> Valid True <span class="pull-right">  <?=date('M d, Y',strtotime($model->valid_true))?> </span> </li>
                    <li> Included Offer <span class="pull-right">  <?=$model->offer_label?> </span> </li>
                    
                </ul>
            </div>
        </div>
    </div>

	<div class="col-xs-12 col-sm-8">
		<div class="highlight-box">
			<div class="box-title">
				<span> Pending Payments</span>
			</div>
			<div class="box-body">
				<ul>


			      <?php if ($dataProvider->totalCount > 0) { ?>  
					<?php Pjax::begin(); ?>
					<?= ListView::widget([
			        'dataProvider' => $dataProvider,
			        'layout' => "{summary}\n{items}\n <div class='pull-right'>{pager}</div> <div class='clearfix'></div>",
			        'pager' => [
			            'firstPageLabel' => 'First',
			            'lastPageLabel' => 'Last',
			            'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
			            'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
			        ],
			        'itemOptions' => ['class' => 'item'],
			        'itemView'=>'_list-payment',
			    	]) ?>
					<?php Pjax::end(); ?>
			        <?php } else {echo'<h1 class="text-center"> <i class="fa fa-check"> </i> no data </h1>';} ?>
			       
				</ul>
			</div>

		</div>
	</div>
	
</div>
