<?php echo'<pre>'; print_r($model); die();  ?>


<script type="text/javascript">
window.onload = function() {

var options = {
	//exportEnabled: true,
	animationEnabled: true,
	title: {
		text: "Scheme Summary"
	},

	data: [{
			type: "pie",
			startAngle: 45,
			showInLegend: "true",
			toolTipContent: "<b>{name}</b>: INR {y} (#percent%)",
			legendText: "{label}",
			indexLabel: "{label} ({y})",
			//yValueFormatString:"#,##0.#"%"",
			dataPoints: [
				{ 
					label: "Paid Amount", 
					name:'Invested Amount',
					y: 5000,
					color:"#00a65a",
				},
				{ 
					label: "Pending Payment Amount", 
					y: 3230,
					color:"#de1846",
				},
				{ 
					label: "Upcoming Payment", 
					y: 2300,
					color:"#dddddd",
				},
			]
	}]
};
$("#chartContainer").CanvasJSChart(options);

}
</script>


<div id="chartContainer" style="height: 450px; width: 100%;"></div>