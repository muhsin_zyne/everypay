<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\components\ViewFunction;


$currentInfo = yii::$app->controller->currentInfo();
$view = new ViewFunction();



/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SchemeJoinsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Scheme - '.$model->plan_label;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12 col-sm-4 pull-right">
        <div class="scheme-summary-box">
            <div class="summary-box-header">
                <h4 class="text-center"> <?= $model->plan_label?> <?= $view->bootsrapLabel('string',$model->status); ?> </h4>
            </div>
            <div class="summary-box-content">
                <ul>
                    <li> Scheme User <span class="pull-right"> <?= $activeUser->f_name.' '.$activeUser->l_name ?>  </span> </li>
                    <li> Joining Date <span class="pull-right"> <?= date('M d, Y h:i A',strtotime($model->created_at)) ?> </span> </li>
                    <li> Monthly Plan <span class="pull-right"> <?= money_format('%i',$model->plan_amount);?> </span> </li>
                    <li> Total Amount Invested  <span class="pull-right"> <?= money_format('%i',$model->total_investment);?> </span> </li>
                    <li> Duration <span class="pull-right"> <?= $model->duration.' '.$view->appendMonth($model->duration); ?> </span> </li>
                    <li> Paid Duration <span class="pull-right"> <?= $model->paid_duration.' '.$view->appendMonth($model->paid_duration); ?> </span> </li>
                    <li>Pending Payments <span class="pull-right"> <?=$pendingPayments?>  </span> </li>
                    <li>Amount due <span class="pull-right"> <?= money_format('%i',$sumOfPendingpPayment);?> </span> </li>
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-8">
        <ul class="nav nav-tabs">
            <li class="<?= $currentInfo['queryParams']['type']=='summary' ? 'active' : '' ?>"><a  href="<?=Url::to(['/schemes/detail-view','scheme_id'=>$model->guid,'type'=>'summary'])?>"> Scheme Summary </a></li>
            <li class="<?= $currentInfo['queryParams']['type']=='pending' ? 'active' : '' ?>"><a  href="<?=Url::to(['/schemes/detail-view','scheme_id'=>$model->guid,'type'=>'pending'])?>"> Pending Payments </a></li>
            <li class="<?= $currentInfo['queryParams']['type']=='success' ? 'active' : '' ?>"><a  href="<?=Url::to(['/schemes/detail-view','scheme_id'=>$model->guid,'type'=>'success'])?>"> Payment History </a></li>
        </ul>
            <?php if($currentInfo['queryParams']['type']=='summary') { ?>
                <?= $this->render('_scheme-summary',['model'=>$model])?>
            <?php } else if($currentInfo['queryParams']['type']=='pending') { ?>
                <?= $this->render('_pending-payment-list',['dataProvider'=>$dataProvider])?>
            <?php } else if($currentInfo['queryParams']['type']=='success') { ?> 
                <?= $this->render('_success-payment-list',['dataProvider'=>$dataProvider])?>
            <?php }?>
        


    </div>
</div>