<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\components\ViewFunction;

$currentInfo = yii::$app->controller->currentInfo();
$view = new ViewFunction();

?>

   <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        
        [   
            'header'    => 'Billing For',
            'attribute' =>'billing_for',
            'value'=>function($model) {
                return date('M d, Y',strtotime($model->billing_for));
            },
        ],
        [
            'header'=>'Amount',
            'attribute'=>'amount',
            'value'=>function($model) {
                 return money_format('%i',$model->amount);
            },
            'contentOptions' => ['class' => 'text-right'],
        ],

        /*[   
            'header'    => 'Issued Time',
            'attribute' =>'created_at',
            'value'=>function($model) {
                return date('M d, Y h:i A',strtotime($model->created_at));
            },
        ],*/

        [   
            'header'    => 'Paid On',
            'attribute' =>'payment_received_at',
            'value'=>function($model) {
                return date('M d, Y h:i A',strtotime($model->payment_received_at));
            },
        ],

        [   
            'header'    => 'Payment Type',
            'attribute' =>'collection_type',
        ],

        [   
            'header'    => 'Periode',
            'attribute' =>'periode',
        ],


        ['class' => 'yii\grid\ActionColumn',   
            'header'=>'Action',
            'template'=>'{view}',
            'buttons'=>[
                'view' => function($url,$model) {
                    return  Html::a('Detail View', [ '/schemes/payment-recipt','scheme_id'=> $model->schemeJoins->guid,'recipt_id'=>$model->id], [
                            'target'    =>'_blank',
                            'class'=> 'badge bg-green',
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'top',
                            'title' => 'Pay Now',
                        ]); 

                },
            ]                            
        ],
        /*[
            'header'=>'Total Investment',
            'attribute'=>'total_investment',
            'value'=>function($model) {
                 return money_format('%i',$model->total_investment);
            },
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'header'=>'Joined On',
            'value'=> function ($model) {
                    return date('d-m-Y h:i A',strtotime($model->created_at));
            },
        ],

        ['class' => 'yii\grid\ActionColumn',   
            'header'=>'Action',
            'template'=>'{view}',
            'buttons'=>[
                'view' => function($url,$model) {
                    return  Html::a('View', ['/schemes/detail-view','scheme_id'=>$model->guid], [
                            'class'=> 'btn btn-primary',
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'top',
                            'title' => 'View',
                        ]); 

                },
            ]                            
        ],*/
    ],
]); ?>