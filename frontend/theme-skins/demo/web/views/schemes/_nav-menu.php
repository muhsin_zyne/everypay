<?php 
use yii\helpers\Url;
$currentInfo = yii::$app->controller->currentInfo();
?>

<ul class="nav nav-tabs">
  <li class="<?php echo $currentInfo['queryParams']['status']=='active' ? 'active':''?>"><a  href="<?=Url::to(['/schemes/index','status'=>'active'])?>">Active Schemes</a></li>
  <li class="<?php echo $currentInfo['queryParams']['status']=='expired' ? 'active':''?>"><a  href="<?=Url::to(['/schemes/index','status'=>'expired'])?>"> Expired Schemes</a></li>
</ul>