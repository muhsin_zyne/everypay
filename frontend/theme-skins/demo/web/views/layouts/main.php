
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript" src="/themes/demo/assets/js/jquery.min.js"></script>

    <link rel="stylesheet" href="/themes/demo/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/themes/demo/assets/css/main.css">
    <!-- Normalize Style -->
    <link rel="stylesheet" href="/themes/demo/assets/css/normalize.css">
    <!-- Fonts Awesome -->
    <link rel="stylesheet" href="/themes/demo/assets/fonts/font-awesome.min.css">
    
    
    <!-- Animate CSS -->
    <link rel="stylesheet" href="/themes/demo/assets/extras/animate.css" type="text/css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/themes/demo/assets/extras/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="/themes/demo/assets/extras/owl.theme.css" type="text/css">
    <!-- Rev Slider Css -->
   
    <link rel="stylesheet" href="/themes/demo/assets/extras/nivo-lightbox.css" type="text/css">
    <!-- Slicknav Css -->
    <link rel="stylesheet" href="/themes/demo/assets/css/slicknav.css" type="text/css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="/themes/demo/assets/css/responsive.css">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="/themes/demo/assets/css/colors/everypay.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/themes/default/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/themes/default/font/flaticon.css" media="screen" />
    <script src="https://unpkg.com/vue@2.4.2/dist/vue.js"></script>

  </head>
  <body>
  <?php $this->beginBody() ?>
    <!-- Header area wrapper Starts -->
    <header id="header-wrap">
      <!-- Roof area Starts -->
      <div id="roof" class="hidden-xs">
          <div class="container">
              <!-- Wellcome Starts -->
              <div class="pull-left">
                <i class="fa fa-map-o" aria-hidden="true"></i> Sydney NSW 2052, Australia
              </div>
              <!-- Wellcome End -->

              <!-- Quick Contacts Starts -->
              <div class="quick-contacts pull-right">
                  
                  <?php if(!Yii::$app->user->isGuest) {  ?>

                  <span> <a href="<?=Url::to(['/my-account/index'])?>"><i class="fa fa-user"></i> My Account</a> </span>
                  <span> <a href="<?=Url::to(['/site/logout'])?>"><i class="fa fa-sign-out"></i> logout</a> </span>

                  <?php } ?>
                  <?php if(Yii::$app->user->isGuest) { ?>
                    <span><a href="<?=Url::to(['/site/login'])?>"><i class="fa fa-user"></i> Login</a> </span>
                  <?php } ?>
                  
              </div>
              <!-- Quick Contacts End -->
          </div>
      </div>
      <!-- Roof area End -->

      <!-- Navbar Start -->
      <nav class="navbar navbar-default main-navigation" role="navigation" data-spy="affix" data-offset-top="50">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?=Url::to(yii::$app->params['frontendUrl'])?>"><img src="/themes/demo/assets/img/logo.png" alt=""></a>
            </div>
            <!-- Brand End -->

              
            <!-- Collapse Navbar -->
            <div class="collapse navbar-collapse" id="navbar">
              <ul class="nav navbar-nav navbar-right">
                <li><a class="<?=yii::$app->controller->navigationMenu('current_page','site/index')?>" href="<?=Url::to(['/site/index'])?>"> Home <i class="fa fa-home"></i> </a>
                </li>
                <li class="dropdown dropdown-toggle">
                  <a class="<?=yii::$app->controller->navigationMenu('controller','my-account')?>" href="<?=Url::to(['/my-account/index'])?>" data-toggle="dropdown">My Account <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu">
                    <li><a class="<?=yii::$app->controller->navigationMenu('current_page','my-account/index')?>" href="<?=Url::to(['/my-account/index'])?>">Account Dashboard</a></li>    
                    <li><a class="<?=yii::$app->controller->navigationMenu('current_page','my-account/pending-payment')?>" href="<?=Url::to(['/my-account/pending-payment'])?>">Pending payment </a></li>  
                    <li><a class="<?=yii::$app->controller->navigationMenu('current_page','my-account/payment-history')?>" href="<?=Url::to(['/my-account/payment-history'])?>">Payment History </a></li>   
                  </ul>                        
                </li>
                            
                <li><a href="contact.html">Contact</a></li>
              </ul>
            </div>  
             <!-- Form for navbar search area -->
            <!-- <form class="full-search">
              <div class="container">
                <div class="row">
                  <input class="form-control" type="text" placeholder="Search">
                  <a class="close-search">
                  <span class="fa fa-times">
                  </span>
                  </a>
                </div>
              </div>
            </form> -->
            <!-- Search form ends -->   

            <!-- Mobile Menu Start -->
            <ul class="wpb-mobile-menu">
              <li>
                <a class="active" href="index.html">Home</a>
                <ul>
                  <li><a class="active" href="index.html">Home Page 1</a></li>    
                  <li><a href="index-1.html">Home Page 2</a></li>     
                </ul>                        
              </li>
              <li>
                <a href="#">Courses</a>
                <ul>
                  <li><a href="courses-list.html">Courses List</a></li>    
                  <li><a href="courses-grid.html">Courses Grid</a></li>  
                  <li><a href="courses-single.html">Single Course</a></li>   
                </ul>                        
              </li>
              <li>
                <a href="#">Pages</a>
                <ul>
                  <li><a href="about.html">About Page</a></li>                     
                  <li><a href="gallery.html">Image Gallery</a></li>
                  <li><a href="faq.html">FAQ</a></li>    
                  <li><a href="login.html">Login Page</a></li> 
                  <li><a href="single-teacher.html">Single Teacher</a></li> 
                  <li><a href="registration.html">Registration Form</a></li> 
                  <li><a href="contact.html">Contacts</a></li> 
                  <li><a href="404.html">404</a></li> 
                </ul>                        
              </li>
              <li>
                <a href="#">Events</a>
                <ul>
                  <li><a href="event-grid.html">Events Grid</a></li>                     
                  <li><a href="event.html">Single Event</a></li>
                </ul>                        
              </li> 
              <li>
                <a href="#">Blog</a>
                <ul>
                  <li><a href="blog.html">Blog - Right Sidebar</a></li>                     
                  <li><a href="blog-left-sidebar.html">Blog - Left Sidebar</a></li>
                  <li><a href="blog-full-width.html">Blog - Full Width</a></li>   
                  <li><a href="single-post.html">Blog Single Post</a></li>   
                </ul>                        
              </li>              
              <li><a href="contact.html">Contact</a></li>
            </ul>
            <!-- Mobile Menu End -->

          </div>
      </nav>
      <!-- Navbar End -->

    </header>
    <!-- Header area wrapper End -->


    <section class="layout">
      <div class="container">
           <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        
            <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
            <?= $content ?>
      </div>
    </section>

    
    <!-- NewsLetter Section End -->

    <!-- Footer Section -->
    <footer>
      <!-- Container Starts -->
      <div class="container">
        <!-- Row Starts -->
        <div class="row">
          <!-- Footer Widget Starts -->
          <!--<div class="footer-widget col-md-3 col-sm-6 col-xs-12">
            <h3 class="small-title">
              Contact Info
            </h3>
            <p>
              RangeTech Solutions
            </p>  
            <ul class="address">
                <li><i class="fa fa-home"></i> Sydney NSW 2052, Australia</li>
                <li><i class="fa fa-phone"></i> +91 80 8943 5573</li>
                <li><i class="fa fa-envelope"></i> ipay@rangetech.in </li>
            </ul>        
          </div> --> <!-- Footer Widget Ends -->
          
                 

          <!-- Footer Widget Starts -->
         <!-- <div class="footer-widget col-md-3 col-sm-6 col-xs-12">
            <h3 class="small-title">
              Our Courses
            </h3>
            <ul class="list">
              <li><a href="#">Web design</a></li>
             
            </ul>
          </div> -->
          <!-- Footer Widget Ends -->

          
        </div><!-- Row Ends -->
      </div><!-- Container Ends -->
      
      <!-- Copyright -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-6  col-sm-6">
              <p class="copyright-text">
                © All rights reserved <?=date('Y')?>
              </p>
            </div>
            <div class="col-md-6  col-sm-6">                
              <div class="bottom-social-icons pull-right">  
                <a class="facebook" target="_blank" href="https://web.facebook.com/GrayGrids"><i class="fa fa-facebook"></i></a> 
                <a class="twitter" target="_blank" href="https://twitter.com/GrayGrids"><i class="fa fa-twitter"></i></a>
                <a class="google-plus" target="_blank" href="https://plus.google.com"><i class="fa fa-google-plus"></i></a>
                <a class="linkedin" target="_blank" href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a>
              </div>            
            </div>
          </div>
        </div>
      </div>
      <!-- Copyright  End-->
      
    </footer>
    <!-- Footer Section End-->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="fa fa-arrow-up"></i>
    </a>

 

    <!-- jQuery  -->
    
    <?php $this->endBody() ?>

    <script type="text/javascript" src="/themes/demo/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/themes/demo/assets/js/jquery.mixitup.js"></script>
    <script type="text/javascript" src="/themes/demo/assets/js/nivo-lightbox.min.js"></script>
    <script type="text/javascript" src="/themes/demo/assets/js/jquery.countdown.js"></script>  
    <script type="text/javascript" src="/themes/demo/assets/js/jquery.counterup.min.js"></script>   
    <script type="text/javascript" src="/themes/demo/assets/js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="/themes/demo/assets/js/form-validator.min.js"></script>
    <script type="text/javascript" src="/themes/demo/assets/js/contact-form-script.js"></script>  
    <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
    <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
    <script type="text/javascript" src="/themes/demo/assets/js/jquery.slicknav.js"></script>
    <script src="/themes/demo/assets/js/main.js"></script>
  </body>
</html>
<?php $this->endPage() ?>