<?php
/* @var $this yii\web\View */
use bryglen\braintree\braintree;

$this->title = 'EveryPay Checkout';

?>
<script src="https://assets.braintreegateway.com/js/braintree-2.17.6.js"></script>
<div class="row">
	<div class="col-xs-12 col-sm-3">
	<?= $this->render('_my-account-menu',[]) ?>
	</div>
	<div class="col-xs-12 col-sm-9">
		<form action="/my-account/do-payment" method="POST">

		    <div id="dropin-container"></div>
		    <input type="hidden" value="orderMaths" name="orderId">
		    <input type="submit" class="btn btn-primary pull-right" value="pay">
		</form>
		<?php
			$braintree = Yii::$app->braintree;
			$clientToken = $braintree->call('ClientToken', 'generate', []);
		?>

		<script type="text/javascript">
			var clientToken ='<?php echo $clientToken ;?>';
			braintree.setup(clientToken, "dropin", {
				container: "dropin-container"
			});
		</script>
	</div>
</div>


<style>
	button#braintree-paypal-button {
	    display: none !important;
	}
</style>