<?php
use frontend\assets\MeterialAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

MeterialAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="/themes/material/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/themes/material/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="/themes/material/plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="/themes/material/css/style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    
    <?php $this->head() ?>
</head>
<body class="login-page">
<?php $this->beginBody() ?>

    <div class="login-box">
        <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?> 
        <?= $content ?>
    </div>
   
<?php $this->endBody() ?>

    <!-- Jquery Core Js -->
    <script src="/themes/material/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="/themes/material/plugins/node-waves/waves.js"></script>
    <script src="/themes/material/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/themes/material/js/admin.js"></script>
    <script src="/themes/material/js/pages/examples/sign-in.js"></script>

</body>
</html>
<?php $this->endPage() ?>
