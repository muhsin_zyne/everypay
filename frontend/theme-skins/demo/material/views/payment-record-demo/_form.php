<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentRecords */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-records-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'u_id')->textInput() ?>

    <?= $form->field($model, 'scheme_join_id')->textInput() ?>

    <?= $form->field($model, 'billing_for')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'collection_type')->dropDownList([ 'online' => 'Online', 'offline' => 'Offline', 'not_set' => 'Not set', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'collection_agent_id')->textInput() ?>

    <?= $form->field($model, 'collection_agent_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'online_payment_id')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'pending' => 'Pending', 'processing' => 'Processing', 'success' => 'Success', 'suspended' => 'Suspended', 'init' => 'Init', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'payment_received_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
