<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="logo">
    <a href="javascript:void(0);"> <?=yii::$app->params['storeName']?>  </a>
    <small> <?=yii::$app->params['storeTagline']?> </small>
</div>
<div class="card">
    <div class="body">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'fieldConfig' => ['options' => ['class' => 'input-group']],
            ]); ?>
            <div class="msg">Sign in to start your session</div>

             <?= $form->field($model, 'username',[
                'template' => '
                    <span class="input-group-addon">
                        <i class="material-icons">person</i>
                    </span>
                    <div class="form-line">
                    {input}
                    </div>
                    {error}',
                'inputOptions' => [
                    'placeholder' => 'Username',
                    'class'=>'form-control',
                    //'required' => true,
                ]])
            ?>

            <?= $form->field($model, 'password',[
                'template' => '
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                    {input}
                    </div>
                    {error}',
                'inputOptions' => [
                    'placeholder' => 'Password',
                    'class'=>'form-control',
                    //'required' => true,
                ]])->passwordInput([]);
            ?>
             <div class="row">
                <div class="col-xs-8 p-t-5">
                <?= $form->field($model, 'rememberMe', [
                    'template' => " {input} <label for='rememberme'>Remember Me</label> ",
                        'options' => [
                            'tag' => false,
                        ],
                    
                ])->checkbox(['class'=>'filled-in chk-col-pink','id' => 'rememberme'],false) ?>
                </div>
                <div class="col-xs-4">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-block bg-pink waves-effect', 'name' => 'login-button']) ?>
                
                </div>
            </div>
          
            <div class="row m-t-15 m-b--20">
                <div class="col-xs-6">
                    <a href="sign-up.html">Register Now!</a>
                </div>
                <div class="col-xs-6 align-right">
                    <a href="forgot-password.html">Forgot Password?</a>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

