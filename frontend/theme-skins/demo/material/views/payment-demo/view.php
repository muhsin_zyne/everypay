<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentOnline */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Onlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-online-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'u_id',
            'payment_record_id',
            'amount',
            'gst',
            'grand_total',
            'discount',
            'status',
            'label',
            'created_at',
            'valid_true',
            'billing_mobile',
            'billing_email:email',
            'billing_first_name',
            'billing_last_name',
            'payment_gateway',
            'transaction_id',
            'payment_collected_at',
            'card_number',
            'refund_status',
            'refund_amount',
        ],
    ]) ?>

</div>
