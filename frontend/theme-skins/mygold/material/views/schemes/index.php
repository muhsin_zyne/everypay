<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\widgets\ListView;

$this->title = 'Scheme Joins';
$this->params['breadcrumbs'][] = $this->title;
?>



 <?php if ($dataProvider->totalCount > 0) { ?>  
		<?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "<div class='card-summary'>{summary}</div>\n{items}\n <div class='clearfix'> </div> <div class='text-center'>{pager}</div> <div class='clearfix'></div>",
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
            'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
        ],
        'itemOptions' => ['class' => 'item-card-type'],
        'itemView'=>'_card-type-list',
    	]) ?>
        <?php } else {echo'<h1 class="text-center"> <i class="fa fa-check"> </i> no data </h1>';} ?>
       

<style type="text/css">
	@media screen and (max-width: 767px) {
		.item-card-type .card {
			margin-left: -30px;
			margin-right: -30px;
		}
		.card-summary .summary {
			margin-left: -16px;
			margin-top: -24px;
			margin-bottom: 5px;
		}   
	}

	a:hover, a:focus {
	    color: #23527c;
	    text-decoration: none !important;
	}
	
</style>