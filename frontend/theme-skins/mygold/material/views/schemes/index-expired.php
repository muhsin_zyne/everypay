<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SchemeJoinsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Scheme Joins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-sm-4 pull-right">
        <div class="scheme-summary-box">
            <div class="summary-box-header">
                <h4 class="text-center">Scheme Summary </h4>
            </div>
            <div class="summary-box-content">
                <ul>
                    <li> Total Scheme Investment <span class="pull-right"> <?= money_format('%i',$dashbordSummary['totalActiveInvestments']);?> </span> </li>
                    <li>Active Schemes <span class="pull-right"> <?=$dashbordSummary['activeSchemesCount']?> </span> </li>
                    <li>Pending Payments <span class="pull-right"> <?=$dashbordSummary['totalPendingPayments'] ?> </span> </li>
                    <li>Amount due <span class="pull-right"> <?= money_format('%i',$dashbordSummary['totalAmountDue']);  ?> </span> </li>
                    
                </ul>
            </div>
        </div>
    </div>

	<div class="col-xs-12 col-sm-8">
		<?= $this->render('_nav-menu') ?>
        <h5> Active Scheme list</h5>
		<div style="overflow-x: scroll;">
			<?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                    	'header'=>'Plan Label',
                    	'value' => 'plan_label',
                    ],
                    [
                        'attribute'=>'Duration',
                        'value'=>function($model) {
                             return $model->paid_duration.'/'.$model->duration;
                        },
                    ],
                    [
                    	'header'=>'Scheme Amount',
                        'attribute'=>'plan_amount',
                        'value'=>function($model) {
                             return money_format('%i',$model->plan_amount);
                        },
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                    	'header'=>'Total Investment',
                        'attribute'=>'total_investment',
                        'value'=>function($model) {
                             return money_format('%i',$model->total_investment);
                        },
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                        'header'=>'Joined On',
                        'value'=> function ($model) {
                                return date('d-m-Y h:i A',strtotime($model->created_at));
                        },
                    ],

                    ['class' => 'yii\grid\ActionColumn',   
                        'header'=>'Action',
                        'template'=>'{view}',
                        'buttons'=>[
                          	'view' => function($url,$model) {
                                return  Html::a('View', ['/schemes/view','scheme_id'=>$model->id], [
                                		'class'=> 'btn btn-primary',
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'title' => 'View',
                                    ]); 

                            },
                        ]                            
                	],
                ],
            ]); ?>
        </div>
	
	</div>
	
</div>
