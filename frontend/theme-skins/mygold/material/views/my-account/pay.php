<?php


use yii\helpers\Html;
use bryglen\braintree\braintree;
$this->title = 'EveryPay Checkout';

/* @var $this yii\web\View */
?>
<script src="https://assets.braintreegateway.com/js/braintree-2.17.6.js"></script>
<div class="row my-account">
	<div class="col-md-3 menu-area">
		<?= $this->render('_my-account-menu',[]) ?>
	</div>
	<div class="col-md-9">
        <div class="profile-content">
            <form action="/my-account/do-payment" method="POST">
                <div id="dropin-container"></div>
                <input type="hidden" value="<?=$guid?>" name="guid">
                <input type="submit" class="btn btn-primary pull-right" value="pay">
            </form>
            <?php
                $braintree = Yii::$app->braintree;
                $clientToken = $braintree->call('ClientToken', 'generate', []);
            ?>
            <script type="text/javascript">
                var clientToken ='<?php echo $clientToken ;?>';
                braintree.setup(clientToken, "dropin", {
                    container: "dropin-container"
                });
            </script>
		

        </div>
	</div>
</div>