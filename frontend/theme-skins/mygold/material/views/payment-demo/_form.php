<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentOnline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-online-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'c2b' => 'C2b', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'u_id')->textInput() ?>

    <?= $form->field($model, 'payment_record_id')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'gst')->textInput() ?>

    <?= $form->field($model, 'grand_total')->textInput() ?>

    <?= $form->field($model, 'discount')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'pending' => 'Pending', 'success' => 'Success', 'suspend' => 'Suspend', 'rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'valid_true')->textInput() ?>

    <?= $form->field($model, 'billing_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'billing_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'billing_first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'billing_last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_gateway')->dropDownList([ 'braintree' => 'Braintree', 'eWay' => 'EWay', 'ccAvenue' => 'CcAvenue', 'PayPal' => 'PayPal', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'transaction_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_collected_at')->textInput() ?>

    <?= $form->field($model, 'card_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'refund_status')->dropDownList([ 0 => '0', 'requested' => 'Requested', 'success' => 'Success', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'refund_amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
