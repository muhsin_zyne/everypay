<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="row my-account">
	<div class="col-md-3 menu-area">
		<?= $this->render('_my-account-menu',[]) ?>
	</div>
	<div class="col-md-9">
        <div class="profile-content">
		  		<div class="row">
		  			<div class="col-xs-12">
		  				<a href="<?=Url::to(['/schemes/index'])?>">
			  				<div class="info-box bg-aqua">
			  					<div class="main-box-content">
			  						<h3> <?=$dashbordSummary['activeSchemesCount']?> </h3>
			  						<span> Active Scheme Joins </span>
			  					</div>
			  					<div class="main-box-icon"> 
			  						<i class="flaticon s60 flaticon-community"></i>
			  					</div>
			  					<div class="clearfix"> </div>
			  					<?=Html::a('View Schemes',['/schemes/index'],['class'=>'box-footer text-center'])?>
			  				</div>
		  				</a>

		  				<a href="<?=Url::to(['/schemes/index'])?>">
			  				<div class="info-box bg-green">
			  					<div class="main-box-content">
			  						<h3> <?= money_format('%i',$dashbordSummary['totalActiveInvestments']);  ?> </h3>
			  						<span>Total Scheme Investment </span>
			  					</div>
			  					<div class="main-box-icon"> 
			  					<i class="flaticon s60 flaticon-savings"></i>
			  					</div>
			  					<div class="clearfix"> </div>
			  					<?=Html::a('More Details',['/schemes/index'],['class'=>'box-footer text-center'])?>
			  				</div>
			  			</a>

			  			<a href="<?=Url::to(['/my-account/pending-payment'])?>">
			  				<div class="info-box bg-yellow">
			  					<div class="main-box-content">
			  						<h3> <?=$dashbordSummary['totalPendingPayments'] ?> </h3>
			  						<span> Pending Payments </span>
			  					</div>
			  					<div class="main-box-icon"> 
			  					<i class="flaticon s60 flaticon-rectangles"></i> 
			  					</div>
			  					<div class="clearfix"> </div>
			  					<?=Html::a('View pending payments',['/my-account/pending-payment'],['class'=>'box-footer text-center'])?>
			  				</div>
		  				</a>
		  				<a href="<?=Url::to(['/my-account/pending-payment'])?>">
			  				<div class="info-box bg-red">
			  					<div class="main-box-content">
			  						<h3> <?= money_format('%i',$dashbordSummary['totalAmountDue']);  ?>  </h3>
			  						<span> Total Amount Due </span>
			  					</div>
			  					<div class="main-box-icon"> 
			  						<i class="flaticon s60 flaticon-payment-method"></i> 
			  					</div>
			  					<div class="clearfix"> </div>
			  					<?=Html::a('Pay Now',['/my-account/pending-payment'],['class'=>'box-footer text-center'])?>
			  				</div>
		  				</a>
					</div>
					<div class="col-xs-12">

					</div>
		  		</div>
        </div>
	</div>
</div>
