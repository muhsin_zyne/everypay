<?php


use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\PaymentRecords;

$paymentRecord = PaymentRecords::findOne($model->payment_record_id);


/* @var $this yii\web\View */


?>

<div class="course-list">
    <div class="courses-list-1">
        <div class="item-list">
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="thumb">
                  <img src="/img/payment.jpg" alt="">
                  <div class="courses-price">
                      <p class="years">Billing for <?=date('M d Y',strtotime($paymentRecord->billing_for))?></p>
                      <span class="price"><i class="fa fa-inr" aria-hidden="true"></i> <?= money_format('%!i', $model->grand_total)?>  </span>
                  </div>
                </div>
              </div> 
              <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="item-body">
                  <h3 class="medium-title"><a href="courses-single.html"><?=$model->label?></a></h3>
                  <div class="meta"> 
                    <span class="meta-part"><a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i>  Valid From  <?=$paymentRecord->created_at ?></a></span>
                   <!--  <span class="meta-part"><a href="#"><i class="fa fa-group"></i> 100</a></span>
                    <span class="meta-part"><a href="#"><i class="fa fa-commenting"></i> 5</a></span> -->
                  </div>
                  <h2> <i class="fa fa-inr" aria-hidden="true"></i> <?= money_format('%!i', $model->grand_total)?></h2>
                <form method="POST" action="/my-account/pay">
                    <input type="hidden" value="<?=$model->guid?>" name="guid">
                    <button class="btn btn-common btn-sm pull-right" onclick ="this.form.submit()"> Pay Now </button>
                </form>
       


                  
                </div>
              </div>
        </div>
    </div>
</div>

<hr>
