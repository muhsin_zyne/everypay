<?php 
use backend\models\PaymentOnline;

$paymentOnlineArray = PaymentOnline::find()
	->where(['payment_record_id'=>$model->id])->one();

?>

<li>
	<div class="payment-list">
		<div class="payment-details">
			<h5> <?=$paymentOnlineArray->label ?> <sapn class="pull-right"> <?=  money_format('%i',$paymentOnlineArray->grand_total); ?>	 </sapn></h5>
			<p>Issued on : <?= date('M d, Y h:i A',strtotime($model->created_at)) ?> <!-- Payment Due : <?=  date('M d, Y h:i A',strtotime("+1 months", strtotime($model->created_at))) ?>  --></p>
		
		</div>
		<div class="payment-action">
			<form method="POST" action="/my-account/pay">
                    <input type="hidden" value="<?=$paymentOnlineArray->guid?>" name="guid">
                    <button class="btn btn-common btn-sm pull-right" onclick ="this.form.submit()"> Pay Now </button>
                </form>
		</div>
	</div>
	
	<div class="clearfix"> </div>
</li>