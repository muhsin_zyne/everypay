<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentRecordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Records';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-records-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Records', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'u_id',
            'scheme_join_id',
            'billing_for',
            'amount',
            // 'created_at',
            // 'collection_type',
            // 'collection_agent_id',
            // 'collection_agent_note:ntext',
            // 'online_payment_id',
            // 'status',
            // 'payment_received_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
