<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\PaymentRecords */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-records-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'u_id',
            'scheme_join_id',
            'billing_for',
            'amount',
            'created_at',
            'collection_type',
            'collection_agent_id',
            'collection_agent_note:ntext',
            'online_payment_id',
            'status',
            'payment_received_at',
        ],
    ]) ?>

</div>
