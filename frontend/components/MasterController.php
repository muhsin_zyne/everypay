<?php
namespace frontend\components;
use yii;
use backend\models\User;
use yii\web\Controller;

class MasterController extends Controller
{      

    public $storePath;
    public $activeUser;
    
    public $guestPermission = [
        'site/index',
        'site/login',
        'site/contanct'
    ];
    
    public function beforeAction($event) {
        $currentInfo = $this->currentInfo();
        if(yii::$app->user->isGuest) {
            if(in_array($currentInfo['currentPage'], $this->guestPermission)) {
                if($currentInfo['currentPage']=='site/index' && isMobileDevice()==true) {
                    return $this->redirect(['site/login','return'=>\Yii::$app->request->url]);   
                }
            } else {
                return $this->redirect(['site/login','return'=>\Yii::$app->request->url]);
            }
        }
        return parent::beforeAction($event);
    }

    public function getActiveUser() {
        if(isset(yii::$app->user->id)) {
            $activeUser = User::find()->where(['id' =>yii::$app->user->id])->one();
            return $activeUser;
        }
        else {
            $activeUser = new User;
            return $activeUser;
        }
    }
    
    public function getGuid(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                    .substr($charid, 0, 8).$hyphen
                    .substr($charid, 8, 4).$hyphen
                    .substr($charid,12, 4).$hyphen
                    .substr($charid,16, 4).$hyphen
                    .substr($charid,20,12)
                    .chr(125);// "}"
            return trim($uuid, '{}');
            
        }
    }

    public function getNowTime() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('Y-m-d H:i:s');
        return $dateTime;
    }


    public function navigationMenu($type,$data) {
        $currentInfo = $this->currentInfo();
        if($type=='current_page') {
            if($currentInfo['currentPage']==$data) {
                return 'active';
            }
        }
        else if($type=='controller') {
            if($currentInfo['controlerId']==$data) {
                return 'active';
            }
        }
    }
    
    public function getLayout() {
        $layoutConf = yii::$app->params['layoutConf'];
        return $layoutConf['currentLayout'];
    }

    public function currentInfo() {
        $controlerId = yii::$app->controller->id;
        $actionId = yii::$app->controller->action->id;
        $currentPage = $controlerId.'/'.$actionId;
        $currentUrl = $_SERVER['REQUEST_URI'];
        $queryParams = yii::$app->request->queryParams;

        $responce = [
            'controlerId'   =>  $controlerId,
            'actionId'      =>  $actionId,
            'currentPage'   =>  $currentPage,
            'currentUrl'    =>  $currentUrl,
            'queryParams'   =>  $queryParams,
        ];
        return $responce;
    }

}
