<?php

namespace frontend\components;
use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class MaterialMenu extends Widget{
    public $menu_type;
    public $currentInfo;
    public $mainMenus;
    
    public function init() {

        $this->currentInfo = yii::$app->controller->currentInfo();
        $this->mainMenus = $this->menuBuild();
        if($this->menu_type === null) {
            $this->menu_type ='main';
        }
        parent::init();
        
    }
    
    public function run(){
        $result = $this->printMenu();
        echo $result;
    }

    public function printMenu() {
        if($this->menu_type=='main') {
            $data = $this->formatMenu($this->mainMenus); 
        }
        return $data;
    }

    public function formatMenu($menuItems) {
        $html = '<div class="menu">';
        $html .='<ul class="list">';
        $html .='<li class="header">MAIN NAVIGATION</li>';
            foreach ($menuItems as $key => $item) {
                
                $visiblePermission = false;
                if(yii::$app->user->isGuest) {
                    if($item['visible']=='user') {
                        $visiblePermission = false;
                    }
                    else if ($item['visible']=='all' || $item['visible']=='guest') {
                        $visiblePermission = true;
                    }
                }
                else {
                    if($item['visible']=='guest') {
                        $visiblePermission = false;
                    }
                    else {
                        $visiblePermission = true;
                    }
                }

                if($visiblePermission==false) {
                    continue;
                }

                $html .= '<li class="'.$this->is_active($item['active']).'">';
                    
                if($item['items']==null) {
                    
                    if(preg_match('/http/', $item['urlTo'])) {
                        $html .= Html::a('<i class="material-icons">'.$item['icon'].'</i> <span> '.$item['label'].' </span', $item['items']!=null ? 'javascript:void(0);' : $item['urlTo'], ['class'=> $item['items']!=null ? 'menu-toggle waves-effect waves-block' : '','target'=>$item['target']]);
                    }
                    else {
                        $html .= Html::a('<i class="material-icons">'.$item['icon'].'</i> <span> '.$item['label'].' </span', $item['items']!=null ? 'javascript:void(0);': Yii::$app->params['frontendUrl'].'/'.$item['urlTo'],['class'=> $item['items']!=null ? 'menu-toggle waves-effect waves-block' : '','target'=>$item['target']]);
                    }
            
                }
                else {
                
                    $html .='<a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                            <i class="material-icons"> '.$item['icon'].' </i>
                            <span>'.$item['label'].'</span>
                        </a>
                        <ul class="ml-menu" style="display: none;">';

                        foreach ($item['items'] as $subKey => $subMenu) {
                            $html .= '<li class="'.$this->is_active($subMenu['active']).'">';
                            if(preg_match('/http/', $item['urlTo'])) {
                                $html .= Html::a($subMenu['label'], $subMenu['urlTo'], ['class'=>'waves-effect waves-block','target'=>$item['target']]);
                            }
                            else {
                                $html .= Html::a($subMenu['label'], Yii::$app->params['frontendUrl'].'/'.$item['urlTo'],['class'=> 'waves-effect waves-block','target'=>$item['target']]);
                            }
                            $html .= '</li>';
                        }
                        
                        $html .='</ul>';
                   
                }
                       
                
                $html .= '</li>';
                
            }
        $html .='</ul>';
        $html .='</div>';

                    
        return $html;
    }

    public function is_active($flag) {
        $responce = '';
        if($flag==true) {
            $responce = 'active';
        }
        return $responce;
    }

    public function findActive($key,$array) {
        if(in_array($key, $array)) {
            return true;
        }
        else {
            return false;
        }
    }

    public function menuBuild() {
        $mainMenus = [
            [
                'label'     =>  'Home',
                'icon'      =>  'home',
                'urlTo'     =>  'site/index',
                'target'    =>  '_self',
                'visible'   =>  'all',
                'active'    =>  in_array($this->currentInfo['currentPage'], ['site/index']) ? true : false,
                'items'     =>  [

                                ],
            ],

            [
                'label'     =>  'Login',
                'icon'      =>  'home',
                'urlTo'     =>  'site/login',
                'target'    =>  '_self',
                'visible'   =>  'guest',
                'active'    =>  in_array($this->currentInfo['currentPage'], ['site/login']) ? true : false,
                'items'     =>  [

                                ],
            ],

            [
                'label'     =>  'My Account',
                'icon'      =>  'home',
                'urlTo'     =>  'my-account/index',
                'target'    =>  '_self',
                'visible'   =>  'user',
                'active'    =>  in_array($this->currentInfo['currentPage'], ['my-account/index']) ? true : false,
                'items'     =>  [

                                ],
            ],
            [
                'label'     =>  'Schemes List',
                'icon'      =>  'home',
                'urlTo'     =>  'schemes/index',
                'target'    =>  '_self',
                'visible'   =>  'user',
                'active'    =>  in_array($this->currentInfo['controlerId'], ['schemes']) ? true : false,
                'items'     =>  [
                                    [
                                        'label'     =>  'Active Scheme List',
                                        'icon'      =>  'home',
                                        'urlTo'     =>  'scheme/index',
                                        'target'    =>  '_self',
                                        'active'    =>  in_array($this->currentInfo['currentPage'], ['schemes/index']) ? true : false,
                                        'items'     =>  [

                                                        ],
                                    ],
                                ],
            ],
            [
                'label'     =>  'Dashbord',
                'icon'      =>  'text_fields',
                'urlTo'     =>  'https://www.google.com/',
                'target'    =>  '_blank',
                'visible'   =>  'user',
                'active'    =>  false,
                'items'     =>  [

                                    [
                                        'label'     =>  'Dashbord',
                                        'icon'      =>  'text_fields',
                                        'urlTo'     =>  '#',
                                        'target'    =>  '_self',
                                        'active'    =>  false,
                                        'items'     =>  [],
                                    ],
                                    [
                                        'label'     =>  'Dashbord',
                                        'icon'      =>  'text_fields',
                                        'urlTo'     =>  '#',
                                        'target'    =>  '_self',
                                        'active'    =>  false,
                                        'items'     =>  [],
                                    ],
                                        
                                ],
            ],
            [
                'label'     =>  'Logout',
                'icon'      =>  'home',
                'urlTo'     =>  'site/logout',
                'target'    =>  '_self',
                'visible'   =>  'user',
                'active'    =>  false,
                'items'     =>  [],
            ],
        ];
    
        return $mainMenus;

    }







}

?>