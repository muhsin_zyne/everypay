<?php

namespace frontend\controllers;

use yii;
use frontend\models\search\PaymentOnlineSearch;
use backend\models\PaymentOnline;
use backend\models\PaymentRecords;
use backend\models\search\PaymentRecordsSearch;
use backend\models\SchemeJoins;
use backend\models\B2cAddress;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\controllers\SiteController;
use frontend\components\MasterController;

class MyAccountController extends MasterController
{	
	
    public function init() {
        $this->layout = $this->getLayout();
        $this->enableCsrfValidation = false;
    }

    public function actionIndex() { 
        $activeUser = $this->getActiveUser();
        $schemeJoinsModel = new SchemeJoins();
        $paymentRecordsModel = new PaymentRecords();
        $activeSchemes = SchemeJoins::find()
            ->where(['status'=>'active'])
            ->andWhere(['u_id'=>$activeUser->id])
            ->all();

        $activeSchemesCount = count($activeSchemes);
        $totalActiveInvestments = $schemeJoinsModel->getSumofInvestments('active',$activeUser->id);
        $totalPendingPayments = $paymentRecordsModel->getAllCountOfpayment('init',$activeUser->id);
        $totalAmountDue = $paymentRecordsModel->getAllSumOfPaymentRecords('init',$activeUser->id);

        $dashbordSummary = [
            'activeSchemesCount'        =>  $activeSchemesCount,
            'totalActiveInvestments'    =>  $totalActiveInvestments,
            'totalPendingPayments'      =>  $totalPendingPayments,
            'totalAmountDue'            =>  $totalAmountDue,
        ];
        
        return $this->render('index',[
            'dashbordSummary'=>$dashbordSummary,
        ]);
    }

    public function actionPendingPayment() {
        $activeUser = $this->getActiveUser();
        if($activeUser==null) {
            return null;
        }
        $searchModel = new PaymentOnlineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['u_id'=>$activeUser->id,'status'=>'pending']);

        return $this->render('pending-payment', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


       // return $this->render('pending-payment');
    }

    public function actionPaymentHistory() {

        $activeUser = $this->getActiveUser();
        $searchModel = new PaymentRecordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status'=>'success','u_id'=>$activeUser->id]);

        return $this->render('payment-history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    
    public function actionPay() {

        if(Yii::$app->request->post()) {
            $guid = $_POST['guid'];
            return $this->render('pay',['guid'=>$guid]);
        }
        else {
            return $this->redirect(['/my-account/index']);
        }
    }



    public function actionCheckout() {
                echo'<pre>';
                print_r($_POST); die();
    	return $this->render('checkout');
    }

    public function actionDoPayment() {
        
        if(Yii::$app->request->post()) {

            $guid = $_POST['guid'];
            $paymentOnlineModel = PaymentOnline::find()->where(['guid'=>$guid])->one();
            
            if($paymentOnlineModel->status!='success') {
                $braintree = Yii::$app->braintree;
                $amount = $paymentOnlineModel->grand_total;

                $userModal = $this->getActiveUser();
                $userModalAddress = B2cAddress::find()->where(['u_id'=>$userModal->id])->one();


                $response = $braintree->call('Transaction', 'sale', [
                    'amount' => $amount,
                    'orderId' => $paymentOnlineModel->payment_record_id,
                    'billing'=> [
                        'firstName'=>$userModal->f_name,
                        'lastName'=> $userModal->l_name,
                        'company'=> '',
                        'streetAddress'=>'',
                        'extendedAddress'=>$userModalAddress->address,
                        'locality'=>'',
                        'region'=>'',
                        'postalCode'=> $userModalAddress->pincode,
                        'countryName'=>'',
                    ],
                    'paymentMethodNonce' => $_POST["payment_method_nonce"],
                    'options' => [
                        'submitForSettlement' => True
                    ],
                ]);

                if($response->success) {
                            
                    $paymentOnlineModel->status='success';
                    $paymentOnlineModel->billing_mobile = $userModal->mobile;
                    $paymentOnlineModel->billing_email = $userModal->email;
                    $paymentOnlineModel->billing_first_name = $response->transaction->billing['firstName'];
                    $paymentOnlineModel->billing_last_name = $response->transaction->billing['lastName']; 
                    $paymentOnlineModel->payment_gateway = 'braintree';
                    $paymentOnlineModel->transaction_id = $response->transaction->id;
                    $paymentOnlineModel->payment_collected_at = $this->getNowTime();
                    $paymentOnlineModel->card_number = $response->transaction->creditCardDetails->maskedNumber;
                    $paymentOnlineModel->refund_status = '0';
                    if($paymentOnlineModel->save()) {
                        $paymentRecordModel = PaymentRecords::findOne($paymentOnlineModel->payment_record_id);   
                        $paymentRecordModel->collection_type ='online';
                        $paymentRecordModel->collection_agent_id = null;
                        $paymentRecordModel->collection_agent_note = null;
                        $paymentRecordModel->online_payment_id = $paymentOnlineModel->id;
                        $paymentRecordModel->status ='success';
                        $paymentRecordModel->payment_received_at = $paymentOnlineModel->payment_collected_at;
                        
                        if($paymentRecordModel->save()) {
                            $schemeJoinModel = SchemeJoins::findOne($paymentRecordModel->scheme_join_id);
                            $schemeJoinModel->total_investment = $schemeJoinModel->total_investment + $schemeJoinModel->plan_amount;
                            $schemeJoinModel->paid_duration = $schemeJoinModel->paid_duration+1;
                            if($schemeJoinModel->save()) {
                                $paymentRecordModel->periode = $schemeJoinModel->paid_duration.'/'.$schemeJoinModel->duration;
                                $paymentRecordModel->save();
                            }
                            
                        }
                        Yii::$app->session->setFlash('success',' payment has been Created !');
                    }
                    

                    
                    return $this->redirect(['/my-account/index']);
                }
                else {
                    die("payment fild");
                }


            }
        
        }  
    	
    }

}
