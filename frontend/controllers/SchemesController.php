<?php

namespace frontend\controllers;

use yii;
use frontend\models\search\PaymentOnlineSearch;
use backend\models\search\SchemeJoinsSearch;
use backend\models\PaymentOnline;
use backend\models\PaymentRecords;
use backend\models\search\PaymentRecordsSearch;
use backend\models\SchemeJoins;
use backend\models\B2cAddress;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\components\MasterController;
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;
use backend\components\CustomPdfRender;

class SchemesController extends MasterController
{   
    public function init() {
        $this->layout = $this->getLayout();
        $this->storePath = yii::$app->params['storePath'];
        $this->activeUser = $this->getActiveUser();

    }

    

    public function actionIndex($status = Null) { 
        if($status==Null) {
            return $this->redirect(['schemes/index','status'=>'active']);
        }
        $activeUser = $this->getActiveUser();
        $schemeJoinsModel = new SchemeJoins();
        $paymentRecordsModel = new PaymentRecords();
        $activeSchemes = SchemeJoins::find()
            ->where(['status'=>$status])
            ->andWhere(['u_id'=>$activeUser->id])
            ->all();
        $activeSchemesCount = count($activeSchemes);
        $totalActiveInvestments = $schemeJoinsModel->getSumofInvestments('active',$activeUser->id);
        $totalPendingPayments = $paymentRecordsModel->getAllCountOfpayment('init',$activeUser->id);
        $totalAmountDue = $paymentRecordsModel->getAllSumOfPaymentRecords('init',$activeUser->id);

        $dashbordSummary = [
            'activeSchemesCount'        =>  $activeSchemesCount,
            'totalActiveInvestments'    =>  $totalActiveInvestments,
            'totalPendingPayments'      =>  $totalPendingPayments,
            'totalAmountDue'            =>  $totalAmountDue,
        ];

        $searchModel = new SchemeJoinsSearch();
        $dataProvider = $searchModel->seachForWeb();
        $dataProvider->query->andWhere([
            'u_id'      => $activeUser->id,
            'status'    => $status,
        ]);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);



        return $this->render('index',[
    		'dashbordSummary'=>$dashbordSummary,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    public function actionPaymentRecipt($scheme_id,$recipt_id) {

        $customPdfRender = new CustomPdfRender();
        $schemeModel = SchemeJoins::find()
            ->where(['guid'=>$scheme_id])
            ->where(['u_id'=>$this->activeUser->id])
            ->one();
        if(empty($schemeModel)) {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }
        $paymentRecord = PaymentRecords::find()
            ->where(['scheme_join_id'=>$schemeModel->id])
            ->where(['u_id'=>$this->activeUser->id])
            ->where(['id'=>$recipt_id])->one();
        if(empty($paymentRecord)) {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }
        $reciptTemplete = '../../'.$this->storePath.'/documents/templetes/payment_recipt_'.$paymentRecord->collection_type.'.pdf';
        $loaderContent = $customPdfRender->loadContentToRecipt($paymentRecord);
        $pdf = new Fpdi();
        $pageCount = $pdf->setSourceFile($reciptTemplete);
        for($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
               // import our page
               $templateId = $pdf->importPage($pageNo);
               $pdf->AddPage();
               $pdf->useTemplate($templateId, ['w'=>200, 'h'=>200]);
               $pdf = $customPdfRender->generatePdfConents($pdf,$loaderContent);
        }
        $pdf->Output();

    }

    public function actionPdfTest() {
        $pdf = new Fpdi();
        $pageCount = $pdf->setSourceFile('../../'.$this->storePath.'/documents/templetes/payment_recipt_offline.pdf');
        
        for($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
               // import our page
               $templateId = $pdf->importPage($pageNo);
               // Add our new page
               $pdf->AddPage();

               $pdf->useTemplate($templateId, ['w'=>200, 'h'=>200]);
               $pdf->SetFont('Helvetica');
               $pdf->SetXY(0, 0);
               $pdf->setFontSize(10);
                $pdf->MultiCell(0,5, 'This is just a simple text SDSJ SJDSJ DSJDSJ SDJSDJSJ DJSDJ SDSJDSJD SJDSJ JDSJ SJDSD This is just a simple text SDSJ SJDThis is just a simple text SDSJ SJDThis is just a simple text SDSJ SJDThis is just a simple text SDSJ SJDThis is just a simple text SDSJ SJD ');
               $pdf->Image('img/mozaic-banner-7.jpg', 0, 10); // For writing an image
          }
        $pdf->Output();

    }

    public function actionDetailView($scheme_id, $type = Null) {    
        if($type==Null) {
            return $this->redirect(['schemes/detail-view','scheme_id'=>$scheme_id,'type'=>'summary']);
        }

        
        //$this->layout = 'material';
        $activeUser = $this->getActiveUser();
        $schemeModel= SchemeJoins::find()
            ->where(['u_id'=>$activeUser->id,'guid'=>$scheme_id,])
            ->one();
        if(empty($schemeModel)) {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        } 
        else {
            $searchModel = new PaymentRecordsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $dataProvider->query->andWhere([
                'u_id'              =>  $activeUser->id,
                'scheme_join_id'    =>  $schemeModel->id,
            ]);
            if($type=='pending') {
                $dataProvider->query->andWhere([
                    'collection_type'   =>'not_set',
                ]);
            }
            else if($type=='success') {
                 $dataProvider->query->andWhere([
                    'status'=>'success',
                ])->orderBy(['id' => SORT_DESC]);
    
            }
            
        }

        return $this->render('detail-view',[
            'model'                 =>  $schemeModel,
            'activeUser'            =>  $activeUser,
            'searchModel'           =>  $searchModel,
            'dataProvider'          =>  $dataProvider,
            'pendingPayments'       =>  $searchModel->pendingPaymentCount($activeUser->id,$schemeModel->id),
            'sumOfPendingpPayment'  =>  $searchModel->sumOfPendingPayment($activeUser->id,$schemeModel->id),
        ]);
    }


   


   

}
