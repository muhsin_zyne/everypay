<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PaymentOnline;

/**
 * PaymentOnlineSearch represents the model behind the search form about `backend\models\PaymentOnline`.
 */
class PaymentOnlineSearch extends PaymentOnline
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'u_id', 'payment_record_id'], 'integer'],
            [['type', 'status', 'label', 'created_at', 'valid_true', 'billing_mobile', 'billing_email', 'billing_first_name', 'billing_last_name', 'payment_gateway', 'transaction_id', 'payment_collected_at', 'card_number', 'refund_status'], 'safe'],
            [['amount', 'gst', 'grand_total', 'discount', 'refund_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentOnline::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 4,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'u_id' => $this->u_id,
            'payment_record_id' => $this->payment_record_id,
            'amount' => $this->amount,
            'gst' => $this->gst,
            'grand_total' => $this->grand_total,
            'discount' => $this->discount,
            'created_at' => $this->created_at,
            'valid_true' => $this->valid_true,
            'payment_collected_at' => $this->payment_collected_at,
            'refund_amount' => $this->refund_amount,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'billing_mobile', $this->billing_mobile])
            ->andFilterWhere(['like', 'billing_email', $this->billing_email])
            ->andFilterWhere(['like', 'billing_first_name', $this->billing_first_name])
            ->andFilterWhere(['like', 'billing_last_name', $this->billing_last_name])
            ->andFilterWhere(['like', 'payment_gateway', $this->payment_gateway])
            ->andFilterWhere(['like', 'transaction_id', $this->transaction_id])
            ->andFilterWhere(['like', 'card_number', $this->card_number])
            ->andFilterWhere(['like', 'refund_status', $this->refund_status]);

        return $dataProvider;
    }
}
