<?php
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Origin: *');

error_reporting(E_ALL);
ini_set('display_errors', '1');

$config_loader =  config_loader();
if(!file_exists(__DIR__ . '/../common/config/'.$config_loader)) {
	// if not a valid configuration found exception for loader
	$config_loader = 'main-local.php';
}
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = yii\helpers\ArrayHelper::merge(
	require(__DIR__ . '/../common/config/main.php'),
    require(__DIR__ . '/../common/config/'.$config_loader),
    require(__DIR__ . '/config/api-config.php')

);

require(__DIR__.'/config/aliases.php');

(new yii\web\Application($config))->run();


function config_loader() {
	$ignore_naming = ["api."];
	$config_loader =  $_SERVER['HTTP_HOST'].'-config.php';
	foreach ($ignore_naming as $key => $string) {
		$config_loader = str_replace($string, "", $config_loader);
	}
	return $config_loader;
}