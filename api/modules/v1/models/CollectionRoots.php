<?php

namespace api\modules\v1\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "collection_roots".
 *
 * @property integer $id
 */
class CollectionRoots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection_roots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['root_name','status'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    public function getList() {
        $query = new Query;
        $query  ->select([
                    'c.*'
                ])  
                ->from('collection_roots as c')
                ->where(['c.status'=>'1']);

        $command = $query->createCommand();
        
        $data = $command->queryAll();
        return $data;
    }
}
