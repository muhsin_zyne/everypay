<?php

namespace api\modules\v1\models;

use Yii;
use api\modules\v1\models\User;
use api\modules\v1\models\SmsConfigurations;
/**
 * This is the model class for table "sms_notifications".
 *
 * @property integer $id
 * @property string $to_mobile
 * @property string $sms_type
 * @property string $content
 * @property string $requested_at
 * @property string $category
 * @property string $attempt
 * @property string $current_status
 * @property string $refrence_id
 */
class SmsNotifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_mobile', 'sms_type', 'content', 'category', 'attempt', 'current_status'], 'required'],
            [['sms_type', 'content', 'category', 'attempt', 'current_status'], 'string'],
            [['requested_at'], 'safe'],
            [['to_mobile'], 'string', 'max' => 12],
            [['refrence_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'to_mobile' => 'To Mobile',
            'sms_type' => 'Sms Type',
            'content' => 'Content',
            'requested_at' => 'Requested At',
            'category' => 'Category',
            'attempt' => 'Attempt',
            'current_status' => 'Current Status',
            'refrence_id' => 'Refrence ID',
        ];
    }

    public function getCustomerDetails($customer_id) {
        $customerDetails = User::find()
            ->where([
                'level'=> 4,
                'id' => $customer_id
            ])
            ->one();
        return $customerDetails;

    }

    public function formatSMSnotfication($typeId = null, $model) {
        $templateContent = '';
        // type 1 = investment payment recipt
        if ($typeId==1) {
            $templateLoader = SmsConfigurations::findOne($typeId);
            if ($templateLoader->templateModel) {
                $templateContent = $templateLoader->templateModel->content;
                $templateContent = $this->loadInputIntoTemplate($templateContent, [
                    'CustomerName' => $model->userModel->f_name.' '. $model->userModel->l_name,
                    'Amount' => money_format('%i',$model->amount),
                    'Month' => date('M d, Y', strtotime($model->clearance_at)),
                    'PlanLabel' => $model->investmentDueAccModel->investmentModel->plan_label,
                    'NameOfExicutive' => $model->agentModel->f_name. ' '. $model->agentModel->l_name,
                    'ClearenceDateTime' => date('M d, Y H:i A',strtotime($model->clearance_at)),
                ]);
            }
        }
        return $templateContent;
    }

    protected function loadInputIntoTemplate($templateContent, $replaceRequest) {
        foreach ($replaceRequest as $key => $value) {
            $templateContent = str_replace("[:".$key."]", $value, $templateContent);
        }
        return $templateContent;
    }
}
