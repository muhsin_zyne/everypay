<?php

namespace api\modules\v1\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "appointment_schedules".
 *
 * @property integer $id
 * @property integer $u_id
 * @property integer $agent_id
 * @property string $scheduled_for
 * @property string $type
 * @property integer $investment_id
 * @property integer $scheme_join_id
 * @property integer $investment_due_account_id
 * @property string $status
 * @property string $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $completed_at
 * @property integer $completed_by
 */
class AppointmentSchedules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointment_schedules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'agent_id', 'scheduled_for', 'type', 'status', 'created_at', 'created_by'], 'required'],
            [['u_id', 'agent_id', 'investment_id', 'investment_due_account_id', 'created_by'], 'integer'],
            [['scheduled_for', 'created_at', 'updated_at', 'completed_at','scheme_join_id','updated_by','completed_at','completed_by'], 'safe'],
            [['type', 'status', 'is_deleted'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'agent_id' => 'Agent ID',
            'scheduled_for' => 'Scheduled For',
            'type' => 'Type',
            'investment_id' => 'Investment Join ID',
            'scheme_join_id' => 'Scheme Join ID',
            'investment_due_account_id' => 'Investment Due Account ID',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'completed_at' => 'Completed At',
            'completed_by' => 'Completed By',
        ];
    }

    public function getScheduledList($bodyParams) {

        $page = $bodyParams->page;
        if(is_numeric($bodyParams->limit) && $bodyParams->limit < 50) {
            $limit = $bodyParams->limit;
        }else {
            $limit = 5;
        }
        if($page == 1 ) {
            $offset = 0;
        } else {
            $offset = ($limit)  * ($page -1 );
        }
        $query = new Query;
        $query  ->select([
                    'ap.*',
                    'u.id as u_id',
                    'u.f_name',
                    'u.l_name',
                    'add.location',
                    // 'i.id as investment_due_account_id',
                    // 'i.investment_id',
                    // 'i.amount',
                    // 'i.status',
                    // 'i.generated_at'
                ])  
                ->from('appointment_schedules as ap')
                ->join('LEFT JOIN', 'user as u', 'u.id = ap.u_id')
                ->join('LEFT JOIN', 'b2c_address as add', 'add.u_id = u.id')
                ->where(['ap.status' => 'scheduled']);
        if(!empty($bodyParams->roots)) {
            $query  ->orwhere(['add.roots' => $bodyParams->roots]);
        }
        $query  ->limit($limit)
                ->offset($offset);
        $command = $query->createCommand();
        $data = $command->queryAll();
        echo '<pre>'; print_r($data); echo '</pre>'; die();
        return $data;
    } 
}
