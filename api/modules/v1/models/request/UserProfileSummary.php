<?php
/**
 * Created by PhpStorm.
 * User: rangetech
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;


use yii\base\Model;
use yii\db\Query;

class UserProfileSummary extends Model
{
    public $user_id;
    public function rules()
    {
        return [
            [['user_id'],'safe'],
        ];
    }

    public function init() {
        // preload content
    }

    public function getUserAccountSummary($user_id = null, $guid = null) {
        if($user_id == null) {
            // return fetch faild log
        }
        $query = new Query;
        $query  ->select([
                    'u.id',
                    'u.username',
                    'u.f_name',
                    'u.l_name',
                    'u.email',
                    'u.mobile',
                    'address.location',
                    'address.address',
                    'address.pincode',
                ])
                ->from('user as u')
                ->join('LEFT JOIN', 'b2c_address as address', 'address.u_id = u.id')
                ->where([
                    // 'u.status' => 10, 
                    'u.level'  => '4',
                    'u.user_type' => 'user',
                    'u.id' => $user_id, 
                ])->one();

        $command = $query->createCommand();
        $userData = $command->queryOne();
        if ($userData) {
            $amountQuery = new Query;
            $amountQuery    ->select([
                                'sum(due_acc.amount) as pending_payment'
                            ])
                            ->from('investment_due_accounts as due_acc')
                            ->where([ 
                                'due_acc.u_id' => $user_id,
                            ])
                            ->andWhere([
                            '!=', 'due_acc.status', 'success',
                            ]);
            $command = $amountQuery->createCommand();
            $data = $command->queryOne();
            $userData['PaymentRefrence'] = $data;
            $response = [
                'status' => true,
                'data' => $userData,
                'messge' => ''
            ];
        } else {
            $response = [
                'status' => false,
                'data' => [],
                'message' => 'Cound not find the user account!'
            ];
        }
        return $response;

    }

    public function getInvestmentAccountList($user_id = null, $guid = null) {
        if($user_id == null) {
            // return fetch faild log
        }
        $query = new Query;
        $query      ->select([

                    ])
                    ->from('investment_joins as investment')
                    ->where([
                        'investment.u_id' => $user_id,
                    ])->all();

        $command = $query->createCommand();
        $data = $command->queryAll();
        $response = [
            'status'    => true,
            'data'      => $data,
            'message'   => ''
        ];
        return $response;
    }

    public function getSchemmeAccountList($user_id = null, $guid = null) {
        if($user_id == null) {
            // return fetch faild log
        }
        $query = new Query;
        $query      ->select([

                    ])
                    ->from('scheme_joins as scheme')
                    ->where([
                        'scheme.u_id' => $user_id,
                    ])->all();

        $command = $query->createCommand();
        $data = $command->queryAll();
        $response = [
            'status'    => true,
            'data'      => $data,
            'message'   => $data ? " " : "No result found" 
        ];
        return $response;
    }
}