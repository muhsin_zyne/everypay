<?php
/**
 * Created by PhpStorm.
 * User: femiibiwoye
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;

use Yii;
use yii\base\Model;
use api\modules\v1\models\InvestmentDueAccounts;
use api\modules\v1\models\User;
use yii\db\Query;

class ScanRequest extends Model
{
    public $scanParam;

    public function rules()
    {
        return [
            [['scanParam'],'safe'],
        ];
    }

    public function findScan() {
    	$type = null;
    	$query = new Query;
    	$query  ->select('*')
    			->from('investment_joins as i')
    			->where(['guid'=> $this->scanParam])
    			->one();
    	$command = $query->createCommand();
        $data = $command->queryOne();
        if($data) {
       		$type = 'investment';
        }
        if(empty($data)) {
        	$query = new Query;
	    	$query  ->select('*')
	    			->from('scheme_joins as i')
	    			->where(['guid'=> $this->scanParam])
	    			->one();
	    	$command = $query->createCommand();
	        $data = $command->queryOne();
	        if($data) {
	        	$type  = 'scheme';
	        }
        }

        return ['type' => $type, 'data'=> $data];
    }
}