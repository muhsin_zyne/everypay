<?php
/**
 * Created by PhpStorm.
 * User: rangetch
 * Date: 13/07/2018
 * Time: 5:33 PM
 */
namespace api\modules\v1\models\request;


use yii\base\Model;
use yii\db\Query;


class PastPaymentListRequest extends Model
{
    public $limit;
    public $page;
    public $u_id;

    public function rules()
    {
        return [
            [['limit'],'safe'],
            [['page','u_id'],'required'],
        ];
    }

    public function init() {
        // preload content
    }

    public function getPastPaymentList() {
        $page = $this->page;
        if(is_numeric($this->limit) && $this->limit <= 50) {
            $limit = $this->limit;
        }else {
            $limit = 50;
        }
        if($page == 1 ) {
            $offset = 0;
        } else {
            $offset = ($limit)  * ($page -1 );
        }

        $query = new Query;
        $query  ->select([
                    'i_due.id',
                    'i_due.u_id',
                    'i_due.investment_id',
                    'i_due.actual_amount',
                    'i_due.payment_for',
                    'i_due.generated_at',
                    'i_due.status',
                    'i_due.collected_at',

                ])
                ->from('investment_due_accounts as i_due')
                ->join('LEFT JOIN', 'investment_joins as i_join', 'i_due.investment_id = i_join.id')
                ->where([ '=', 'i_due.status', 'success' ]);
        $query  ->andFilterWhere([
                    'i_due.u_id' => $this->u_id,
                ]);
        $query  ->limit($limit)
                ->offset($offset);
        $command = $query->createCommand();
        
        $data = $command->queryAll();
        
        echo '<pre>'; print_r($data); echo '</pre>'; die();

   

        
    }

}