<?php
/**
 * Created by PhpStorm.
 * User: femiibiwoye
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;


use yii\base\Model;

class ScheduledList extends Model
{
    public $page;
    public $limit;
    public $roots;
    public $date_range_start;
    public $date_range_end;

    public function rules()
    {
        return [
            [['limit','roots','date_range_start','date_range_end'],'safe'],
            [['page'],'required'],
        ];
    }

}