<?php
/**
 * Created by PhpStorm.
 * User: rangetch
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;

use yii;
use Yii\helpers\ArrayHelper;
use yii\base\Model;
use api\modules\v1\models\InvestmentDueAccounts;
use api\modules\v1\models\InvestmentAccounts;
use api\modules\v1\models\SmsNotifications;


class InvestmentCollectionForm extends Model
{
    public $investment_due_account_id;
    public $u_id;
    public $actual_amount;
    public $collected_amount;
    public $returned_balance;


    public function rules()
    {
        return [
            [['investment_due_account_id', 'actual_amount', 'collected_amount', 'returned_balance'],'required'],
        ];
    }

    public function init() {
        // preload content
    }

    public function paymentCollection() {
        $investDueAccount = InvestmentDueAccounts::findOne($this->investment_due_account_id);
        if($investDueAccount->status != 'success') {
            $investDueAccount->actual_amount = $this->actual_amount;
            $investDueAccount->status = 'success';
            $investDueAccount->collected_at = yii::$app->controller->getNowTime();
            if ($investDueAccount->save()) {
                $investmentAcc = new InvestmentAccounts();
                $investmentAcc->investment_id = $investDueAccount->investment_id;
                $investmentAcc->investment_due_account_id = $investDueAccount->id;
                $investmentAcc->u_id = $investDueAccount->u_id;
                $investmentAcc->amount = $investDueAccount->actual_amount;
                $investmentAcc->type = 'dr';
                $investmentAcc->account_agent = yii::$app->user->identity->id;
                $investmentAcc->status = 'success';
                $investmentAcc->generated_at = yii::$app->controller->getNowTime();
                $investmentAcc->clearance_at = yii::$app->controller->getNowTime();
                $investmentAcc->collected_amount = $this->collected_amount;
                $investmentAcc->returned_balance = $this->returned_balance;
                if($investmentAcc->save()) {
                    $smsLog = new SmsNotifications();
                    $customerDetails = $smsLog->getCustomerDetails($investmentAcc->u_id);
                    $smsLog->to_mobile = $customerDetails->mobile;
                    $smsLog->sms_type = 'transactional';
                    $smsLog->content = $smsLog->formatSMSnotfication(1, $investmentAcc);
                    $smsLog->requested_at = yii::$app->controller->getNowTime();
                    $smsLog->category = 'recipt';
                    $smsLog->attempt = '0';
                    $smsLog->current_status = 'pending';
                    $smsLog->save();
                    return $investmentAcc;
                }

            }   
        } else if($investDueAccount->status == 'success') {
            // if pay request got twise 
            $findTransactonRecord = InvestmentAccounts::find()
                ->where([
                    'investment_due_account_id' => $investDueAccount->id
                ])->one();
            return $findTransactonRecord;
        }
        

    }

}