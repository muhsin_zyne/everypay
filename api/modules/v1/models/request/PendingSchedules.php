<?php
/**
 * Created by PhpStorm.
 * User: femiibiwoye
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;


use yii\base\Model;

class PendingSchedules extends Model
{
    public $page;
    public $limit;
    public $roots;

    public function rules()
    {
        return [
            [['limit','roots'],'safe'],
            [['page'],'required'],
        ];
    }

}