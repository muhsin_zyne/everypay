<?php
/**
 * Created by PhpStorm.
 * User: rangetch
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;


use yii\base\Model;
use api\modules\v1\models\InvestmentDueAccounts;

class PendingInvestmentPayments extends Model
{
    public $page;
    public $limit;
    public $u_id;
    public $investment_id;

    public function rules()
    {
        return [
            [['page'],'required'],
            [['limit', 'u_id', 'investment_id'],'safe'],
        ];
    }

    public function init() {
        // preload content
        if ($this->limit == null || $this->limit == '' || $this->limit >= 50) {
            $this->limit = 50;
        }
    }

    public function getPendingPayments() {
        $this->init();
        $investmentDueObj = new InvestmentDueAccounts();
        $dbResponse = $investmentDueObj->getPendingPayments($this);
        return $dbResponse;
    }

    public function getPendingPaymentById($id = null) {
        $investmentDueObj = new InvestmentDueAccounts();
        $dbResponse = $investmentDueObj->getPendingPaymentById($id);
        return $dbResponse;
    }
}