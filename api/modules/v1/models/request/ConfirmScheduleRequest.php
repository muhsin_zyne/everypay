<?php
/**
 * Created by PhpStorm.
 * User: femiibiwoye
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;


use yii\base\Model;
use api\modules\v1\models\InvestmentDueAccounts;

class ConfirmScheduleRequest extends Model
{
    public $agent_id;
    public $investment_obj;
    public $note;
    public $schedule_time;

    public function rules()
    {
        return [
            [['agent_id','investment_obj','note','schedule_time'],'required'],
        ];
    }

    public function loadInvestmentDueModel($id) {
        return InvestmentDueAccounts::findOne($id);
    }

}