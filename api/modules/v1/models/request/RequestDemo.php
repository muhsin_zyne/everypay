<?php
/**
 * Created by PhpStorm.
 * User: rangetch
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;


use yii\base\Model;

class RequestDemo extends Model
{
    public $id;
    public function rules()
    {
        return [
            [['id'],'safe'],
        ];
    }

    public function init() {
        // preload content
    }

}