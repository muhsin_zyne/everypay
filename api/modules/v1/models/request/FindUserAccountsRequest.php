<?php
/**
 * Created by PhpStorm.
 * User: femiibiwoye
 * Date: 13/07/2018
 * Time: 5:33 PM
 */

namespace api\modules\v1\models\request;

use Yii;
use yii\base\Model;
use api\modules\v1\models\InvestmentDueAccounts;
use api\modules\v1\models\User;
use yii\db\Query;

class FindUserAccountsRequest extends Model
{
    public $searchTearm;
    public $limit;
    public $page;

    public function rules()
    {
        return [
            [['limit','searchTearm'],'safe'],
            [['page'],'required'],
        ];
    }

    public function init() {

    }

    public function findAccounts() {
        $page = $this->page;
        if(is_numeric($this->limit) && $this->limit <= 50) {
            $limit = $this->limit;
        }else {
            $limit = 20;
        }
        if($page == 1 ) {
            $offset = 0;
        } else {
            $offset = ($limit)  * ($page -1 );
        }

        $query = new Query;
        $countQuery = new Query;

        $countQuery ->select(['count(*)']) 
            ->from('user as u')
            ->where([
                'u.status' => 10, 
                'u.level'  => '4',
                'u.user_type' => 'user' 
            ]);
        if ($this->searchTearm != '') {
        $countQuery ->andWhere(['or',
                ['like', 'u.f_name', $this->searchTearm],
                ['like', 'u.l_name', $this->searchTearm],
                ['like', 'u.email', $this->searchTearm],
                ['like', 'u.mobile', $this->searchTearm],
                ['like', 'u.username', $this->searchTearm],
           ]);
        }
        $query  ->select([
                    'u.id',
                    'u.username',
                    'u.f_name',
                    'u.l_name',
                    'u.email',
                    'u.mobile'
                ])
                ->from('user as u')
                ->where([
                    'u.status' => 10, 
                    'u.level'  => '4',
                    'u.user_type' => 'user' 
                ]);
        if ($this->searchTearm != '') {
        $query  ->andWhere(['or',
                    ['like', 'u.f_name', $this->searchTearm],
                    ['like', 'u.l_name', $this->searchTearm],
                    ['like', 'u.email', $this->searchTearm],
                    ['like', 'u.mobile', $this->searchTearm],
                    ['like', 'u.username', $this->searchTearm],
               ]);
        }
        $query  ->limit($limit)
                ->offset($offset);
        $count = $countQuery->createCommand()->queryScalar();
        $command = $query->createCommand();
        $data = $command->queryAll();

        return ['count' => $count, 'data' => $data];
    }


}