<?php

namespace api\modules\v1\models;

use Yii;
use api\modules\v1\models\InvestmentJoins;
use yii\db\Query;

/**
 * This is the model class for table "investment_due_accounts".
 *
 * @property integer $id
 * @property integer $u_id
 * @property integer $investment_id
 * @property double $amount
 * @property double $actual_amount
 * @property string $generated_at
 * @property string $status
 * @property string $collected_at
 */
class InvestmentDueAccounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investment_due_accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'investment_id', 'amount', 'generated_at', 'status'], 'required'],
            [['u_id', 'investment_id'], 'integer'],
            [['amount', 'actual_amount'], 'number'],
            [['generated_at', 'collected_at'], 'safe'],
            [['status'], 'string'],
        ];
    }

    public function getInvestmentModel(){
        return $this->hasOne(InvestmentJoins::className(),['id'=>'investment_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'investment_id' => 'Investment ID',
            'amount' => 'Amount',
            'actual_amount' => 'Actual Amount',
            'generated_at' => 'Generated At',
            'status' => 'Status',
            'collected_at' => 'Collected At',
        ];
    }
    public function getPendingPaymentById($id = null) {
        $query = new Query;
        $query  ->select([
                   'i_due.*',
                   'i_join.guid as investment_join_guid',
                   'i_join.plan_amount',
                   'i_join.plan_label',
                   'i_join.created_at as joined_on',
                   'i_join.valid_from',
                   'i_join.valid_true',
                   'i_join.status as investment_status',
                   'i_join.reccoring_type'  
                ])  
                ->from('investment_due_accounts as i_due')
                ->join('LEFT JOIN', 'investment_joins as i_join', 'i_due.investment_id = i_join.id')
                ->where([ '!=', 'i_due.status', 'success' ])
                ->andWhere(['i_due.id' => $id])
                ->one();
        $command = $query->createCommand();
        $data = $command->queryOne();
        return ['data' => $data];
        
    }


    public function getPendingPayments($bodyParams) {
        $page = $bodyParams->page;
        if(is_numeric($bodyParams->limit) && $bodyParams->limit <= 50) {
            $limit = $bodyParams->limit;
        }else {
            $limit = 50;
        }
        if($page == 1 ) {
            $offset = 0;
        } else {
            $offset = ($limit)  * ($page -1 );
        }

        $query = new Query;
        $countQuery = new Query;
        $query  ->select([
                   'i_due.*',
                   'i_join.guid as investment_join_guid',
                   'i_join.plan_amount',
                   'i_join.plan_label',
                   'i_join.created_at as joined_on',
                   'i_join.valid_from',
                   'i_join.valid_true',
                   'i_join.status as investment_status',
                   'i_join.reccoring_type'  
                ])  
                ->from('investment_due_accounts as i_due')
                ->join('LEFT JOIN', 'investment_joins as i_join', 'i_due.investment_id = i_join.id')
                ->where([ '!=', 'i_due.status', 'success' ]);
        $query  ->andFilterWhere([
                    'i_due.u_id' => $bodyParams->u_id,
                    'i_due.investment_id' => $bodyParams->investment_id,
                ]);

        $countQuery ->select(['count(*)']) 
            ->from('investment_due_accounts as i_due')
            ->where(['!=', 'i_due.status', 'success' ]);
        $countQuery ->andFilterWhere([
                    'i_due.u_id' => $bodyParams->u_id,
                    'i_due.investment_id' => $bodyParams->investment_id,
                ]);



        $count = $countQuery->createCommand()->queryScalar();
        $query  ->limit($limit)
                ->offset($offset);
        $command = $query->createCommand();
        
        $data = $command->queryAll();
        return [
            'data' => $data,
            'count' => $count
        ];
    }

    public function getPendingSheduleList($bodyParams) {
        $page = $bodyParams->page;
        if(is_numeric($bodyParams->limit) && $bodyParams->limit < 50) {
            $limit = $bodyParams->limit;
        }else {
            $limit = 5;
        }
        if($page == 1 ) {
            $offset = 0;
        } else {
            $offset = ($limit)  * ($page -1 );
        }
        $query = new Query;
        $countQuery = new Query;
        $query  ->select([
                    'u.id as u_id',
                    'u.f_name',
                    'u.l_name',
                    'add.location',
                    'i.id as investment_due_account_id',
                    'i.investment_id',
                    'i.amount',
                    'i.status',
                    'i.generated_at'
                ])  
                ->from('investment_due_accounts as i')
                ->join('LEFT JOIN', 'user as u', 'u.id = i.u_id')
                ->join('LEFT JOIN', 'b2c_address as add', 'add.u_id = u.id')
                ->where(['i.status'=> 'pending']);

        $countQuery ->select(['count(*)']) 
            ->from('investment_due_accounts as i')
            ->join('LEFT JOIN', 'user as u', 'u.id = i.u_id')
            ->join('LEFT JOIN', 'b2c_address as add', 'add.u_id = u.id')
            ->where(['i.status'=> 'pending']);

        if(!empty($bodyParams->roots)) {
            $query  -> andwhere(['add.roots' => $bodyParams->roots]);
            $countQuery  ->andwhere(['add.roots' => $bodyParams->roots]);
        }
        $count = $countQuery->createCommand()->queryScalar();
        $query  ->limit($limit)
                ->offset($offset);
        $command = $query->createCommand();
        
        $data = $command->queryAll();
        return [
            'data' => $data,
            'count' => $count
        ];
    }
}
