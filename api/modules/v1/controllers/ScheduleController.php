<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\ErrorException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\UnauthorizedHttpException;
use api\modules\v1\controllers\AuthenticationController;

use api\modules\v1\models\InvestmentDueAccounts;
use api\modules\v1\models\CollectionRoots;
use api\modules\v1\models\AppointmentSchedules;
use api\modules\v1\models\search\InvestmentDueAccountsSearch;

use api\modules\v1\models\request\PendingSchedules;
use api\modules\v1\models\request\ScheduledList;
use api\modules\v1\models\request\ConfirmScheduleRequest;

class ScheduleController extends AuthenticationController
{
    public function actionPendingSchedules() {
        if(yii::$app->request->getMethod() == 'POST') {
            $requestModel = new PendingSchedules();
            if($requestModel->load(['PendingSchedules' => yii::$app->request->post() ])) {
                if(!$requestModel->validate()) {
                    $requestModel->page = 1;
                }
                $investmentDueAccounts = new InvestmentDueAccounts();
                $dbResponse = $investmentDueAccounts->getPendingSheduleList($requestModel);
                $response = [
                    'status' => true,
                    'message' => '',
                    'data' => $dbResponse['data'],
                    'count' => $dbResponse['count']
                ];
                return $response;
            }   
        } else {
            throw new MethodNotAllowedHttpException('Accepts method (POST)');   
        }
        
    }

    public function actionScheduledList() {
        $requestModel = new ScheduledList();
        if($requestModel->load(['ScheduledList' => yii::$app->request->post() ])) {
            if(!$requestModel->validate()) {
                $requestModel->page = 1;
            }
            $appointmentSchedule = new AppointmentSchedules();
            $bdResponse = $appointmentSchedule->getScheduledList($requestModel);
           // echo '<pre>'; print_r("Sdsdsdsdd"); echo '</pre>'; die();
            // $investmentDueAccounts = new InvestmentDueAccounts();
            // $dbResponse = $investmentDueAccounts->getPendingSheduleList($requestModel);
            // $response = [
            //     'status' => true,
            //     'message' => '',
            //     'data' => $dbResponse
            // ];
            // return $response;
        }
    }

    public function actionGetScheduleFilter() {
        if(yii::$app->request->getMethod() == 'GET') {
            $dbQueryModel = new CollectionRoots();
            $dbResponse = $dbQueryModel->getList();
            $response = [
                'status' => true,
                'message' => '',
                'data' => $dbResponse
            ];
            return $response;
        } else {
            throw new MethodNotAllowedHttpException('Accepts method (GET)');   
        }
    }

    public function actionConfirmSchedules() {

        $requestModel = new ConfirmScheduleRequest();
        if( yii::$app->request->post() && $requestModel->load( ['ConfirmScheduleRequest' => yii::$app->request->post()] ) ) {
            if($requestModel->validate()) {
                $skip_count = 0;
                foreach ($requestModel->investment_obj as $investment_due_account_id) {
                    $investmentDueAccount = $requestModel->loadInvestmentDueModel($investment_due_account_id);
                    if($investmentDueAccount->status !='pending') {
                        // update log gose and skipp the insertion 
                        $skip_count++;
                        continue;
                    }
                    $appointmentSchedule = new AppointmentSchedules();
                    $appointmentSchedule->u_id = $investmentDueAccount->u_id;
                    $appointmentSchedule->agent_id = '1';  // active user id replace here
                    $appointmentSchedule->scheduled_for = date('Y-m-d H:i:s',strtotime($requestModel->schedule_time));
                    $appointmentSchedule->type = 'investment_collection';
                    $appointmentSchedule->investment_id = $investmentDueAccount->investment_id;
                    // applay scheme join id here if scheduling for scheme
                    $appointmentSchedule->investment_due_account_id = $investmentDueAccount->id;
                    $appointmentSchedule->status = 'scheduled';
                    $appointmentSchedule->created_at = $this->getNowTime();
                    $appointmentSchedule->updated_at = '';
                    $appointmentSchedule->created_by = '1'; // active User id replace
                    $appointmentSchedule->updated_at = '';
                    if ($appointmentSchedule->save()) {
                        $investmentDueAccount->status = 'scheduled';
                        $investmentDueAccount->save();
                    }
                }

                $updated_count = count($requestModel->investment_obj) - $skip_count;
                $response = [
                    'status' => $updated_count > 0 ? true : false,
                    'message' => $updated_count > 0 ? "".$updated_count." Payment collection has been scheduled to ".date('M d, Y',strtotime($requestModel->schedule_time)) : "No records maching to schedule",
                    'updated_count' => $updated_count,
                    'skip_count' => $skip_count
                ];

                return $response;
            } else {
                return [
                    'status' => false,
                    'message' => 'Invalid Request Params',
                    'errors' => $requestModel->getErrors(),
                ];
            }
        } else {
            throw new MethodNotAllowedHttpException();
        } 
    }
}