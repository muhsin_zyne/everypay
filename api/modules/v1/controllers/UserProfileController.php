<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\base\ErrorException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\UnauthorizedHttpException;
use api\modules\v1\controllers\AuthenticationController;

use api\modules\v1\models\request\FindUserAccountsRequest;
use api\modules\v1\models\request\ScanRequest;
use api\modules\v1\models\request\UserProfileSummary;


class UserProfileController extends AuthenticationController
{   

    /* action
        
    
    */
    
    public function actionFindAccount() {
        $requestModel = new FindUserAccountsRequest();
        if(yii::$app->request->getMethod() == 'POST') {
            if($requestModel->load(['FindUserAccountsRequest' => yii::$app->request->post() ])) { 
                if(!$requestModel->validate()) {
                    $requestModel->page = 1;
                }
                $dbResponse = $requestModel->findAccounts();
                $response = [
                    'status' => true,
                    'message' => '',
                    'data' => $dbResponse['data'],
                    'count' => $dbResponse['count']
                ];
                return $response;
            }
        } else {
            throw new MethodNotAllowedHttpException('Accepts method (POST)'); 
        }
    }

    public function actionScanFind() {
        $requestModel = new ScanRequest();
        if(yii::$app->request->getMethod() == 'POST') {
            if($requestModel->load(['ScanRequest' => yii::$app->request->post() ])) { 
                $dbResponse = $requestModel->findScan();
                $response = [
                    'status' => true,
                    'message' => '',
                    'data' => $dbResponse,
                ];
                return $response;
            }
        } else {
            throw new MethodNotAllowedHttpException('Accepts method (POST)'); 
        }
    }

    public function actionView($user_id = null, $guid = null) {
        $requestModel = new UserProfileSummary();
        if (yii::$app->request->getMethod() == 'GET') {
            $dbResponse = $requestModel->getUserAccountSummary($user_id, $guid);
            return $dbResponse;

        } else {
            throw new MethodNotAllowedHttpException('Accepts method (GET)'); 
        }

    }

    public function actionInvestmentAccountList($user_id = null, $guid = null) {
        $requestModel = new UserProfileSummary();
        if (yii::$app->request->getMethod() == 'GET') {
            $dbResponse = $requestModel->getInvestmentAccountList($user_id, $guid);
            return $dbResponse;

        } else {
            throw new MethodNotAllowedHttpException('Accepts method (GET)'); 
        }        
    }

    public function actionSchemeAccountList($user_id = null, $guid = null) {
        $requestModel = new UserProfileSummary();
        if (yii::$app->request->getMethod() == 'GET') {
            $dbResponse = $requestModel->getSchemmeAccountList($user_id, $guid);
            return $dbResponse;

        } else {
            throw new MethodNotAllowedHttpException('Accepts method (GET)'); 
        }

    }
   
}