<?php
/**
 * Created by PhpStorm.
 * User: muhsinzyne
 * Date: 13/02/2019
 * Time: 10:33 PM
 */

namespace api\modules\v1\controllers;

use Yii;
use yii\base\ErrorException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\UnauthorizedHttpException;
use api\modules\v1\controllers\AuthenticationController;

use api\modules\v1\models\request\PendingInvestmentPayments;
use api\modules\v1\models\request\InvestmentCollectionForm;
use api\modules\v1\models\request\PastPaymentListRequest;


class InvestmentController extends AuthenticationController
{   

    public function actionPendingPaymentList() {
    	$requestModel = new PendingInvestmentPayments();
        if(yii::$app->request->getMethod() == 'POST') {
            if($requestModel->load(['PendingInvestmentPayments' => yii::$app->request->post() ])) { 
                if(!$requestModel->validate()) {
                    $requestModel->page = 1;
                }
                $dbResponse = $requestModel->getPendingPayments();
                $response = [
                    'status' => true,
                    'message' => '',
                    'data' => $dbResponse['data'],
                    'count' => $dbResponse['count']
                ];
                return $response;
            }
        } else {
            throw new MethodNotAllowedHttpException('Accepts method (POST)'); 
        }
    }

    public function actionGetPendingPayment($id = null) {
    	$requestModel = new PendingInvestmentPayments();
        if(yii::$app->request->getMethod() == 'GET') {
        	$bdResponse = $requestModel->getPendingPaymentById($id);
        	$response = [
                'status' => true,
                'message' => '',
                'data' => $bdResponse['data'],
            ];
            return $response;
            
        } else {
            throw new MethodNotAllowedHttpException('Accepts method (GET)'); 
        }
    }

    public function actionCollectPayment() {
        $requestModel = new InvestmentCollectionForm();
        if(yii::$app->request->getMethod() == 'POST') {
            if($requestModel->load(['InvestmentCollectionForm' => yii::$app->request->post() ])) { 
                if(!$requestModel->validate()) {
                    return [
                        'status' => false,
                        'message' => 'Error in input data',
                        'data' => $requestModel->getErrors(),
                    ];
                }   else {
                    $collectionReport = $requestModel->paymentCollection();
                    $agentModel = $collectionReport->agentModel;
                    $customerModel = $collectionReport->userModel;

                    $response = [
                        'status' => true,
                        'message' => '',
                        'data' => [
                            'agentInfo' => [
                                'id' => $agentModel->id,
                                'f_name' => $agentModel->f_name,
                                'l_name' => $agentModel->l_name,
                            ],
                            'customerInfo' => [
                                'f_name' => $customerModel->f_name,
                                'l_name' => $customerModel->l_name,
                                'mobile' => $customerModel->mobile,
                            ],
                            'amount' => $collectionReport->amount,
                            'type' => $collectionReport->type,
                            'clearance_at' => $collectionReport->clearance_at,
                            'payment_status' => $collectionReport->status,
                            'payment_for' => $collectionReport->investmentDueAccModel->payment_for,
                        ],
                    ];
                    return $response;
                }
            }
        } else {
            throw new MethodNotAllowedHttpException('Accepts method (POST)'); 
        }

    }  

    public function actionPastPaymentList() {
       $requestModel = new PastPaymentListRequest();
       if(yii::$app->request->getMethod() == 'POST') {
            if($requestModel->load(['PastPaymentListRequest' => yii::$app->request->post()])) {
                if(!$requestModel->validate()) {
                    $requestModel->page = 1;
                    if(!$requestModel->validate()){
                        return [
                            'status' => false, 
                            'message'=> 'Missing Input fields', 
                            'data' => $requestModel->getErrors()
                        ]; 
                    }
                }
                $dbResponse = $requestModel->getPastPaymentList();
                echo '<pre>'; print_r($requestModel); echo '</pre>'; die();
            }
       } 
    } 
    
   
}