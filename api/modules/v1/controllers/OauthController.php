<?php
namespace api\modules\v1\controllers;


use Yii;
use api\modules\v1\models\LoginForm;
use api\modules\v1\models\User;
use yii\base\ErrorException;
use yii\web\MethodNotAllowedHttpException;
use yii\rest\Controller;

class OauthController extends Controller
{

    /**
     * @return array
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin()
    {   
        header('Access-Control-Allow-Origin: *'); 
        $model = new LoginForm();
        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '')) {
            if( $model->login() ) {
                $user = User::findOne(['username' => $model->username]);
                $user->access_token = \Yii::$app->security->generateRandomString();
                $user->token_expires = time() + (24*60*60);
                $user->save();
                return [
                    'status' => true,
                    'token' => $user->access_token, 
                    'expiry' => $user->token_expires,
                    'user' => $this->getUserArray($user),
                ];   
            }else {
                return [
                    'status' => false,
                    'message' => 'Incorrect username or password',
                ];
            }
            
        } else {
            throw new MethodNotAllowedHttpException('Not submitted');
        }
    }

    protected function getUserArray($user,$type='basic') {
        if($type=='basic') {
            $response = [
                'id' => $user->id,
                'username' => $user->username,
                'l_name' => $user->l_name,
                'f_name' => $user->f_name,
                'user_type' => $user->user_type,
                'email' => $user->email,
                'level' => $user->level,
            ];    
        }

        return $response;
    }
}