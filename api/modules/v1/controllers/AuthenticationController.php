<?php
namespace api\modules\v1\controllers;


use Yii;
use api\modules\v1\models\User;
use yii\base\ErrorException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\UnauthorizedHttpException;
use yii\rest\Controller;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use api\modules\v1\methods\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

class AuthenticationController extends Controller
{
    
    // public function behaviors() {   
    //     $behaviors = parent::behaviors();
    //     $user = new User();
    //     $authHeader = yii::$app->request->headers->get('Authorization');
    //     if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
    //         $identity = $user->findIdentityByAccessToken($matches[1]);
    //         if($identity===null) {
    //             throw new UnauthorizedHttpException("Sorry! you are not authorized");
    //         }
    //     }
    //     else {
    //         throw new UnauthorizedHttpException("Sorry! you are not authorized");
    //     }

    //     return $behaviors;
    // }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                // HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                // QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }


    public function getNowTime() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('Y-m-d H:i:s');
        return $dateTime;
    }
    public function getActiveUser() {
        return yii::$app->user->identity;
    }
}