<?php

$config = [
    'id' => 'api-app',
    'basePath' => dirname(__DIR__),
    'name'=>null,
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
           //Disable cookie and CSRF for stateless API
            //'cookieValidationKey' => 'rZqd3jYypLrmKlcRh2XGernm931sViMV',
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,

           'parsers' => [
                'text/plain' => 'yii\web\JsonParser',
                'text/json' => 'yii\web\JsonParser',
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response'=>[
            'format'=>\yii\web\Response::FORMAT_JSON
        ],
        'user' => [
            'identityClass' => 'api\modules\v1\models\User',
            'enableAutoLogin' => true,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'enableStrictParsing'=>true,
            'rules' => [
               // ['class'=>'yii\rest\UrlRule','controller'=>'v1/posts']
            ],
        ],
        'view' => null,

    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
