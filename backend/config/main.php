<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    

    'modules' => [
        'modules' => [
            'class' => 'backend\modules\Page',
        ],
    ],


    'components' => [

        

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            //'class' => '/common\models\User',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                //'investment-joins' => 'investment-joins/index',
                //'investment-joins/<guid>' => 'investment-joins/view',
                // '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>' // 2
            ],
        ],
    ],
    'params' => $params,
];
