<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model backend\models\Packages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="packages-form">
  <div class="row">
    <div class="col-xs-12">

	    <?php $form = ActiveForm::begin([
	    	'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'id' => 'ajax'

         ]); ?>

	    <?= $form->field($model, 'package_label')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'package_value')->widget(MaskMoney::classname(), [
		    'pluginOptions' => [
		        'allowNegative' => false
		    ]
		]); ?>
	    <!-- <?= $form->field($model, 'package_value')->textInput() ?>
 -->
	    <?= $form->field($model, 'status')->dropDownList([ '0'=>'Active', '1'=>'Inactive', ], ['prompt' => 'status']) ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>
    </div>
  </div>

</div>
