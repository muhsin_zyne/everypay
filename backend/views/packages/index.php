	<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PackagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="packages-index">
    <div class="row">
       <div class="col-xs-12">
        <p> <?= Html::a('Create Packages', ['create'], ['class' => 'btn btn-success theme-btn pull-right bootstrap-modal-new','data-title'=>'Create Package','data-width'=>'30%']) ?> </p>
           
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{summary}\n{items}\n<div class='pull-right'>{pager}</div>",
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                'package_label',
                'package_value',
                'status' => [   'header'=>'Status',
                            'value' => function($model, $attribute){ if($model->status==0) {return 'Active';} else if($model->status==1) {return 'Inactive';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'Active', '1' => 'Inactive'],['class'=>'form-control','prompt' => 'All']),

                ],
                ['class' => 'yii\grid\ActionColumn',   
                        'header'=>'Action',
                        'template'=>'{view}{update}{delete}',
                        'buttons'=>[
                          'update' => function ($url, $model) {    
                                return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'Edit '.$model->package_label,                                           
                                        'data-width'=>'30%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'Edit ',
                                    ]); 
                                },
                            'view' => function($url,$model) {
                                return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>$model->package_label,                                           
                                        'data-width'=>'30%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'View',
                                    ]); 

                            },
                        ]                            
                ],
            ],

        ]); ?>
        
       </div>   
   </div>
</div>
