<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SchemeJoins */

$this->title = 'Create Scheme Joins';
$this->params['breadcrumbs'][] = ['label' => 'Scheme Joins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scheme-joins-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
