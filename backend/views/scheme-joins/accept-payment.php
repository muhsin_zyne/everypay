<?php 
use backend\components\ViewFunction;
use kartik\money\MaskMoney;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use yii\helpers\Html;

$view = new ViewFunction();

$model->collected_amount		=	$acceptPaymentModel->grand_total;
$model->u_id 					=	$acceptPaymentModel->u_id;
$model->payment_record_id 		=	$acceptPaymentModel->payment_record_id;
$model->amount 					=	$acceptPaymentModel->amount;
$model->gst 					= 	$acceptPaymentModel->gst;
$model->grand_total				=	$acceptPaymentModel->grand_total;
$model->discount				=	$acceptPaymentModel->discount;


?>

<div class="scheme-joins-accept-payment">
	<div class="row">
		<div class="col-xs-12 col-sm-6 pull-right">
			<div class="box">
	            <div class="box-header with-border">
	                <h3 class="box-title">Payment Details</h3>
	            </div><!-- /.box-header -->
	            <div class="box-body">
	                <table class="table table-bordered">
	                    <tbody>
	                    	<tr>
	                            <th style="width: 40px">Amount to Pay</th>
	                            <td style="width: 60px"> <?= money_format('%!i',$acceptPaymentModel->grand_total) ?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px">Scheme Status</th>
	                            <td style="width: 60px"> <?= $view->bootsrapLabel('string',$acceptPaymentModel->paymentRecord->schemeJoins->status); ?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px">Billing For</th>
	                            <td style="width: 60px"> <?= date('M d, Y',strtotime($acceptPaymentModel->paymentRecord->billing_for)) ?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px">Bill Generated Time</th>
	                            <td style="width: 60px"> <?= date('M d, Y h:i A',strtotime($acceptPaymentModel->created_at)) ?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px"> Scheme Holder </th>
	                            <td style="width: 60px"> <?=$acceptPaymentModel->paymentUser->f_name.' '.$acceptPaymentModel->paymentUser->l_name?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px"> Plan </th>
	                            <td style="width: 60px"> <?=$acceptPaymentModel->paymentRecord->schemeJoins->plan_label ?> </td>
	                        </tr>
	                        


	                    </tbody>
	                </table>
	            </div><!-- /.box-body -->
        	</div><!-- /.box -->
		</div>
		<div class="col-xs-12 col-sm-6 bg-fff">
			<div class="row">
				<div class="col-xs-12">
					<div class="payment-box-summary pull-right">
						
					</div>
				</div>
				<div class="col-xs-12" id="loader-block">
					<?php $form = ActiveForm::begin([
								'method'	=> 'POST',
							    'id'=>'schemePaymentAdmin'
						]); ?>
					<table class="table table-bordered">
						
	                    <tbody>
	                    	<tr>
	                            <th style="width: 40px">Grand Total </th>
	                            <td style="width: 60px"> <span class="grand-total"> <span> <?= money_format('%!i',$acceptPaymentModel->grand_total) ?> </span> </span> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px"> Collected Amount </th>
	                            <td style="width: 60px"> <?= $form->field($model, 'collected_amount')->widget(MaskMoney::classname(), [
					                'pluginOptions' => [
					                    'allowNegative' => false
					                ],
					            ])->label(false) ?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px"> Collection Agent </th>
	                            <td style="width: 60px"> <?= $form->field($model, 'collection_agent_id')->dropDownList($model->findCollectionAgent())->label(false) ?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px"> Collection Note </th>
	                            <td style="width: 60px"> <?= $form->field($model, 'collection_agent_note')->textArea(['rows'=>3])->label(false) ?> </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px"> Return Balance </th>
	                            <td style="width: 60px"> <span class="moneydis"> <span id="return_balance_view"> </span> </span>  </td>
	                        </tr>
	                        <tr>
	                            <th style="width: 40px"> Balance Due </th>
	                            <td style="width: 60px"> <span class="moneydis"> <span id="balance_due_view"> </span> </span></td>
	                        </tr>
	                        <tr>
	                        	<?= $form->field($model, 'u_id')->hiddenInput([])->label(false) ?>
	                        	<?= $form->field($model, 'payment_record_id')->hiddenInput([])->label(false) ?>
	                        	<?= $form->field($model, 'amount')->hiddenInput([])->label(false) ?>
	                        	<?= $form->field($model, 'gst')->hiddenInput([])->label(false) ?>
	                        	<?= $form->field($model, 'grand_total')->hiddenInput([])->label(false) ?>
	                        	<?= $form->field($model, 'discount')->hiddenInput([])->label(false) ?>
	                        	<?= $form->field($model, 'return_balance')->hiddenInput([])->label(false) ?>
	                        	<?= $form->field($model, 'balance_due')->hiddenInput([])->label(false) ?>


	                        </tr>

	                        <tr>
	                        	<th> </th>
	                        	<td> 
	                        		<div class="form-group">
								        <?= Html::submitButton('Make Payment',[
								        	/*'data' => [
										        'confirm' => 'Are you sure want to delete this message?',
										    ],*/
										    'class' =>  'btn btn-primary pull-right']) ?>
								    </div>
								</td>
	                        </tr>
	                    </tbody>
	                </table>
				

					

		            
		            

				<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>




<style type="text/css">
	span.grand-total {
    	font-size: 23px;
    	color: #f39c12;
	}
	span.moneydis {
    	font-size: 23px;
    	color: #f39c12;
	}
	span.moneydis span {
		font-weight: bold;
	}
	span.grand-total span {
		font-weight: bold;
	}

</style>


<script type="text/javascript">
	$(document).ready(function() {
		paymentCalculate();



		/*$("form#schemePaymentAdmin").on("beforeSubmit", function (event, messages) {
			event.preventDefault();
			run_waitMe();
			alert("before");
          	var formData = $("form#schemePaymentAdmin").serializeArray();
            var ajaxRequestUrl = $("#schemePaymentAdmin").attr('action');
            jQuery.ajax({
                method: 'POST',
                url:ajaxRequestUrl,
                crossDomain: false,
                data: formData,
                dataType: 'json',
                success: function(responseData, textStatus, jqXHR) {
                    
                },
                error: function (responseData, textStatus, errorThrown) {
                    
                },
            });

            

        }).on('submit', function(e,messages){
        	e.preventDefault();
            
        });*/





		$("#paymentoffline-collected_amount-disp").change(function() {
    		paymentCalculate();	
    	});
	});

	function paymentCalculate() {
		run_waitMe();
		var formData = $("form#schemePaymentAdmin").serializeArray();
		jQuery.ajax({
            method: 'POST',
            url: '<?=yii::$app->params['backendUrl']?>/scheme-joins/api-payment-calculations',
            crossDomain: false,
            dataType: 'json',
            data : formData,
            success: function(responseData, textStatus, jqXHR) {
   				if(responseData.status==true) {
   					$("#return_balance_view").text(responseData.return_balance_m);
   					$("#balance_due_view").text(responseData.balance_due_m);
   					$("#paymentoffline-return_balance").val(responseData.return_balance);
   					$("#paymentoffline-balance_due").val(responseData.balance_due);

   				}
   				$(".waitMe").hide();
            },
            error: function (responseData, textStatus, errorThrown) {
              	$(".waitMe").hide();  
            },
	    });

		
	}


	function run_waitMe(effect){
    $('#loader-block').waitMe({
    	effect: 'roundBounce',
		text: 'Loading...',
		bg: 'rgba(255,255,255,0.7)',
		color: '#00aff0',
		sizeW: '',
		sizeH: '',
		source: '',
		onClose: function() {}

    });
  }
	
</script>