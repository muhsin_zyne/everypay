<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use backend\components\ViewFunction;


$view = new ViewFunction();
/* @var $this yii\web\View */

  
?>

<?php if($schemeJoinsModel->status!='active') {
    echo $view->printWatermark('Scheme '.$schemeJoinsModel->status,'Please refer account logs for more informations');
} ?>

<div class="row">
  <div class="col-xs-12 col-sm-8">
    <div class="box box-primary payment-list-box">
      <div class="box-header with-border">
        <h3 class="box-title">Pending Payments </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div><!-- /.box-header -->
      <div class="box-body">
        <ul class="products-list product-list-in-box">

          <?php if ($dataProvider->totalCount > 0) { ?>  
          <?= ListView::widget([
              'dataProvider' => $dataProvider,
              'layout' => "{summary}\n{items}\n <div class='pull-right'>{pager}</div> <div class='clearfix'></div>",
              'pager' => [
                  'firstPageLabel' => 'First',
                  'lastPageLabel' => 'Last',
                  'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                  'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
              ],
              'itemOptions' => ['class' => 'item'],
              'itemView'=>'_list-pending-payment',
            ]) ?>
          <?php } else {echo'<h1 class="text-center green"> <i class="fa fa-check"> </i> <br> No pending payments </h1>';} ?>


          
          
        </ul>
      </div><!-- /.box-body -->
      <div class="box-footer text-center">
        <a href="javascript::;" class="uppercase">View All Products</a>
      </div><!-- /.box-footer -->
    </div>
  </div>
   <div class="col-md-4 col-xs-12 pull-right">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Scheme Details</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 40px">Scheme Status</th>
                            <td style="width: 60px"> <?= $view->bootsrapLabel('string',$schemeJoinsModel->status); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Joining Date </th>
                            <td style="width: 60px"> <?= date('M d, Y h:i A',strtotime($schemeJoinsModel->created_at)) ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Scheme Label </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->plan_label ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Scheme User </th>
                            <td style="width: 60px"> <?= $userModel->f_name.' '.$userModel->l_name ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Monthly Plan </th>
                            <td style="width: 60px"> <?= money_format('%!i', $schemeJoinsModel->plan_amount)?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Total Investment </th>
                            <td style="width: 60px"> <?= money_format('%!i', $schemeJoinsModel->total_investment)?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Duration </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->duration ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Paid Duration </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->paid_duration ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Valid from </th>
                            <td style="width: 60px"> <?= date('M d, Y',strtotime($schemeJoinsModel->valid_from)) ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px">  Valid True </th>
                            <td style="width: 60px">  <?= date('M d, Y',strtotime($schemeJoinsModel->valid_true)) ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Included Offer </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->offer_label ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Offer Applicable only </th>
                            <td style="width: 60px">  <?php if($schemeJoinsModel->offer_availing_on=='end_of') echo'End of Period'; else if($schemeJoinsModel->offer_availing_on=='anytime') echo'Any Time'; ?> </td>
                        </tr>
                        


                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>


<style type="text/css">

li.item.payment-list {
    background: #f1f1f1;
    padding: 16px;
  }
  .payment-list-box .item{
    padding: 8px 0px;
  }

.scheme-summary-box {
    background: #F7F7F7;
    border-radius: 3px;
    padding: 10px 15px;
}
.scheme-summary-box .summary-box-content {
    padding: 0px 15px;
    margin-left: -57px;
}
.scheme-summary-box .summary-box-content li {
    font-size: 14px;
    font-weight: bold;
    border-bottom: 1px solid rgba(216, 216, 216, 0.52);
    padding: 8px 10px;
}
.scheme-summary-box .summary-box-content ul li {
    list-style: none !important;
}
.scheme-summary-box .summary-box-content li span {
    color: #000;
}


</style>