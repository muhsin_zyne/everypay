<?php 
use yii\helpers\Html;
use backend\components\ViewFunction;


$view = new ViewFunction();
?>
<?php if($schemeJoinsModel->status!='active') {
    echo $view->printWatermark('Scheme '.$schemeJoinsModel->status,'Please refer account logs for more informations');
} ?>


<div class="row">
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3> <?= money_format('%!i', $schemeJoinsModel->plan_amount)?> </h3>
                <p> Monthly Plan </p>
            </div>
            <div class="icon">
                <i class="fa fa-inr"></i>
            </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= money_format('%!i', $schemeJoinsModel->total_investment)?> </h3>
                <p>Total Investment</p>
            </div>
            <div class="icon">
                <i class="fa fa-inr"></i>
            </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>  <?=$schemeJoinsModel->paid_duration ?> </h3>
                <p> Paid Months </p>
            </div>
            <div class="icon">
                <i class="fa fa-bar-chart"></i>
            </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-sm-6 col-xs-12">
        <div class="small-box bg-red">
            <div class="inner">
                <h3>  <?= count($pendingPaymentsArray)?> </h3>
                <p>  Pending Payments </p>
            </div>
            <div class="icon">
                <i class="fa fa-bar-chart"></i>
            </div>
            <?= Html::a('More info <i class="fa fa-arrow-circle-right"></i>',['/scheme-joins/view-pending-payments','join_id'=>' '/*$schemeJoinsModel->id*/,'join_guid'=>$schemeJoinsModel->guid],['class'=>'small-box-footer'])?>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-6 col-xs-12 pull-right">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Scheme Details</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 40px">Scheme Status</th>
                            <td style="width: 60px"> <?= $view->bootsrapLabel('string',$schemeJoinsModel->status); ?></td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Joining Date </th>
                            <td style="width: 60px"> <?= date('M d, Y h:i A',strtotime($schemeJoinsModel->created_at)) ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Scheme Label </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->plan_label ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Scheme User </th>
                            <td style="width: 60px"> <?= $userModel->f_name.' '.$userModel->l_name ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Monthly Plan </th>
                            <td style="width: 60px"> <?= money_format('%!i', $schemeJoinsModel->plan_amount)?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Total Investment </th>
                            <td style="width: 60px"> <?= money_format('%!i', $schemeJoinsModel->total_investment)?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Duration </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->duration ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Paid Duration </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->paid_duration ?>  </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Valid from </th>
                            <td style="width: 60px"> <?= date('M d, Y',strtotime($schemeJoinsModel->valid_from)) ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px">  Valid True </th>
                            <td style="width: 60px">  <?= date('M d, Y',strtotime($schemeJoinsModel->valid_true)) ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Included Offer </th>
                            <td style="width: 60px"> <?=$schemeJoinsModel->offer_label ?> </td>
                        </tr>
                        <tr>
                            <th style="width: 40px"> Offer Applicable only </th>
                            <td style="width: 60px">  <?php if($schemeJoinsModel->offer_availing_on=='end_of') echo'End of Period'; else if($schemeJoinsModel->offer_availing_on=='anytime') echo'Any Time'; ?> </td>
                        </tr>
                        


                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>









<style type="text/css">

.scheme-summary-box {
    background: #F7F7F7;
    border-radius: 3px;
    padding: 10px 15px;
}
.scheme-summary-box .summary-box-content {
    padding: 0px 15px;
    margin-left: -57px;
}
.scheme-summary-box .summary-box-content li {
    font-size: 14px;
    font-weight: bold;
    border-bottom: 1px solid rgba(216, 216, 216, 0.52);
    padding: 8px 10px;
}
.scheme-summary-box .summary-box-content ul li {
    list-style: none !important;
}
.scheme-summary-box .summary-box-content li span {
    color: #000;
}



</style>