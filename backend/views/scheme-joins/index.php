<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SchemeJoinsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Scheme Joins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-box">
    <div class="row">
        <div class="col-xs-12">
            <div class="input-group add-on" style="padding-bottom: 10px">
              <input class="form-control"  placeholder="QR Code" name="srch-term" id="schemeScanBox" type="text" autofocus/>
              <div class="input-group-btn">
                <button class="btn btn-default search-icon" type="submit"><i class="glyphicon glyphicon-search"></i></button>
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php //$this->render('_search',['model'=>$searchModel])?>
    </div>
</div>
<div class="scheme-joins-index bg-fff">
    <div class="row">
        <div class="col-xs-12">
            <p> <?= Html::a('<i class="fa fa-plus"> </i> Create Scheme Joins', ['/accounts/account-validate'], ['class' => 'btn btn-success pull-right bootstrap-modal-new','data-title'=>'User already on the system ?','data-width'=>'30%']) ?> </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'header'=>'First Name',
                        'attribute' => 'f_name',
                        'value'=>'schemeUser.f_name',
                    ],
                     [
                        'header'=>'Last Name',
                        'attribute' => 'l_name',
                        'value'=>'schemeUser.l_name',
                    ],
                    [
                        'attribute'=>'plan_amount',
                        'value'=>function($model) {
                             return money_format('%i',$model->plan_amount);
                        },
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    'plan_label',
                    [
                        'attribute'=>'duration',
                        'value'=>'duration',
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                        'attribute'=>'paid_duration',
                        'value'=>'paid_duration',
                        'contentOptions' => ['class' => 'text-right'],
                    ],
                    [
                        'attribute'=>'total_investment',
                        'value'=>function($model) {
                             return money_format('%i',$model->total_investment);
                        },
                        'contentOptions' => ['class' => 'text-right'],
                    ],

                    'status' => [   'header'=>'Status',
                            'value' => function($model, $attribute){ if($model->status=='active') {return 'Active';} else if($model->status=='expired') {return 'Expired';} else if($model->status=='suspended') {return 'Suspended';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['active' => 'Active', 'expired' => 'Expired','suspended'=>'Suspended'],['class'=>'form-control','prompt' => 'All']),

                        ],
                    [
                        'header'=>'Joined Date',
                        'value'=> function ($model) {
                                return date('d-m-Y h:i A',strtotime($model->created_at));
                        },
                    ],
                    ['class' => 'yii\grid\ActionColumn',   
                        'header'=>'Action',
                        'template'=>'{view}{update}',
                        'buttons'=>[
                          'update' => function ($url, $model) {    
                                return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', ['/scheme-joins/view','join_id'=>' ','join_guid'=>$model->guid], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'title' => 'Edit ',
                                    ]); 
                                },
                            'view' => function($url,$model) {
                                return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ',['/scheme-joins/view','join_id'=>' ','join_guid'=>$model->guid], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'title' => 'View',
                                    ]); 

                            },
                        ]                            
                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>

<style type="text/css">

input.form-control.scan-box {
    height: 60px;
    font-size: 40px;
}
span.input-group-addon.search-icon {
    width: 102px;
    background: #a93232;
    color: #fff;
}
</style>


<script type="text/javascript">
$(document).ready(function(){
    $("#schemeScanBox").change(function() {
        formSubmit();
    });

    

    $(".search-icon").click(function() {
        formSubmit();
    })

    function formSubmit() {
        var schemeJoinId = $("#schemeScanBox").val();
        $.ajax({
                method: 'GET',
                url: '/scheme-joins/scan-join?join_id='+schemeJoinId,
                crossDomain: false,
                success: function(responseData, textStatus, jqXHR) {
                    responseData = JSON.parse(responseData);
                    if(responseData.status==true) {
                        window.location='/scheme-joins/view?join_id='+responseData.joinId;
                    }
                    else {
                        location.reload();
                    }
                            
            },
            error: function (responseData, textStatus, errorThrown) {
               
                
            },
        });
    }
});
</script>