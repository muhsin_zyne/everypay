<?php
use yii\helpers\Html;

?>
<li class="item payment-list">
  <div class="product-img">
    <i class="fa fa-inr f-65"> </i>
  </div>
  <div class="product-info">
    <span class="h3"> <?= money_format('%!i',$model->paymentRecordOnline->grand_total)?> </span>
    	<?=Html::a('Pay Now',['/scheme-joins/accept-payment','guid'=>$model->paymentRecordOnline->guid],['class'=>'btn btn-primary pull-right']) ?>
    <span class="product-description">
      <?=$model->paymentRecordOnline->label?>
    </span>
  </div>
</li><!-- /.item -->
<div class="clearfix"> </div>