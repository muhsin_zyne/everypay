<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SchemeJoins */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scheme-joins-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'u_id')->textInput() ?>

    <?= $form->field($model, 'plan_amount')->textInput() ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'total_investment')->textInput() ?>

    <?= $form->field($model, 'paid_duration')->textInput() ?>

    <?= $form->field($model, 'startup_payment')->dropDownList([ 'paid' => 'Paid', 'paid_updated' => 'Paid updated', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'valid_from')->textInput() ?>

    <?= $form->field($model, 'valid_true')->textInput() ?>

    <?= $form->field($model, 'offer_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'offer_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bonus_month')->textInput() ?>

    <?= $form->field($model, 'bonus_cash')->textInput() ?>

    <?= $form->field($model, 'bonus_gold')->textInput() ?>

    <?= $form->field($model, 'offer_valid_from')->textInput() ?>

    <?= $form->field($model, 'offer_valid_true')->textInput() ?>

    <?= $form->field($model, 'offer_availing_on')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'expired' => 'Expired', 'suspended' => 'Suspended', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
