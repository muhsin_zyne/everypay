<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\search\SchemeJoinsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scheme-joins-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-xs-12 col-sm-3">
        <?= $form->field($model, 'created_at', [
            'addon'=>['prepend'=>['content'=>'<i class="glyphicon glyphicon-calendar"></i>']],
                'options'=>['class'=>'drp-container form-group']
        ])->widget(DateRangePicker::classname(), [
            'convertFormat'=>true,
            'pluginOptions'=>[
                'timePicker'=>false,
                'locale'=>[
                    'format'=>'m/d/Y'
                ],
            ],
            ])->label('Join Date Range');

        ?>
    </div>
    <div class="col-xs-12 col-sm-3">
        <?= $form->field($model, 'f_name')->textInput(['placeholder'=>'First Name'])->label('First Name') ?>
    </div>
    <div class="col-xs-12 col-sm-3">
        <?= $form->field($model, 'l_name')->textInput(['placeholder'=>'Last Name']) ?>
    </div>
    <div class="col-xs-12 col-sm-3">
        <?= $form->field($model, 'status')->dropDownlist(['active' => 'Active', 'expired' => 'Expired','suspended'=>'Suspended'],['class'=>'form-control','prompt' => 'All']) ?>
    </div>
    
    <div class="col-xs-12 col-sm-12">
        <div class="form-group pull-right">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
        
    <?php ActiveForm::end(); ?>
</div>
