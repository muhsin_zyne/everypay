<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SchemeJoins */

$this->title = 'Update Scheme Joins: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Scheme Joins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="scheme-joins-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
