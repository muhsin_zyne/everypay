<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionRoots */

$this->title = 'Update Collection Roots: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Collection Roots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="collection-roots-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
