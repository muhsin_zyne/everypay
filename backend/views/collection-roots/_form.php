<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionRoots */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="collection-roots-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'root_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 1 => 'Active', 0 => 'Disable', ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?>
    </div>
    <div class="clearfix"></div>
    <?php ActiveForm::end(); ?>

</div>
