<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CollectionRootsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Collection Roots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collection-roots-index bg-fff">


    <p> <?= Html::a('Create Root', ['create'], ['class' => 'btn btn-success theme-btn pull-right bootstrap-modal-new','data-title'=>'Create Collection Root','data-width'=>'30%']) ?> </p> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'root_name',
            'status' => [   'header'=>'Status',
                            'value' => function($model, $attribute){ if($model->status==1) {return 'Active';} else if($model->status==0) {return 'Inactive';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['1' => 'Active', '0' => 'Inactive'],['class'=>'form-control','prompt' => 'All']),

                ],
            ['class' => 'yii\grid\ActionColumn',   
                    'header'=>'Action',
                    'template'=>'{view}{update}{delete}',
                    'buttons'=>[
                      'update' => function ($url, $model) {    
                            return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', [$url], [
                                    'data-toggle'=>'tooltip',
                                    'data-placement'=>'top',
                                    'data-title'=>'Edit - '.$model->root_name,                                           
                                    'data-width'=>'30%',
                                    'class'=>'bootstrap-modal-new',
                                    'title' => 'Edit ',
                                ]); 
                            },
                        'view' => function($url,$model) {
                            return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', [$url], [
                                    'data-toggle'=>'tooltip',
                                    'data-placement'=>'top',
                                    'data-title'=>$model->root_name,                                           
                                    'data-width'=>'30%',
                                    'class'=>'bootstrap-modal-new',
                                    'title' => 'View',
                                ]); 

                        },
                    ]                            
            ],
        ],
    ]); ?>
</div>
