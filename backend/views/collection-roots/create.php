<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CollectionRoots */

$this->title = 'Create Collection Roots';
$this->params['breadcrumbs'][] = ['label' => 'Collection Roots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collection-roots-create">
	<?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
