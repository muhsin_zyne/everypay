<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CollectionRoots */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Collection Roots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collection-roots-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'root_name',
            [
                'attribute' => 'status',
                'value' => function($model) { if($model->status==1) return'Active'; else return 'Inactive'; },
            ],
        ],
    ]) ?>

</div>
