<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SmsTemplates */

$this->title = 'Update Sms Templates: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sms Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sms-templates-update">

    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
