<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SmsTemplates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sms-templates-form">

    <?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 1 => 'Active', 0 => 'Disabled', ], []) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right theme-btn' : 'btn btn-primary pull-right theme-btn']) ?>
    </div>
    <div class="clearfix"> </div>

    <?php ActiveForm::end(); ?>

</div>
