<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\components\ViewFunction;


/* @var $this yii\web\View */
/* @var $model backend\models\SmsTemplates */

$this->title = $model->label;
$this->params['breadcrumbs'][] = ['label' => 'Sms Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-templates-view">

    
    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'label',
            'content:ntext',
            [
                'attribute' => 'status',
                'format'=>'raw',
                'value' => function($model) {
                    $viewFunction = new ViewFunction();
                    return $viewFunction->bootsrapLabel('number',$model->status);
                }
            ]
        ],
    ]) ?>

</div>
