<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\ViewFunction;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SmsTemplatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sms Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-templates-index bg-fff">

    
     <p> <?= Html::a('Create Template', ['create'], ['class' => 'btn btn-success theme-btn pull-right bootstrap-modal-new ','data-title'=>'Create SMS Templeate','data-width'=>'80%',]) ?> </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'label',
            'status' => [   
                'header'=>'Status',
                'format'=>'raw',
                'value' => function($model, $attribute){ 
                    $viewFunction = new ViewFunction();
                    return $viewFunction->bootsrapLabel('number',$model->status);
                },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['1' => 'Active', '0' => 'Disabled'],['class'=>'form-control','prompt' => 'All']),

            ],
            [
                'class' => 'yii\grid\ActionColumn',   
                'header'=>'Action',
                'template'=>'{view}{update}{delete}',
                'buttons'=>[
                    'update' => function ($url, $model) {    
                        return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', [$url], [
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'top',
                            'data-title'=>'Update SMS Templeate - '.$model->label,                                           
                            'data-width'=>'80%',
                            'class'=>'bootstrap-modal-new',
                            'title' => 'Edit ',
                        ]); 
                    },
                    'view' => function($url,$model) {
                        return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', [$url], [
                            'data-toggle'=>'tooltip',
                            'data-placement'=>'top',
                            'data-title'=>'View SMS Templeate - '.$model->label,                                           
                            'data-width'=>'80%',
                            'class'=>'bootstrap-modal-new',
                            'title' => 'View',
                        ]); 

                    },
                ]                            
            ],
        ],
    ]); ?>
</div>
