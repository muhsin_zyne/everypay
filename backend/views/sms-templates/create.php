<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SmsTemplates */

$this->title = 'Create Sms Templates';
$this->params['breadcrumbs'][] = ['label' => 'Sms Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-templates-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
