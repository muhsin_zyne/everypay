<style>
#signature{
 width: 300px; height: 200px;
 border: 1px solid black;
}
</style>

<!-- Signature -->
<div id="signature" style=''>
 <canvas id="signature-pad" class="signature-pad" width="300px" height="200px"></canvas>
</div>

<button id="export" class="btn btn-primary"> export </button>
<button id="clear" class="btn btn-primary"> clear </button>
<!-- Script -->

<script>
$(document).ready(function() {
 var signaturePad = new SignaturePad(document.getElementById('signature-pad'));

 $('#click').click(function(){
  var data = signaturePad.toDataURL('image/png');
  $('#output').val(data);

  $("#sign_prev").show();
  $("#sign_prev").attr("src",data);
  // Open image in the browser
  //window.open(data);
 });

 $("#clear").click(function() {
  signaturePad.clear();

 })

 $("#export").click(function() {

 	if(!signaturePad.isEmpty()) {

 		var data = signaturePad.toDataURL('image/png');
	 	alert(data);
	 	var url = '/test/save';

		$.ajax({ 
	    type: "POST", 
	    url: url,
	    dataType: 'text',
		    data: {
		        base64data : data
		    }
		});

 	}
 	else {
 		alert("signature is required");
 	}


 	


 });
})
 </script>