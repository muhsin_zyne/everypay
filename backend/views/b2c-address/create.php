<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\B2cAddress */

$this->title = 'Create B2c Address';
$this->params['breadcrumbs'][] = ['label' => 'B2c Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="b2c-address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
