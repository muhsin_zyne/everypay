<?php 


?>
<?php if($schemeJoinsModel->status!='active') {
	echo yii::$app->controller->printWatermark('Scheme '.$schemeJoinsModel->status,'Please refer account logs for more informations');
} ?>


<div class="row">
	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-yellow">
            <div class="inner">
                <h3> <?= money_format('%!i', $schemeJoinsModel->plan_amount)?> </h3>
                <p> Monthly Plan </p>
            </div>
            <div class="icon">
                <i class="fa fa-inr"></i>
            </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

	<div class="col-lg-3 col-xs-6">
		<div class="small-box bg-green">
            <div class="inner">
                <h3><?= money_format('%!i', $schemeJoinsModel->total_investment)?> </h3>
                <p>Total Investment</p>
            </div>
            <div class="icon">
                <i class="fa fa-inr"></i>
            </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
		<div class="small-box bg-blue">
            <div class="inner">
                <h3>  <?=$schemeJoinsModel->paid_duration ?> </h3>
                <p> Paid Duration </p>
            </div>
            <div class="icon">
                <i class="fa fa-bar-chart"></i>
            </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
		<div class="small-box bg-red">
            <div class="inner">
                <h3>  <?=$schemeJoinsModel->paid_duration ?> </h3>
                <p>  Pending Payments </p>
            </div>
            <div class="icon">
                <i class="fa fa-bar-chart"></i>
            </div>
           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>


</div>

<div class="row">
   <div class="col-xs-12 col-sm-4 pull-right">
  		<div class="box box-info">
			<div class="box-header with-border">
			  <h3 class="box-title">Scheme Details</h3>
			  <div class="box-tools pull-right">
			    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
			  </div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="scheme-summary-box">
		            <div class="summary-box-content">
		            	<?php 	 ?>
		                <ul>
		                	<li> Scheme Status <span class="pull-right">  <?= ucwords($schemeJoinsModel->status) ?> </span> </li>
		                    <li> Scheme Label<span class="pull-right"> <?=$schemeJoinsModel->plan_label ?> </span> </li>
		                    <li> Scheme User<span class="pull-right"> <?= $userModel->f_name.' '.$userModel->l_name ?> </span> </li>
		                    <li> Monthly Plan <span class="pull-right"> <?= money_format('%!i', $schemeJoinsModel->plan_amount)?> </span> </li>
		                    <li> Total Investment <span class="pull-right"> <?= money_format('%!i', $schemeJoinsModel->total_investment)?> </span> </li>
		                    <li> Duration <span class="pull-right">  <?=$schemeJoinsModel->duration ?> </span> </li>
		                    <li> Paid Duration <span class="pull-right">  <?=$schemeJoinsModel->paid_duration ?> </span> </li>
		                    <li> Valid from <span class="pull-right">  <?= date('M d, Y',strtotime($schemeJoinsModel->valid_from)) ?> </span> </li>
		                    <li> Valid True <span class="pull-right">  <?= date('M d, Y',strtotime($schemeJoinsModel->valid_true)) ?> </span> </li>
		                    <li> Included Offer <span class="pull-right"> <?=$schemeJoinsModel->offer_label ?> </span> </li>
		                    <li> Offer Applicable only <span class="pull-right"> <?php if($schemeJoinsModel->offer_availing_on=='end_of') echo'End of Period'; else if($schemeJoinsModel->offer_availing_on=='anytime') echo'Any Time'; ?> </span> </li>
		                    <li> Joining Date <span class="pull-right">  <?= date('M d, Y h:i A',strtotime($schemeJoinsModel->created_at)) ?>  </span> </li>
		                    
		                </ul>
		            </div>
		        </div>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
			</div><!-- /.box-footer -->	
		</div>
	</div>
</div>







<style type="text/css">



.important-box ul li {
    list-style: none;
    padding: 2px 0px;
    width: 50%;
    /* float: left; */
} 

.important-box {
    margin: -15px -15px;
    margin-bottom: 15px;
}

.scheme-summary-box {
    background: #F7F7F7;
    border-radius: 3px;
    padding: 10px 15px;
}
.scheme-summary-box .summary-box-content {
    padding: 0px 15px;
    margin-left: -57px;
}
.scheme-summary-box .summary-box-content li {
    font-size: 14px;
    font-weight: bold;
    border-bottom: 1px solid rgba(216, 216, 216, 0.52);
    padding: 8px 10px;
}
.scheme-summary-box .summary-box-content ul li {
	list-style: none !important;
}
.scheme-summary-box .summary-box-content li span {
    color: #000;
}



</style>