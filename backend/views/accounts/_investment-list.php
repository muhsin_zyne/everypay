<?php 
use yii\helpers\Html;
?>

<div class="investment-box">
  <div class="title">
     <?=$model->plan_label ?>
  </div>
  <div class="body">
    <h2> <?= number_format($model->investmentTotal, 2) ?> </h2> <span> Ruppes invested </span>
  </div>
  <div class="footer-investment">
    <p class="valid-form"> <?=date('M d, Y',strtotime($model->valid_from)). ' to '. date('M d, Y',strtotime($model->valid_true))?> </p> 

    <?=Html::a('More Info', ['/investment-joins/view', 'guid'=> $model->guid],['class'=>'btn btn-primary pull-right'])?>
    <div class="clearfix"> </div>
  </div>
</div>


