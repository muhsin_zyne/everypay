<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;    
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index bg-fff">
    <div class="row searchbox">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p> <?= Html::a('Create User', ['/accounts/account-validate'], ['class' => 'btn btn-success bootstrap-modal-new theme-btn pull-right','data-title'=>'User already on the system ?','data-width'=>'30%']) ?> </p>
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class' => 'table -custom',
            'rowOptions' => function ($model, $key, $index, $grid) {
                return ['data-href' => '/accounts/view?id=' . $model->id];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],


                'username',
                'f_name',
                'l_name',
                'mobile',
                [   
                    'header'=>'Join Date',
                    'value'=> function($model) {
                        return date('d-m-Y h:i A',$model->created_at);
                    },
                ],
                [   
                    'header'=>'Action',
                    'format'=> 'raw',
                    'value'=> function($model) {
                        return Html::a('<i class="fa fa-eye"> </i> View',['/sss'],['classs'=>'ssss']);
                    },
                ],
            ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('table tr[data-href]').click(function () {
            if ($(this).data('href') !== undefined) {
                window.location.href = $(this).data('href');
            }
        });
    });
</script>