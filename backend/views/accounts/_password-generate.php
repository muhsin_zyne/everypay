<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
$user = new User;
//print_r($user->getRandomPassword());


?>
<div class="form-group">
    <?= Html::textInput('xxx', $user->getRandomPassword(),['class'=>'form-control','id'=>'generatedPassword']); ?>
</div>
<?= Html::a('Refresh <i class="fa fa-refresh"> </i>', ['/accounts/password-generate'], ['class' => 'bootstrap-modal-new btn btn-primary pull-left', 'id'=>'passwordRefresh', 'data-title' =>'Password Generate','data-width'=>'30%']) ?>

<div class="row">
    <div class="col-xs-12">
        <label class="checkbox checkbox--material">
          <input type="checkbox" class="checkbox__input checkbox--material__input" id="check1">
          <div class="checkbox__checkmark checkbox--material__checkmark"></div>
          I have copied password
        </label>
    </div>
    <div class="col-xs-12">
        <?= Html::a('Apply <i class="fa fa-save"> </i>', ['#'], ['class' => 'btn btn-primary pull-right disabled', 'id'=>'passwordInsert']) ?>

    </div>
</div>




<script type="text/javascript">
    
    $("#passwordInsert").click(function(event){
        event.preventDefault();
        var password = $("#generatedPassword").val();
        $("#signupform-password").val(password);
        $("#signupform-c_password").val(password);
        $("#signupform-password").addClass("disabled");
        $("#signupform-c_password").addClass("disabled");
        
        $("#modal-default").modal('hide');
        //alert(password);
    });

    $("#passwordRefresh").click(function(event) {
        event.preventDefault();

        $("#modalContentDefault")
        .load($(this).attr('href'));
        $('h4#ModalHeaderID').text($(this).data('title'));
        $(".modal-dialog").css("width", $(this).data('width'));  
            
    });

    $('#check1').click(function() {
    if($(this).is(':checked'))
        $("#passwordInsert").removeClass('disabled');
    else
        $("#passwordInsert").addClass('disabled');
});

</script>