<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use backend\components\widgets\MaterialCheckbox;

$packages = new backend\models\Packages();
$period = new backend\models\Periodes();
$schemeOffers = new backend\models\schemeOffers();

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="accounts-create bg-fff">
  <div class="row">
    <div class="col-xs-12">
      <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
      <div class="nav-tabs-custom form-new">
          <ul class="nav nav-tabs">
              <li class="active" id="tab-h-1"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Basic Information</a></li>
              <li class="" id="tab-h-2"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Contact Information</a></li>
              <li class="" id="tab-h-3"><a href="#tab_3" data-toggle="tab" aria-expanded="false">Offer Information</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <?= $form->field($model, 'f_name')->textInput(['autofocus' => true,'maxlength' => true]) ?>
                      <?= $form->field($model, 'l_name')->textInput(['maxlength' => true]) ?>
                      <?php if($email!=null) { ?>
                      <?= $form->field($model, 'email')->textInput(['readOnly'=>true,'value'=>$email])?>
                      <?php } else {  ?>
                      <?= $form->field($model, 'email')?>
                      <?php } ?>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                      <?= Html::activeHiddenInput($model, 'user_type',['value'=>'user']) ;?>
                      <?= Html::activeHiddenInput($model, 'level',['value'=>'4']) ;?>
                      
                      <div class="form-group">
                          <?= Html::a('Generate Password', ['/accounts/password-generate'], ['class' => 'bootstrap-modal-new btn btn-primary pull-right', 'data-title' =>'Password Generate','data-width'=>'30%']) ?>
                      </div>
                      <div class="clearfix"> </div>
                          <?= $form->field($model, 'password')->passwordInput([]) ?>
                          <?= $form->field($model, 'c_password')->passwordInput([]) ?>
                  </div>
              </div>

              <div class="row">
                <div class="col-xs-12 col-sm-12">
                  <div class="form-group pull-right">
                    <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_2'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 1']);?>
          
                  </div>
                </div>
              </div>

            </div><!-- /.tab-pane -->
            <div class="tab-pane " id="tab_2">
              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                      <?php if($mobile!=null){ ?>
                       <?= $form->field($model, 'mobile')->textInput(['maxlength' => true,'readOnly'=>true,'value'=>$mobile]) ?>
                      <?php } else { ?>
                      <?= $form->field($model, 'mobile')->textInput(['maxlength' => true,]) ?>
                      <?php } ?>
                      <?= $form->field($model, 'pincode')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                      <?= $form->field($model, 'address')->textarea(['rows' => '5']) ?>
                  </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12">
                  <div class="form-group pull-right">
                    <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_1'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 2']); ?>
                    <?= Html::a('Next  <i class="fa fa-arrow-right" aria-hidden="true"></i>', ['#tab_3'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn next-pane 2']);?>
          
                  </div>
                </div>
              </div>
            </div><!-- /.tab-pane -->

            <div class="tab-pane " id="tab_3">
              <div class="row">
                  <div class="col-xs-12 col-sm-6">
                       
                       <?= $form->field($model, 'scheme')->widget(Select2::classname(), [
                          'data' => $packages->generatePackageLists(),
                          'language' => 'en',
                          'options' => ['placeholder' => 'select'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                        ]); ?>

                        <?= $form->field($model, 'period')->widget(Select2::classname(), [
                          'data' => $period->generatePeriodeLists(),
                          'language' => 'en',
                          'options' => ['placeholder' => 'select'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                        ]); ?>
                        <?= $form->field($model, 'offer')->widget(Select2::classname(), [
                          'data' => $schemeOffers->generateSchemeOffersLists(),
                          'language' => 'en',
                          'options' => ['placeholder' => 'select'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                        ]); ?>

                        <?= $form->field($model, 'valid_from')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Customer plan valid from'],
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'todayHighlight' => true,
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true,
                                'startDate' => date("yyyy-MM-dd H:i:s"),

                            ]
                        ]); ?>


                        
                  </div>
                  <div class="col-xs-12 col-sm-6">
                      
                    <div class="box box-info" id="loader-block">
                      <div class="box-header with-border">
                        <h3 class="box-title">Offer Summary</h3>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                        <div class="table-responsive">
                          <table class="table no-margin">
                           
                            <tbody>
                              <tr>
                                <td style="width:60%"><b>Free Month Bonus</b></td>
                                <td style="width:40%" id="monthBonusDetails"><span class="label label-success">Shipped</span></td>
                              </tr>
                              <tr>
                                <td style="width:60%"><b>Free Cash Bonus</b></td>
                                <td style="width:40%" id="cashBonusDetails"><span class="label label-success">Shipped</span></td>
                              </tr>
                              <tr>
                                <td style="width:60%"><b>Free Gold Bonus</b></td>
                                <td style="width:40%" id="goldBonusDetails"><span class="label label-success">Shipped</span></td>
                              </tr>
                              <tr>
                                <td style="width:60%"><b>Offer Availing on</b></td>
                                <td style="width:40%" id="offerAvailingOn"><span class="label label-success">Shipped</span></td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </div><!-- /.table-responsive -->
                      </div><!-- /.box-body -->
                    </div>
                     <?= $form->field($model, 'startup_payment')->dropDownlist(['0'=>'Yes','1'=>'No'],['prompt'=>'startup Payment']) ?>
                  </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12">
                  <div class="form-group pull-right">
                    <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_2'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 3']);?>
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary theme-btn', 'name' => 'signup-button']) ?>
                  </div>
                </div>
              </div>
            </div><!-- /.tab-pane -->
            
          </div><!-- /.tab-content -->
      </div>
      <?php ActiveForm::end(); ?>  
    </div>
  </div>
  
</div>

<script type="text/javascript">
$( document ).ready(function() {
  
   $("#loader-block").hide();

  function replaceDetails(data) {
      if(data.bonus_cash!=null) {
        $("#cashBonusDetails").html('<span class="label label-success">'+data.bonus_cash+' Ruppess  </span>');
      }
      else if (data.bonus_cash==null) {
        $("#cashBonusDetails").html('<span class="label label-danger">Not Available</span>');
      }
      if (data.bonus_gold!=null) {
          $("#goldBonusDetails").html('<span class="label label-success">'+data.bonus_gold+' Grams </span>');
      }
      else if (data.bonus_gold==null) {
          $("#goldBonusDetails").html('<span class="label label-danger">Not Available</span>');
      }
      if (data.bonus_month==null) {
          $("#monthBonusDetails").html('<span class="label label-danger">Not Available </span>');
      }
      else if (data.bonus_month=='1') {
          $("#monthBonusDetails").html('<span class="label label-success"> 1 Month </span>');
      }
      else if (data.bonus_month!=null) {
          $("#monthBonusDetails").html('<span class="label label-success">'+data.bonus_month+' Months</span>');
      }
      if(data.offer_availing_on=='anytime') {
        $("#offerAvailingOn").html('<span class="label label-info">Anytime</span>');
      }
      else if (data.offer_availing_on=='end_of') {
        $("#offerAvailingOn").html('<span class="label label-info">End of period </span>');
      }
  }

   $("#signupform-offer").change(function() {
        $("#loader-block").show();
        run_waitMe();
        var offerId = $(this).val();
        
        if(offerId==null) {
          $(".waitMe").hide();
          $("#loader-block").hide();
        }

        else {

          $.ajax({
                method: 'POST',
                url: '/accounts/get-offer-details',
                crossDomain: false,
                data:{'offerId':offerId},
                success: function(responseData, textStatus, jqXHR) {
                    var obj = JSON.parse(responseData);
                    replaceDetails(obj);
                    $(".waitMe").hide();
                            
            },
            error: function (responseData, textStatus, errorThrown) {
               $(".waitMe").hide();
               $("#loader-block").hide();
                
            },
        }); 

        }

         
    });



   $(".next-pane.1").click(function() {
       $("#tab-h-1").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });
   $(".next-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-3").addClass("active"); 
   });
   
   $(".previous-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-1").addClass("active"); 
   });
   $(".previous-pane.3").click(function() {
       $("#tab-h-3").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });


   function run_waitMe(effect){
    $('#loader-block').waitMe({

    //none, rotateplane, stretch, orbit, roundBounce, win8, 
    //win8_linear, ios, facebook, rotation, timer, pulse, 
    //progressBar, bouncePulse or img
    effect: 'roundBounce',

    //place text under the effect (string).
    text: 'Offer Details...',

    //background for container (string).
    bg: 'rgba(255,255,255,0.7)',

    //color for background animation and text (string).
    color: '#00aff0',

    //change width for elem animation (string).
    sizeW: '',

    //change height for elem animation (string).
    sizeH: '',

    // url to image
    source: '',

    // callback
    onClose: function() {}

    });
  }
  
});
</script>


<style type="text/css">
  .tab-pane {
    padding: 10px 10px;
}
</style>