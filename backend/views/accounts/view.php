<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12 col-sm-8">
        <div class="box box-widget widget-user">
                
            <div class="widget-user-header bg-navy-active">
              <h3 class="widget-user-username"><?= $model->f_name.' '.$model->l_name ?></h3>
              <h5 class="widget-user-desc"><?= $model->mobile ?></h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="/themes/adminlte/dist/img/avatar5.png" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"> <?=$dataProvider->getCount()?> </h5>
                    <span class="description-text"> Scheme Joins </span>
                  </div><!-- /.description-block -->
                </div><!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"> 
                        <?=money_format('%i',$userSummaryInfo['schemeInvestment'])?> </h5>
                    <span class="description-text">Ruppes Invested in Schemes</span>
                  </div><!-- /.description-block -->
                </div><!-- /.col -->


                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header"> <?= number_format($userSummaryInfo['totalInvestment'],2)?> </h5>
                    <span class="description-text">Ruppes Invested in General </span>
                  </div><!-- /.description-block -->
                </div><!-- /.col -->

              </div><!-- /.row -->
              <hr/>
                <div class="col-xs-12">
                    <div class="navbar navbar-default navbar-component navbar-xs">
                        <ul class="nav navbar-nav visible-xs-block">
                            <li class="full-width" style="padding-left: 15px;">
                                <a data-target="#navbar-filter-profile" data-toggle="collapse" class="collapsed" aria-expanded="false">
                                    <i class="fa fa-bars"></i> Menu
                                </a>
                            </li>
                        </ul>
                        <div id="navbar-filter-profile" class="navbar-collapse collapse custom-tab">
                            <ul class="nav navbar-nav ">
                                <li class="active"><a href="#tab_investments" data-toggle="tab" aria-expanded="false"><i class="fa fa-users" aria-hidden="true"></i> Investments </a></li>
                                <li class=""><a href="#tab_schemes" data-toggle="tab" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i> Scheme Joins</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                    
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_investments">
                        <div class="row">
                            <div class="col-xs-12">
                                 <?= ListView::widget([
                                    'dataProvider' => $investmentDataProvider,
                                    'layout' => "{summary}\n{items}\n<div class='pull-right'>{pager}</div>",
                                    'itemOptions' => ['class' => 'item'],
                                    'itemView' => '_investment-list',
                                    'pager' => [
                                        'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                                        'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
                                    ],
                                    //'itemView' => function ($model, $key, $index, $widget) {
                                    //    return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
                                   // },
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane " id="tab_schemes">
                       <div class="row">
                            <div class="col-xs-12">
                                <div style="padding: 15px">
                                     <?= ListView::widget([
                                            'dataProvider' => $dataProvider,
                                            'layout' => "{summary}\n{items}\n<div class='pull-right'>{pager}</div>",
                                            'itemOptions' => ['class' => 'item'],
                                            'itemView' => '_scheme-list',
                                            'pager' => [
                                                'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                                                'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
                                            ],
                                            //'itemView' => function ($model, $key, $index, $widget) {
                                            //    return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
                                           // },
                                        ]) ?>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>


            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-4">
        <div class="row">
            <div class="col-xs-12">
                <div class="btn-group pull-left">
                    <?=Html::a('<i class="fa fa-plus"></i> New Scheme',['/accounts/join-new-scheme','user_id'=>$model->id],['class'=>'btn btn-app theme-btn btn-app-bg-1'])?>
                    
                </div>
            </div>
        </div>
        <div class="row bg-fff">
            <?php $form = ActiveForm::begin([
                'action' => ['/accounts/update-user-details'],
                'id' => 'userConfigurationEdit'
            ]);


             $model->b2cAddressModel->roots = explode(', ',$model->b2cAddressModel->roots);
            ?>
            <?= $form->field($model,'id')->hiddenInput()->label(false)?>
            <?= $form->field($model->b2cAddressModel, 'roots')->widget(Select2::classname(), [
                'data' => $model->loadRootList(),
                'language' => 'en',
                'options' => ['placeholder' => 'Roots'],
                'pluginOptions' => [
                    'multiple' => false,
                    'allowClear' => true,
                ],
            ]); ?>
             <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary theme-btn pull-right']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

               
    </div>

</div>





<?php /*
<div class="row">

 <div class="col-xs-12 col-sm-6">
        <div class="item panel panel-default"><!-- widgetBody -->
            <div class="panel-heading">
                <h3 class="panel-title pull-left">Account Details</h3>
                <div class="pull-right">
                    <div class="btn-group pull-right">
                       
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary theme-btn']) ?>
                        <?= Html::a('Suspend', ['update', 'id' => $model->id], ['class' => 'btn btn-danger theme-btn']) ?>
                        <!-- <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger theme-btn',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?> -->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                
                <div class="clients-view">
                      <?= DetailView::widget([
					        'model' => $model,
					        'attributes' => [
					            'id',
					            'username',
					            'f_name',
                                'l_name',
                                'mobile',
                                'email',
                                [
                                'attribute' => 'status',
                                'value' => function($model) { if($model->status==10) return'Active'; else return 'Inactive'; },
                                ],
					           
					        ],
					    ]) ?>  

                </div>
                
            </div>
        </div>
    </div>

</div>





<div class="user-view">
   
    

</div>
*/

?>


<style type="text/css">
    .investment-box {
    background: #7f96963d;
    padding: 10px;
    margin: 5px 15px;
}

.investment-box span {
    font-style: italic;
    color: #a94442;
}
.investment-box .body {
    text-align: right;
    margin-top: -43px;
    color: #a94442;
}
</style>