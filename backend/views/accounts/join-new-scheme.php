<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use backend\components\widgets\MaterialCheckbox;

$packages = new backend\models\Packages();
$period = new backend\models\Periodes();
$schemeOffers = new backend\models\schemeOffers();

$this->title = 'Join New Scheme';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
<div class="nav-tabs-custom form-new bg-fff">
    <ul class="nav nav-tabs">
        <li class="active" id="tab-h-1"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Offer Information</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                 
                 <?= $form->field($model, 'scheme')->widget(Select2::classname(), [
                    'data' => $packages->generatePackageLists(),
                    'language' => 'en',
                    'options' => ['placeholder' => 'select'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                  ]); ?>

                  <?= $form->field($model, 'period')->widget(Select2::classname(), [
                    'data' => $period->generatePeriodeLists(),
                    'language' => 'en',
                    'options' => ['placeholder' => 'select'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                  ]); ?>
                  <?= $form->field($model, 'offer')->widget(Select2::classname(), [
                    'data' => $schemeOffers->generateSchemeOffersLists(),
                    'language' => 'en',
                    'options' => ['placeholder' => 'select'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                  ]); ?>

                  <?= $form->field($model,'user_id')->hiddenInput(['value'=>$userId])->label(false)?>

                  <?= $form->field($model, 'valid_from')->widget(DatePicker::classname(), [
                  'options' => ['placeholder' => 'Customer plan valid from'],
                      'pluginOptions' => [
                          'autoclose'=>true,
                          'todayHighlight' => true,
                          'format' => 'yyyy-mm-dd',
                          'autoclose' => true,
                          'startDate' => date("yyyy-MM-dd H:i:s"),

                      ]
                  ]); ?>


                  
            </div>
            <div class="col-xs-12 col-sm-6">
                
              <div class="box box-info" id="loader-block">
                <div class="box-header with-border">
                  <h3 class="box-title">Offer Summary</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                     
                      <tbody>
                        <tr>
                          <td style="width:60%"><b>Free Month Bonus</b></td>
                          <td style="width:40%" id="monthBonusDetails"><span class="label label-success">Shipped</span></td>
                        </tr>
                        <tr>
                          <td style="width:60%"><b>Free Cash Bonus</b></td>
                          <td style="width:40%" id="cashBonusDetails"><span class="label label-success">Shipped</span></td>
                        </tr>
                        <tr>
                          <td style="width:60%"><b>Free Gold Bonus</b></td>
                          <td style="width:40%" id="goldBonusDetails"><span class="label label-success">Shipped</span></td>
                        </tr>
                        <tr>
                          <td style="width:60%"><b>Offer Availing on</b></td>
                          <td style="width:40%" id="offerAvailingOn"><span class="label label-success">Shipped</span></td>
                        </tr>
                        
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
              </div>
               <?= $form->field($model, 'startup_payment')->dropDownlist(['0'=>'Yes','1'=>'No'],['prompt'=>'startup Payment']) ?>
            </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <div class="form-group pull-right">
              <?= Html::submitButton('Signup', ['class' => 'btn btn-primary theme-btn', 'name' => 'signup-button']) ?>
            </div>
          </div>
        </div>

      </div><!-- /.tab-pane -->
      
      
    </div><!-- /.tab-content -->
</div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
$( document ).ready(function() {
  
   $("#loader-block").hide();

  function replaceDetails(data) {
      if(data.bonus_cash!=null) {
        $("#cashBonusDetails").html('<span class="label label-success">'+data.bonus_cash+' Ruppess  </span>');
      }
      else if (data.bonus_cash==null) {
        $("#cashBonusDetails").html('<span class="label label-danger">Not Available</span>');
      }
      if (data.bonus_gold!=null) {
          $("#goldBonusDetails").html('<span class="label label-success">'+data.bonus_gold+' Grams </span>');
      }
      else if (data.bonus_gold==null) {
          $("#goldBonusDetails").html('<span class="label label-danger">Not Available</span>');
      }
      if (data.bonus_month==null) {
          $("#monthBonusDetails").html('<span class="label label-danger">Not Available </span>');
      }
      else if (data.bonus_month=='1') {
          $("#monthBonusDetails").html('<span class="label label-success"> 1 Month </span>');
      }
      else if (data.bonus_month!=null) {
          $("#monthBonusDetails").html('<span class="label label-success">'+data.bonus_month+' Months</span>');
      }
      if(data.offer_availing_on=='anytime') {
        $("#offerAvailingOn").html('<span class="label label-info">Anytime</span>');
      }
      else if (data.offer_availing_on=='end_of') {
        $("#offerAvailingOn").html('<span class="label label-info">End of period </span>');
      }
  }

   $("#signupform-offer").change(function() {
        $("#loader-block").show();
        run_waitMe();
        var offerId = $(this).val();
        
        if(offerId==null) {
          $(".waitMe").hide();
          $("#loader-block").hide();
        }

        else {

          $.ajax({
                method: 'POST',
                url: '/accounts/get-offer-details',
                crossDomain: false,
                data:{'offerId':offerId},
                success: function(responseData, textStatus, jqXHR) {
                    var obj = JSON.parse(responseData);
                    replaceDetails(obj);
                    $(".waitMe").hide();
                            
            },
            error: function (responseData, textStatus, errorThrown) {
               $(".waitMe").hide();
               $("#loader-block").hide();
                
            },
        }); 

        }

         
    });




   function run_waitMe(effect){
    $('#loader-block').waitMe({

    //none, rotateplane, stretch, orbit, roundBounce, win8, 
    //win8_linear, ios, facebook, rotation, timer, pulse, 
    //progressBar, bouncePulse or img
    effect: 'roundBounce',

    //place text under the effect (string).
    text: 'Offer Details...',

    //background for container (string).
    bg: 'rgba(255,255,255,0.7)',

    //color for background animation and text (string).
    color: '#00aff0',

    //change width for elem animation (string).
    sizeW: '',

    //change height for elem animation (string).
    sizeH: '',

    // url to image
    source: '',

    // callback
    onClose: function() {}

    });
  }
  
});
</script>