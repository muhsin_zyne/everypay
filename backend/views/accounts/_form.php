<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
  
      <div class="row">

        <?php $form = ActiveForm::begin(); ?>

            <div class="col-xs-12 col-sm-6">

                <?= $form->field($model, 'username')->textInput(['maxlength' => true ,'readonly' => true]) ?>

                <?= $form->field($model, 'f_name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'l_name')->textInput(['maxlength' => true ]) ?>
            </div>
            
            <div class="col-xs-12 col-sm-6">

                <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>    

                 <?= $form->field($model, 'status')->dropDownList([ '10'=>'Active', '1'=>'Inactive', ], ['prompt' => 'status']) ?>
                 

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary theme-btn pull-right']) ?>
                </div>

            </div>

        <?php ActiveForm::end(); ?>

      </div>
</div>
