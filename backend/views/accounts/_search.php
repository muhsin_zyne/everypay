<?php

use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\models\search\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search search-box-header">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'accountSearchForm'
    ]); ?>

    <div class="col-xs-12 col-sm-3">
        <?= $form->field($model, 'f_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-12 col-sm-3">
        <?= $form->field($model, 'l_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-12 col-sm-2">
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-xs-12 col-sm-4 pull-right">

        <?= $form->field($model, 'date_range', [
            'options'=>['class'=>'drp-container form-group']
            ])->widget(DateRangePicker::classname(), [
            'presetDropdown'=>true,
            'hideInput'=>true,
            'pluginOptions' => [
                    'locale'=>[
                    'format'=>'MM/DD/YYYY',
                ],
                'opens'=>'left'
                ],
            ]); 
        ?>
    </div>  
    <?php ActiveForm::end(); ?>
    <div class="clearfix"> </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $("#accountSearchForm input").change(function() {
            $('#accountSearchForm').submit();
        });
    });
</script>

<style type="text/css">
    .search-box-header {
  background: #bfbfbf !important;
  margin: -10px 5px;
  margin-bottom: 10px;
  padding-top: 5px;
}
</style>
