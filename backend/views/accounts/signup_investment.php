<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use backend\components\widgets\MaterialCheckbox;

$packages = new backend\models\Packages();
$period = new backend\models\Periodes();
$schemeOffers = new backend\models\SchemeOffers();

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="accounts-create bg-fff">
<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
   <div class="row"> 
    <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'f_name')->textInput(['autofocus' => true,'maxlength' => true]) ?>
        
        <?php if($mobile!=null){ ?>
         <?= $form->field($model, 'mobile')->textInput(['maxlength' => true,'readOnly'=>true,'value'=>$mobile]) ?>
        <?php } else { ?>
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true,]) ?>
        <?php } ?>
        <?= $form->field($model, 'address')->textarea(['rows' => '5']) ?>
        <?= $form->field($model, 'location')->textInput([]) ?>

          <?= $form->field($model, 'valid_from')->widget(DatePicker::classname(), [
          'options' => ['placeholder' => 'Customer plan valid from'],
              'pluginOptions' => [
                  'autoclose'=>true,
                  'todayHighlight' => true,
                  'format' => 'yyyy-mm-dd',
                  'autoclose' => true,
                  'startDate' => date("yyyy-MM-dd H:i:s"),

              ]
          ]); ?>
           <div class="form-group">
            <?= Html::a('Generate Password', ['/accounts/password-generate'], ['class' => 'bootstrap-modal-new btn btn-primary pull-right', 'data-title' =>'Password Generate','data-width'=>'30%']) ?>
        </div>
        <div class="clearfix"> </div>


    </div>
    <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'l_name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'pincode')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'investment_amount')->textInput(['type' =>'number']) ?> 
        <?= $form->field($model, 'reccoring_type')->dropDownlist(['day'=>'Daily','week'=>'Weekly','month'=>'Monthly'],['prompt'=>'Recorring Type']) ?> 
        <?= Html::activeHiddenInput($model, 'user_type',['value'=>'user']) ;?>
         <?= Html::activeHiddenInput($model, 'email')?>
        <?= Html::activeHiddenInput($model, 'level',['value'=>'4']) ;?>
        <?= $form->field($model, 'password')->passwordInput([]) ?>
        <?= $form->field($model, 'c_password')->passwordInput([]) ?>
    </div>

    <div class="col-xs-12 col-sm-12">
      <div class="form-group pull-right">
        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> Previous', ['#tab_2'], ['data-toggle'=>'tab','aria-expanded'=>'false','class' => 'btn btn-danger theme-btn previous-pane 3']);?>
        <?= Html::submitButton('Signup', ['class' => 'btn btn-primary theme-btn', 'name' => 'signup-button']) ?>
      </div>
    </div>
  </div>
  <?php ActiveForm::end(); ?>  
</div>

<script type="text/javascript">
$( document ).ready(function() {
  
   $("#loader-block").hide();

   $(".next-pane.1").click(function() {
       $("#tab-h-1").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });
   $(".next-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-3").addClass("active"); 
   });
   
   $(".previous-pane.2").click(function() {
       $("#tab-h-2").removeClass("active");
       $("#tab-h-1").addClass("active"); 
   });
   $(".previous-pane.3").click(function() {
       $("#tab-h-3").removeClass("active");
       $("#tab-h-2").addClass("active"); 
   });


   function run_waitMe(effect){
    $('#loader-block').waitMe({

    //none, rotateplane, stretch, orbit, roundBounce, win8, 
    //win8_linear, ios, facebook, rotation, timer, pulse, 
    //progressBar, bouncePulse or img
    effect: 'roundBounce',

    //place text under the effect (string).
    text: 'Offer Details...',

    //background for container (string).
    bg: 'rgba(255,255,255,0.7)',

    //color for background animation and text (string).
    color: '#00aff0',

    //change width for elem animation (string).
    sizeW: '',

    //change height for elem animation (string).
    sizeH: '',

    // url to image
    source: '',

    // callback
    onClose: function() {}

    });
  }
  
});
</script>


<style type="text/css">
  .tab-pane {
    padding: 10px 10px;
}
</style>