<?php 
use yii\helpers\Html;

$completedScale = $model->paid_duration/$model->duration*100;

if($completedScale<=29) {
	$progressBarHtml='<div class="progress-bar progress-bar-red" style="width:'.$completedScale.'%"></div>';
}
else if($completedScale<=49 && $completedScale>30) {
	$progressBarHtml='<div class="progress-bar progress-bar-yellow" style="width:'.$completedScale.'%"></div>';
}
else if ($completedScale<=69 && $completedScale>50) {
	$progressBarHtml='<div class="progress-bar progress-bar-aqua" style="width:'.$completedScale.'%"></div>';
}
else if ($completedScale>70) {
	$progressBarHtml='<div class="progress-bar progress-bar-green" style="width:'.$completedScale.'%"></div>';
}
?>

<div class="progress-group">
    <span class="progress-text"> <?=$model->plan_label?></span>
 	<span class="progress-number"><b><?=$model->paid_duration?>/<?= $model->duration.'</b> Payment Paid'?></span>
 	<div class="progress sm">
      <?=$progressBarHtml?>
   	</div>
   	<div class="bn btn-group pull-right">
   		<?= Html::a('Print', ['/accounts/recipt','join_id'=>$model->id], ['target'=>'_blank','class' => 'btn theme-btn bg-maroon']) ?>
   		<?= Html::a('view', ['/scheme-joins/view','join_id'=>$model->id], ['target'=>'_blank','class' => 'btn theme-btn btn-primary']) ?>
  		
   	</div>
   	<div class="clearfix"> </div>
</div>