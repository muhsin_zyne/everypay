<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
  
      <div class="row">
        <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'id' => 'ajax'
        ]); ?>

            <div class="col-xs-12"><!-- 
               <?= $form->field($model, 'type')->radioList(['1'=>'Mobile Number','2'=>'Email Account'],['class'=>'']); ?> -->
                <?= $form->field($model,'type')->dropDownlist([0=>'Mobile Number',1=>'Email Account'],[]) ?>
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
                 <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            

                <div class="form-group">
                    <?= Html::submitButton('Validate User', ['class' =>  'btn btn-primary theme-btn pull-right']) ?>
                </div>

            </div>

        <?php ActiveForm::end(); ?>

      </div>
</div>
<script type="text/javascript">
    $( document).ready(function() { 
        $("div.form-group.field-uservalidation-email").hide();
        $("#uservalidation-type").change(function() {
            var item = $("#uservalidation-type").val();
            if(item==0) {
              $("div.form-group.field-uservalidation-email").hide();  
              $("div.form-group.field-uservalidation-mobile").show();  
            }
            else if (item==1) {
                $("div.form-group.field-uservalidation-email").show();  
                $("div.form-group.field-uservalidation-mobile").hide();  
            }
        });
    });

</script>