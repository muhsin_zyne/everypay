<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use backend\models\User;
use backend\components\TestMenu;
use kartik\sidenav\SideNav;
use backend\config\Menu;
use yii\bootstrap;
use backend\components\widgets\CustomModal;

DashboardAsset::register($this);

$activeuser =  User::find()->where(['id' =>yii::$app->user->id])->one();
if($activeuser->image=="") {
    $activeuser->image = 'avatar-01.png';
}
$currentPage = yii::$app->controller->id.'/'.yii::$app->controller->action->id;
$controllerId = Yii::$app->controller->id;
$actionId = Yii::$app->controller->action->id;
$requestUrl = yii::$app->request->url;
$item = $currentPage;

if(yii::$app->user->isGuest) {
  die("403 Not Allowded");
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/jquery.form.js"></script>
    


    <?php $this->head() ?>
</head>

<body class="hold-transition skin-black sidebar-mini">
<?php $this->beginBody() ?>
<div class="se-pre-con">



  <svg class="progress-circular">
    <circle class="progress-circular__primary" cx="50%" cy="50%" r="30%" fill="none" stroke-width="9%" stroke-miterlimit="10"/>
  </svg>


</div>
<div class="wrapper">

        

                  <header class="main-header">
                    <!-- Logo -->
                    
                    <!-- Header Navbar: style can be found in header.less -->
                    <nav class="navbar navbar-static-top" role="navigation">
                      <!-- Sidebar toggle button-->
                      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                      </a>
                      <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                          
                          <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                              <img src="/drive/uploads/avatar/<?=$activeuser->image;?>" class="user-image" alt="User Image">
                              <span class="hidden-xs"> <?= $activeuser->display_name; ?> </span>
                            </a>
                            <ul class="dropdown-menu">
                              <!-- User image -->
                              <li class="user-header">
                                <img src="/drive/uploads/avatar/<?=$activeuser->image;?>" class="img-circle" alt="User Image">
                                <p>
                                  <?= $activeuser->display_name.' - ('.$activeuser->user_type.')'; ?>
                                  <small><?= $activeuser->email; ?></small>
                                </p>
                              </li>
                              
                              <li class="user-footer">
                                <div class="pull-left">
                                  <a href="<?= Url::to(['/profile']);?>" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                   <a href="<?= Url::to(['/site/logout']);?>" class="btn btn-default btn-flat"> Logout </a>
                                 
                                </div>
                              </li>
                            </ul>
                          </li>
                          <!-- Control Sidebar Toggle Button -->
                          
                        </ul>
                      </div>
                    </nav>
                  </header>





                  <aside class="main-sidebar">
                    <section class="sidebar">
                      <div class="user-panel">
                        <div class="pull-left image">
                          <img src="/drive/uploads/avatar/<?=$activeuser->image;?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                          <p> <?= $activeuser->display_name; ?> </p>
                          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                      </div>
                      <!-- search form -->
                      <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                          <input type="text" name="q" class="form-control" placeholder="Search...">
                          <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                          </span>
                        </div>
                      </form>
                      

                        <?php 


                        $type = 'SideNav::TYPE_DEFAULT';
                        echo SideNav::widget([
                        'type' => $type,
                        'encodeLabels' => false,
                        'heading' => '',
                        'items' =>[
                            
                                      [
                                        'label' => 'Home', 
                                        'icon' => 'home', 
                                        'visible'=>yii::$app->controller->isVisible('Index'),
                                        'url' => Url::to(['/site/index']),
                                        'active' => ($currentPage == 'site/index')
                                      ],

                                      [
                                        'label' => 'Customer Accounts', 
                                        'icon' => 'users', 
                                        'visible'=>yii::$app->controller->isVisible('Customer Accounts'),
                                        'url' => Url::to(['/accounts/index']),
                                        'active' => ($controllerId == 'accounts'),
                                        'items' =>[  
                                                    [
                                                      'label' => 'All Accounts',
                                                      'url' => Url::to(['/accounts/index']), 
                                                      'active' => ($currentPage == 'accounts/index')
                                                    ],
                                                    [
                                                      'label' => 'Create New',
                                                      'url' => Url::to(['/accounts/index']), 
                                                      'active' => ($currentPage == 'accounts/create'),
                                                      'class'=>'bootstrap-modal-new',
                                                    ],
                                                  ],
                                      ],

                                      [
                                        'label' => 'Scheme Joins', 
                                        'icon' => 'sort-alpha-asc', 
                                        'visible'=>yii::$app->controller->isVisible('Scheme Joins'),
                                        'url' => Url::to(['/scheme-joins/index']),
                                        'active' => ($controllerId == 'scheme-joins'),
                                        'items' =>[  
                                                    [
                                                      'label' => 'All scheme-joins',
                                                      'url' => Url::to(['/scheme-joins/index']), 
                                                      'active' => ($currentPage == 'scheme-joins/index')
                                                    ],
                                                  ],
                                      ],

                                       [
                                        'label' => 'Investment Joins', 
                                        'icon' => 'sort-alpha-asc', 
                                        'visible'=>yii::$app->controller->isVisible('Investment Joins'),
                                        'url' => Url::to(['/investment-joins/index']),
                                        'active' => ($controllerId == 'investment-joins'),
                                        'items' =>[  
                                                    [
                                                      'label' => 'All Investment-joins',
                                                      'url' => Url::to(['/investment-joins/index']), 
                                                      'active' => ($currentPage == 'investment-joins/index')
                                                    ],
                                                  ],
                                      ],


                                      [
                                        'label' => 'Configurations', 
                                        'icon' => 'cogs', 
                                        'visible'=>yii::$app->controller->isVisible('Configurations'),
                                        'url' => '#',
                                        'active' => in_array($controllerId,['packages','periodes','scheme-offers','collection-roots','sms-configurations','sms-templates']),
                                        'items' =>[  
                                                    [
                                                      'label' => 'Offers',
                                                      'icon' => 'suitcase',
                                                      'url' => Url::to(['#']),
                                                      'visible'=>yii::$app->controller->isVisible('Offers'), 
                                                      'active' => ($controllerId == 'scheme-offers'),
                                                      'items' => [
                                                          [
                                                            'label' => 'Offers',
                                                            'url' => Url::to(['/scheme-offers/index']),
                                                            'visible'=>yii::$app->controller->isVisible('Offers'), 
                                                            'active' => ($controllerId == 'scheme-offers'),
                                                          ],
                                                      ],
                                                    ],

                                                    [
                                                        'label' => 'Collection Roots', 
                                                        'icon' => 'road', 
                                                        'visible'=>yii::$app->controller->isVisible('Collection Roots'),
                                                        'url' => Url::to(['/collection-roots/index']),
                                                        'active' => ($controllerId == 'collection-roots'),
                                                      ],


                                                    [
                                                      'label' => 'Packages',
                                                      'url' => Url::to(['/packages/index']),
                                                      'visible'=>yii::$app->controller->isVisible('Packages'), 
                                                      'active' => ($currentPage == 'packages/index'),
                                                    ],

                                                    [
                                                      'label' => 'Periodes',
                                                      'url' => Url::to(['/periodes/index']),
                                                      'visible'=>yii::$app->controller->isVisible('Periodes'), 
                                                      'active' => ($currentPage == 'periodes/index'),
                                                    ],

                                                    [
                                                      'label' => 'SMS Configuration',
                                                      'url' => Url::to(['/sms-configurations/index']),
                                                      'visible'=>yii::$app->controller->isVisible('SMS Configuration'), 
                                                      'active' => ($currentPage == 'sms-configurations/index'),
                                                    ],

                                                    [
                                                      'label' => 'SMS Templates',
                                                      'url' => Url::to(['/sms-templates/index']),
                                                      'visible'=>yii::$app->controller->isVisible('SMS Templates'), 
                                                      'active' => ($currentPage == 'sms-templates/index'),
                                                    ],
                                                    
                                                    
                                                    
                                                  ],
                                      ],



                                      
                                    /* [
                                        'label' => 'Attribute-set', 
                                        'icon' => 'bars',
                                        'visible'=>yii::$app->controller->isVisible('Attribute-set'),
                                        'url' => Url::to(['/attribute-set/index']),
                                        'active' => ($currentPage == 'attribute-set/index')
                                      ],
                                      
                                      [
                                        'label' => 'Configurations',
                                        'icon' => 'cogs',
                                        'visible'=>yii::$app->controller->isVisible('Configurations'),
                                        'active'=>($controllerId=='configuration'),
                                        'items' =>[
                                                      [
                                                        'label' => 'General Configuration',
                                                        'url' => Url::to(['/configuration/general']), 
                                                        'active' => ($currentPage == 'configuration/general')
                                                      ],

                                                  ],
                                       
                                      ],

                                      */


                                      
                                  ],
                      ]); ?>

                      



                    </section>
                  </aside>

         

        

          <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1 class="capitalise">
                <a href="" id="browserback" class="navigation"> <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back </a>
                <?= Html::encode($this->title); ?>
                <small class="capitalise">   <?= $controllerId ?>  </small>
              </h1>
            </section>


            <section class="content">
               <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
               <div class="box-layout">

                  <?= skinka\widgets\gritter\AlertGritterWidget::widget() ?>
                  <?= $content ?>

               </div>
            </section>
        </div>

    <footer class="main-footer">
            <div class="pull-right hidden-xs">
              <b>Version</b> 2.3.0
            </div>
            <strong>Copyright &copy; <?= date('Y');?><a href="http://www.rangetech.in" target="_blank"> RangeTech Solutions</a>.</strong> All rights reserved.
    </footer>
</div>

 <?= CustomModal::widget([
            'header' => 'DemoMenu',
            'id' => 'modal-default',
            'body' =>'<div id="modalContentDefault"> 
                      <svg class="progress-circular">
                      <circle class="progress-circular__primary" cx="50%" cy="50%" r="25%" fill="none" stroke-width="9%" stroke-miterlimit="10"/>
                  </svg>
                  </div>',

        ]); 
  ?>


  <?= CustomModal::widget([
            'header' => 'DemoMenu2',
            'id' => 'modal-pop-select',
            'size' =>'model-xs',
            'body' =>'<div id="modalContentpop"> 
                      
                  </div>',

        ]); 
  ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script src="/js/signature_pad.js"></script>
<script>
//paste this code under the head tag or in a separate js file.
  // Wait for window load
  $(window).load(function() {
    // Animate loader off screen
    
    $(".se-pre-con").fadeOut();
  });
</script>
<script>
$(function () {
    $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
         $( ".picker" ).each(function() {
            $( this ).datepicker({
            dateFormat : 'dd-mm-yy',
            language : 'en',
            changeMonth: true,
            changeYear: true
          });
        });
    });
});
</script>

<script> 
$( document).ready(function() {

   $(".bootstrap-modal-new").on('click', function(event){
        event.preventDefault();
        $("#modal-default").modal('show')
        .find("#modalContentDefault")
        .load($(this).attr('href'));
        $('h4#ModalHeaderID').text($(this).data('title'));
        $(".modal-dialog").css("width", $(this).data('width'));
   });


  var screenHeight = $(window).height();
  $ ( '.custom-nav' ).slimScroll({
        height: screenHeight-100

    });

  $("#browserback").click(function(e) {
    e.preventDefault();
    window.history.back();
  });
  
}); 
</script>


<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>