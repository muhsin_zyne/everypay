<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = 'SMS Configurations';
$this->params['breadcrumbs'][] = 'Configurations'; 
$this->params['breadcrumbs'][] = $this->title; 

?>

<div class="sms-configurations bg-fff">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-info-box">
				<p>  <i class="fa fa-info" aria-hidden="true"></i> Want to manage the existing SMS template or want to create the custom template ? <?=Html::a('Click Here', ['/sms-templates/index'],['class'=> ''])?>  </p>
			</div>
		</div>
	</div>
	<?php $form = ActiveForm::begin(); ?>
	<?php foreach ($smsConfigData as $key =>  $item) { ?>
	<div class="row">
		<div class="col-xs-6">

			<?= $form->field($item, 'templeate_id['.$item->id.']')->widget(Select2::classname(), [
			    'data' => $smsConfig->getTempleatesList(),
			    'options' => ['placeholder' => 'select', 'class'=> 'controller-input', 	'data-viewid'=>$item->id, 'value'=> $item->templeate_id],
			    'pluginOptions' => [
			        'allowClear' => true
			    ],
			])->label($item->label) ?>
		</div>
		<div class="col-xs-6">
			<div class="messag-view">
				<p id="view<?=$item->id?>"> <?php if($item->templateModel) { 
					echo $item->templateModel->content;
				}
				?> </p> 
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group pull-right">
		        <?= Html::submitButton($smsConfigData[0]->isNewRecord ? 'Create' : 'Update', ['class' => $smsConfigData[0]->isNewRecord ? 'btn btn-success' : 'btn btn-primary theme-btn pull-right']) ?>
		    </div>
		    <div class="clearfix"> </div>
		</div>
	</div>
	<?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$(".controller-input").change(function () {
			var id = $(this).val();
			var viewId = $(this).attr('data-viewid');
			$.ajax({
                method: 'GET',
                dataType: 'json',
                url: '/sms-configurations/view-sms-template?id=' + id,
                crossDomain: false,
                success: function(responseData, textStatus, jqXHR) {
                    $("#view" + viewId).text(''+ responseData.data.content+'');
                            
            	},
	            error: function (responseData, textStatus, errorThrown) {
		        },
	        }); 
			
			// alert("changed");
		});
	});
</script>

<style type="text/css">
	.messag-view {
	    padding: 15px;
	    background: #dde1e2;
	    margin: 10px;
	    border-radius: 11px;
	}
</style>