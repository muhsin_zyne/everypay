<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Activities */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activities-view">
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'activity_data:ntext',
            'location',
        ],
    ]) ?>

</div>
