<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Activities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activities-form">
	<?php $form = ActiveForm::begin([
	    	'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'id' => 'ajax'

    ]); ?>
	<div class="row">
		<div class="col-xs-12">
			<?= $form->field($model, 'activity_data')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

			<div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?>
		    </div>
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
