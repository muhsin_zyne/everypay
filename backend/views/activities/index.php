<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ActivitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activities-index">
    <div class="row">
        <div class="col-xs-12">
            <p> <?= Html::a('Create Activity Record', ['create'], ['class' => 'btn btn-success theme-btn pull-right bootstrap-modal-new','data-title'=>'Create Activity Record','data-width'=>'70%']) ?> </p>
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'activity_data:ntext',
                    'location',
                    ['class' => 'yii\grid\ActionColumn',   
                        'header'=>'Action',
                        'template'=>'{view}{update}',
                        'buttons'=>[
                          'update' => function ($url, $model) {    
                                return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'Edit Activity',                                           
                                        'data-width'=>'70%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'Edit ',
                                    ]); 
                                },
                            'view' => function($url,$model) {
                                return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'View Activity',                                           
                                        'data-width'=>'70%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'View',
                                    ]); 

                            },
                        ]                            
                ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>

        </div>
    </div>
</div>
