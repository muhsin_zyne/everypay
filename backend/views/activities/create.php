<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Activities */

$this->title = 'Create Activities';
$this->params['breadcrumbs'][] = ['label' => 'Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activities-create">
	
	<?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
