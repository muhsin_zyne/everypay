<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\InvestmentJoins */

$this->title = 'Create Investment Joins';
$this->params['breadcrumbs'][] = ['label' => 'Investment Joins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investment-joins-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
