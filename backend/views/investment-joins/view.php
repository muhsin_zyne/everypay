<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\components\ViewFunction;

$view = new ViewFunction();

/* @var $this yii\web\View */
/* @var $model backend\models\InvestmentJoins */

$this->title = $model->plan_label;
$this->params['breadcrumbs'][] = ['label' => 'Investment Joins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investment-joins-view">
    <div class="row">
        <div class="col-xs-12 col-sm-4 pull-right">
            <div class="profile-block">
                <div class="dp-container">
                    <img src="/drive/uploads/avatar/avatar-01.png" class="img-circle user-profile">
                    <h4 class="customer-naming"> <?= $model->investmentUser->f_name . ' '. $model->investmentUser->l_name ?> </h4>
                </div>
                <div class="profile-details">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th style="width: 40px">Full Name</th>
                                <td style="width: 60px"> <?= $model->investmentUser->f_name . ' '. $model->investmentUser->l_name ?></td>
                            </tr>
                            <tr>
                                <th style="width: 40px">Username </th>
                                <td style="width: 60px"> <?= $model->investmentUser->username ?></td>
                            </tr>
                            <tr>
                                <th style="width: 40px"> Mobile </th>
                                <td style="width: 60px"> <?= $model->investmentUser->mobile ?></td>
                            </tr>
                            <tr>
                                <th style="width: 40px"> Address </th>
                                <td style="width: 60px"> <?= $model->investmentUser->b2cAddressModel->address ?></td>
                            </tr>
                            <tr>
                                <th style="width: 40px"> Location </th>
                                <td style="width: 60px"> <?= $model->investmentUser->b2cAddressModel->location ?></td>
                            </tr>
                            <tr>
                                <th style="width: 40px"> PIN </th>
                                <td style="width: 60px"> <?= $model->investmentUser->b2cAddressModel->pincode ?></td>
                            </tr>
                            <tr>
                                <th style="width: 40px">Investment Status</th>
                                <td style="width: 60px"> <?= $view->bootsrapLabel('string',$model->status); ?></td>
                            </tr>
                            <tr>
                                <th style="width: 40px"> Joining Date </th>
                                <td style="width: 60px"> <?= date('M d, Y h:i A',strtotime($model->created_at)) ?> </td>
                            </tr>
                            


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8">
            <div class="row">
                <div class="col-lg-4 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-green">
                    <div class="inner">
                      <h3> &#8377;  <?=number_format($model->investmentTotal, 2)?></h3>
                      <p>Rupees Invested</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div><!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3><?= $model->investmentPaymentSummary['sucessPayments']?></h3>
                      <p>Sucessfull payments</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div><!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-red">
                    <div class="inner">
                      <h3><?=$model->investmentPaymentSummary['pendingRecord']?></h3>
                      <p>Pending Due</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div><!-- ./col -->
            </div><!-- /.row -->
            <!-- BAR CHART -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Current Year Summary</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="barChart" style="height:430px"></canvas>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
    </div>
</div>


<style type="text/css" media="screen">
    
    h4.customer-naming {
        font-size: 19px;
        font-weight: normal;
        text-transform: UPPERCASE;
        text-indent: 'Raleway';
    }
</style>

 <script>

    $(document).ready(function() {
        loadPaymentSummaryChart();

    });

    function loadPaymentSummaryChart() {
        var schemeGuid = "<?=$model->guid?>";
        var dateFrom = "";
        var dateTo = "";
        var monthArray = [];
        var paidArray = [];
        var toPayArray = [];
        $.ajax({
                method: 'POST',
                url: '/investment-joins/get-investment-reports',
                crossDomain: false,
                dataType: 'json',
                data:{ guid : schemeGuid, dateFrom: dateFrom, dateTo: dateTo },
                success: function(responseData, textStatus, jqXHR) {
                    if(responseData.status === true) {
                        reloadchart(responseData.data.month, responseData.data.paidArray, responseData.data.dueAmountArray);
                    }
                },
            error: function (responseData, textStatus, errorThrown) {
               $(".waitMe").hide();
               $("#loader-block").hide();
                
            },
        }); 
    }

    function loadMonth(number) {
        switch(number) {
            case 0: return 'January';
            case 1: return 'February';
            case 2: return 'March';
            case 3: return 'April';
            case 4: return 'May';
            case 5: return 'June';
            case 6: return 'July';
            case 7: return 'August';
            case 8: return 'September';
            case 9: return 'October';
            case 10: return 'November';
            case 11: return 'December';
            default: return '';
        }
    }

    function reloadchart(monthArrayRecived, paidArrayRecived, toPayArrayRecived) {
         var areaChartData = {
          labels: monthArrayRecived,
          datasets: [
            {
              label: "Due statergies",
              fillColor: "#ddd",
              strokeColor: "#ddd",
              pointColor: "#00aff0",
              pointStrokeColor: "#00aff0",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: toPayArrayRecived
            },
            {
              label: "Actual Payment",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: paidArrayRecived
            }
          ]
        };

        var barChartCanvas = $("#barChart").get(0).getContext("2d");
        var barChart = new Chart(barChartCanvas);
        var barChartData = areaChartData;
        barChartData.datasets[1].fillColor = "#00a65a";
        barChartData.datasets[1].strokeColor = "#00a65a";
        barChartData.datasets[1].pointColor = "#00a65a";
        var barChartOptions = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: true,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - If there is a stroke on each bar
          barShowStroke: true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth: 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing: 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing: 1,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to make the chart responsive
          responsive: true,
          maintainAspectRatio: true
        };

        barChartOptions.datasetFill = false;
        barChart.Bar(barChartData, barChartOptions);

    }


      $(function () {


        /* ChartJS
         * -------
         * Here we will create a few charts using ChartJS
         */

        //--------------
        //- AREA CHART -
        //--------------

        // Get context with jQuery - using jQuery's .get() method.
        // var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        // var areaChart = new Chart(areaChartCanvas);

        
      });
    </script>