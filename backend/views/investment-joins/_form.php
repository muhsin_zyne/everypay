<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\InvestmentJoins */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="investment-joins-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'guid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'u_id')->textInput() ?>

    <?= $form->field($model, 'plan_amount')->textInput() ?>

    <?= $form->field($model, 'plan_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'valid_from')->textInput() ?>

    <?= $form->field($model, 'valid_true')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'expired' => 'Expired', 'suspended' => 'Suspended', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'reccoring_type')->dropDownList([ 'day' => 'Day', 'week' => 'Week', 'month' => 'Month', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
