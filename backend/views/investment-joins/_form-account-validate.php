<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
  
      <div class="row">
        <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'id' => 'ajax'
        ]); ?>

            <div class="col-xs-12"><!-- 
               <?= $form->field($model, 'type')->radioList(['1'=>'Mobile Number','2'=>'Email Account'],['class'=>'']); ?> -->
                <?= $form->field($model,'type')->dropDownlist([0=>'Mobile Number'],[]) ?>
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Validate User', ['class' =>  'btn btn-primary theme-btn pull-right']) ?>
                </div>

            </div>

        <?php ActiveForm::end(); ?>

      </div>
</div>
<script type="text/javascript">
    $( document).ready(function() { 
    });

</script>