<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\InvestmentJoins */

$this->title = 'Update Investment Joins: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Investment Joins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="investment-joins-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
