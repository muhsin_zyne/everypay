<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\InvestmentJoinsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Investment Joins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investment-joins-index bg-fff">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Investment Joins', ['/investment-joins/account-validate'], ['class' => 'btn btn-success pull-right bootstrap-modal-new','data-title'=>'User already on the system ?','data-width'=>'30%']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'guid',
            [
                'header' => 'First Name',
                'attribute' => 'f_name',
                'value' => 'investmentUser.f_name',
            ],
            [
                'header' => 'Last Name',
                'attribute' => 'l_name',
                'value' => 'investmentUser.f_name',
            ],
            [
                'attribute' => 'plan_amount',
                'value' => function($model) {
                    return money_format('%i',$model->plan_amount);
                }, 
                'label' => 'Plan Amount',
                'contentOptions' => ['class' => 'text-right'],         
                'headerOptions' => ['class'=>'text-center'],
            ],
            'plan_label',
            'reccoring_type' => [   
                'header'=>'Type',
                'value' => function($model, $attribute){ 
                    if($model->reccoring_type=='week') {return 'Weekly';} else if($model->reccoring_type=='day') {return 'Daily';} else if($model->reccoring_type=='month') {return 'Monthly';} },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'reccoring_type', ['day' => 'day', 'week' => 'Weekly','month'=>'Monthly'],['class'=>'form-control Yii::$app->request->isPjaxl','prompt' => 'All']),

            ],
            'status' => [   
                'header'=>'Status',
                'value' => function($model, $attribute){ 
                    if($model->status=='active') {return 'Active';} else if($model->status=='expired') {return 'Expired';} else if($model->status=='suspended') {return 'Suspended';} },
                'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['active' => 'Active', 'expired' => 'Expired','suspended'=>'Suspended'],['class'=>'form-control Yii::$app->request->isPjaxl','prompt' => 'All']),

            ],
            // 'created_at',
            // 'valid_from',
            // 'valid_true',
            // 'status',
            // 'reccoring_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
