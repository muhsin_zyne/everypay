<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model backend\models\SchemeOffers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scheme-offers-form form-new">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'id' => 'ajax'
    ]); ?>
    <div class="row">

        <div class="col-xs-12 col-sm-12">    
            <?= $form->field($model, 'offer_type')->dropDownList([ 'month'=>'Bonus Month', 'cash'=>'Bonus Cash','gold'=>'Bonus Gold'], ['prompt' => 'Bonus Offer']) ?>
           
           <?= $form->field($model, 'offer_label')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'valid_from')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => ' Offer Valid From'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'todayHighlight' => true,
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'startDate' => date("yyyy-MM-dd H:i:s"),

                    ]
            ]); ?>

            <?= $form->field($model, 'valid_true')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => ' Offer Valid True'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'todayHighlight' => true,
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                        'startDate' => date("yyyy-MM-dd H:i:s"),

                    ]
            ]); ?>


            <?= $form->field($model, 'bonus_month')->textInput() ?>

            <?= $form->field($model, 'bonus_cash')->widget(MaskMoney::classname(), [
                'pluginOptions' => [
                    'allowNegative' => false
                ]
            ]); ?>

            <?= $form->field($model, 'bonus_gold')->widget(MaskMoney::classname(), [
                'pluginOptions' => [
                    'allowNegative' => false,
                    'prefix' => 'Grams (g) ',
                ]
            ]); ?>

            
            <?= $form->field($model, 'offer_availing_on')->dropDownList(['end_of'=>'End of Period','anytime'=>'Anytime'], ['prompt' => '']) ?>

            <?= $form->field($model, 'status')->dropDownList([ '0'=>'Active', '1'=>'Inactive', ], ['prompt' => 'status']) ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<style type="text/css">


</style>




<script type="text/javascript">
    
  $(document).ready(function() {

    function check_fild_active(type) {
        if(type=='month') {
            $(".form-group.field-schemeoffers-bonus_month").show();
            $(".form-group.field-schemeoffers-bonus_cash").hide();
            $(".form-group.field-schemeoffers-bonus_gold").hide();
        }
        else if(type=='cash') {
            $(".form-group.field-schemeoffers-bonus_cash").show();
            $(".form-group.field-schemeoffers-bonus_month").hide();
            $(".form-group.field-schemeoffers-bonus_gold").hide();
        }
         else if(type=='gold') {

            $(".form-group.field-schemeoffers-bonus_gold").show();
            $(".form-group.field-schemeoffers-bonus_cash").hide();
            $(".form-group.field-schemeoffers-bonus_month").hide();
        }
        else {

            $(".form-group.field-schemeoffers-bonus_cash").hide();
            $(".form-group.field-schemeoffers-bonus_month").hide();
            $(".form-group.field-schemeoffers-bonus_gold").hide();

        }

    }
    
    var type = $("#schemeoffers-offer_type").val();
    check_fild_active(type);
    
    $("#schemeoffers-offer_type").change(function() {
        var type = $("#schemeoffers-offer_type").val();
        check_fild_active(type);
    });

    
        

});


</script>