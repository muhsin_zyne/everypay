<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SchemeOffers */

$this->title = 'Update Scheme Offers: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Scheme Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="scheme-offers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
