<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SchemeOffersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Scheme Offers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scheme-offers-index">
    <div class="row">
        <div class="col-xs-12">
            <p> <?= Html::a('Create Scheme Offers', ['create'], ['class' => 'btn btn-success theme-btn pull-right bootstrap-modal-new ','data-title'=>'Create Scheme Offer','data-width'=>'50%',]) ?> </p>
            
                
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        
                        'offer_type' => [   'header'=>'Offer Type',
                            'value' => function($model, $attribute){ if($model->offer_type=='month') {return 'Bonus Month';} else if($model->offer_type=='cash') {return 'Bonus Cash';} else if($model->offer_type=='gold') {return 'Bonus Gold';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'offer_type', ['month' => 'Bonus Month', 'cash' => ' Bonus Cash','gold'=>'Bonus Gold'],['class'=>'form-control','prompt' => 'All']),

                        ],


                        'valid_from' => [

                            'header'=>'Valid From',
                            'attribute' => 'valid_from',
                            'filter'=> DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'valid_from',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]),
                        ],

                        'valid_true' => [

                            'header'=>'Valid True',
                            'attribute' => 'valid_true',
                            'filter'=> DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'valid_true',
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                            ]),
                        ],
                        'bonus_month',
                        'bonus_cash',
                        'bonus_gold',
                         
                        // 'valid_true',
                        // 'created_at',
                        // 'updated_at',
                        'status' => [   'header'=>'Status',
                            'value' => function($model, $attribute){ if($model->status==0) {return 'Active';} else if($model->status==1) {return 'Inactive';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'Active', '1' => 'Inactive'],['class'=>'form-control','prompt' => 'All']),

                        ],
                        ['class' => 'yii\grid\ActionColumn',   
                        'header'=>'Action',
                        'template'=>'{view}{update}{delete}',
                        'buttons'=>[
                          'update' => function ($url, $model) {    
                                return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'Update Scheme Offers - '.$model->id,                                           
                                        'data-width'=>'50%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'Edit ',
                                    ]); 
                                },
                            'view' => function($url,$model) {
                                return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'View Offer - '.$model->id,                                           
                                        'data-width'=>'50%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'View',
                                    ]); 

                            },
                        ]                            
                ],
                    ],
                ]); ?>
        

        </div>
    </div>
    
    
</div>
