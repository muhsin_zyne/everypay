<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SchemeOffers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Scheme Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scheme-offers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger theme-btn pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'value' => function($model) { if($model->status==0) return'Active'; else return 'Inactive'; },
            ],
            'offer_type',
            'bonus_month',
            'bonus_cash',
            'bonus_gold',
            'valid_from',
            'valid_true',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
