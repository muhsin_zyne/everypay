<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SchemeOffers */

$this->title = 'Create Scheme Offers';
$this->params['breadcrumbs'][] = ['label' => 'Scheme Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scheme-offers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
