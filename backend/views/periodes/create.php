<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Periodes */

$this->title = 'Create Periodes';
$this->params['breadcrumbs'][] = ['label' => 'Periodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="periodes-create">

    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
