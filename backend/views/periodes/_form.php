<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Periodes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="periodes-form">
	<?php $form = ActiveForm::begin([
		'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'id' => 'ajax'
	]); ?>
	<div class="row">
		<div class="col-xs-12">
			<?= $form->field($model, 'months')->textInput() ?>
			<?= $form->field($model, 'status')->dropDownList([ '0'=>'Active', '1'=>'Inactive', ], ['prompt' => 'status']) ?>
			<div class="form-group">
	        	<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success theme-btn pull-right' : 'btn btn-primary theme-btn pull-right']) ?>
	    	</div>
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
