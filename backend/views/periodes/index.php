<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PeriodesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Periodes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="periodes-index">
    <div class="row">
        <div class="col-xs-12">
            <p><?= Html::a('Create Periodes', ['create'], ['class' => 'btn btn-success theme-btn pull-right bootstrap-modal-new','data-title'=>'Create Periods','data-width'=>'30%']) ?> </p>
            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{summary}\n{items}\n<div class='pull-right'>{pager}</div>",
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                    'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'months',
                    'status' => [   'header'=>'Status',
                            'value' => function($model, $attribute){ if($model->status==0) {return 'Active';} else if($model->status==1) {return 'Inactive';} },
                            'filter' => \yii\helpers\Html::activeDropDownList($searchModel, 'status', ['0' => 'Active', '1' => 'Inactive'],['class'=>'form-control','prompt' => 'All']),

                    ],
                    
                    ['class' => 'yii\grid\ActionColumn',   
                        'header'=>'Action',
                        'template'=>'{view}{update}{delete}',
                        'buttons'=>[
                          'update' => function ($url, $model) {    
                                return  Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'Edit Period',                                           
                                        'data-width'=>'30%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'Edit ',
                                    ]); 
                                },
                            'view' => function($url,$model) {
                                return  Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', [$url], [
                                        'data-toggle'=>'tooltip',
                                        'data-placement'=>'top',
                                        'data-title'=>'View Period',                                           
                                        'data-width'=>'30%',
                                        'class'=>'bootstrap-modal-new',
                                        'title' => 'View',
                                    ]); 

                            },
                        ]                            
                    ],
                ],
                ]); ?>

        </div>
    </div>


    

    
</div>
