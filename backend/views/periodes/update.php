<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Periodes */

$this->title = 'Update Periodes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Periodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="periodes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
