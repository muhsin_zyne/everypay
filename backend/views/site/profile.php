<?php

/* @var $this yii\web\View */
//use yii;
use frontend\models\Settings;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;

$this->title = 'My Yii Application';
?>

<div class="site-index">
	<div class="row">
		<div class="col-xs-4">
			<div class="sig-block">
				<img id="newSignature" src="<?=yii::$app->params['rootUrl'].'/'.yii::$app->params['storePath'].'/admin/signature/'.$activeUser->adminData->signature ?>">
			</div>

		</div>

		<div class="col-xs-4">

			<!-- Signature -->
			<div id="signature" style="">
			 <canvas id="signature-pad" class="signature-pad" width="350px" height="250px" style="background: #fff"></canvas>
			<div class="actions-btn">
			 	<a href="#" id="export"> <i class="fa fa-save"> </i> </a>
			 	<a href="#" id="clear"> <i class="fa fa-window-close" style="color: #f00"> </i> </a>
			</div>
			</div>

			<!-- <button id="export" class="btn btn-primary"> export </button>
			<button id="clear" class="btn btn-primary"> clear </button> -->
			<!-- Script -->

		</div>
	</div>


</div>


<script>
$(document).ready(function() {
 var signaturePad = new SignaturePad(document.getElementById('signature-pad'));
 $('#click').click(function(){
 	var data = signaturePad.toDataURL('image/png');
 $('#output').val(data);
 $("#sign_prev").show();
 $("#sign_prev").attr("src",data);
 });

 $("#clear").click(function() {
  signaturePad.clear();

 });

 $("#export").click(function() {

 	if(!signaturePad.isEmpty()) {

 		var data = signaturePad.toDataURL('image/png');
	 	
	 	jQuery.ajax({
            method: 'POST',
            url: '<?=yii::$app->params['backendUrl']?>/site/save-signature',
            crossDomain: false,
            dataType: 'text',
			data: {
			   	base64data : data
			},
            success: function(responseData, textStatus, jqXHR) {
            	console.log(responseData);
            	
   				$("#newSignature").attr('src',responseData);
            },
            error: function (responseData, textStatus, errorThrown) {
              	  
            },
	    });

		/*$.ajax({ 
	    type: "POST", 
	    url: url,
	    dataType: 'text',
		    data: {
		        base64data : data
		    }
		});*/

 	}
 	else {
 		alert("signature is required");
 	}


 	


 });
})
 </script>

 <style type="text/css">
 .actions-btn {
    position: relative;
    margin-top: -36px;
    /* padding-left: 36px !important; */
    text-align: right;
}
.actions-btn .fa {
    font-size: 26px;
    color: #0f86ec;
}
.sig-block {
    background: #fff;
}


 </style>