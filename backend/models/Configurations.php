<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "configurations".
 *
 * @property integer $id
 * @property string $type
 * @property string $attribute_name
 * @property string $value
 */
class Configurations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configurations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'attribute_name', 'value'], 'required'],
            [['type'], 'string'],
            [['attribute_name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'attribute_name' => 'Attribute Name',
            'value' => 'Value',
        ];
    }

    public function loadSMSconfigurations() {
        $data = Configurations::find()
            ->where([
                'type' => 'sms'
            ])->all();
        return ArrayHelper::map($data, 'attribute_name', 'value');
    }
}
