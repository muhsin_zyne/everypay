<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "periodes".
 *
 * @property integer $id
 * @property integer $months
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Periodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'periodes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['months', 'status', 'created_at', 'updated_at'], 'required'],
            [['months'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'months' => 'Months',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getPeriodes() {
        return Periodes::find()->where(['status'=>'0'])->all();
    }

    public function generatePeriodeLists() {
        $periodes = $this->getPeriodes();
        return ArrayHelper::map($periodes, 'months','months');

    }


}
