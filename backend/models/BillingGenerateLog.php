<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "billing_generate_log".
 *
 * @property integer $id
 * @property string $exicuted_date
 * @property string $status
 * @property string $type
 */
class BillingGenerateLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_generate_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exicuted_date', 'status', 'type'], 'required'],
            [['exicuted_date','exicuted_at'], 'safe'],
            [['status', 'type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exicuted_date' => 'Exicuted Date',
            'status' => 'Status',
            'type' => 'Type',
        ];
    }

    public function getlogData($type,$date) {
        $permission = false;  
        $requested_date = date('d',strtotime($date));
        $requested_month = date('m',strtotime($date));
        $requested_year = date('Y',strtotime($date));
                
        $data = BillingGenerateLog::find()->where([ 'type'=>$type, 'exicuted_month' => $requested_month,'exicuted_year' => $requested_year, 'status'=>'success',
                ])->one();

        if(!empty($data)) {

            $exicuted_date = $data->exicuted_date;
            $exicuted_month = $data->exicuted_month;
            $exicuted_year = $data->exicuted_year;

            if($exicuted_month==$requested_month) {
                $permission = 'false';
            }
            else {
                $permission = 'true';
            }

        }
        else {
            $permission = 'true';

        }

        return $permission;

    }
}
