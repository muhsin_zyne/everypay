<?php

namespace backend\models;
use Yii;
use backend\models\PaymentRecords;
use backend\models\User;

/**
 * This is the model class for table "payment_online".
 *
 * @property integer $id
 * @property string $type
 * @property integer $u_id
 * @property integer $payment_record_id
 * @property double $amount
 * @property double $gst
 * @property double $grand_total
 * @property double $discount
 * @property string $status
 * @property string $label
 * @property string $created_at
 * @property string $valid_true
 * @property string $billing_mobile
 * @property string $billing_email
 * @property string $billing_first_name
 * @property string $billing_last_name
 * @property string $payment_gateway
 * @property string $transaction_id
 * @property string $payment_collected_at
 * @property string $card_number
 * @property string $refund_status
 * @property double $refund_amount
 */
class PaymentOnline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_online';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'u_id', 'payment_record_id', 'amount', 'gst', 'grand_total', 'discount', 'status', 'label', 'created_at', 'valid_true', 'billing_mobile', 'billing_email', 'billing_first_name', 'billing_last_name', 'payment_gateway', 'transaction_id', 'payment_collected_at', 'card_number', 'refund_status', 'refund_amount'], 'safe'],
            [['type', 'status', 'payment_gateway', 'refund_status'], 'string'],
            [['u_id', 'payment_record_id'], 'integer'],
            [['guid'],'required'],
            [['amount', 'gst', 'grand_total', 'discount', 'refund_amount'], 'number'],
            [['created_at', 'valid_true', 'payment_collected_at'], 'safe'],
            [['label', 'billing_email', 'billing_first_name', 'billing_last_name'], 'string', 'max' => 255],
            [['billing_mobile'], 'string', 'max' => 10],
            [['transaction_id'], 'string', 'max' => 100],
            [['card_number'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'u_id' => 'U ID',
            'payment_record_id' => 'Payment Record ID',
            'amount' => 'Amount',
            'gst' => 'Gst',
            'grand_total' => 'Grand Total',
            'discount' => 'Discount',
            'status' => 'Status',
            'label' => 'Label',
            'created_at' => 'Created At',
            'valid_true' => 'Valid True',
            'billing_mobile' => 'Billing Mobile',
            'billing_email' => 'Billing Email',
            'billing_first_name' => 'Billing First Name',
            'billing_last_name' => 'Billing Last Name',
            'payment_gateway' => 'Payment Gateway',
            'transaction_id' => 'Transaction ID',
            'payment_collected_at' => 'Payment Collected At',
            'card_number' => 'Card Number',
            'refund_status' => 'Refund Status',
            'refund_amount' => 'Refund Amount',
        ];
    }

    public function getPaymentRecord() {
        return $this->hasOne(PaymentRecords::className(),['id'=>'payment_record_id']);
    }

    // Payment User Details Finding
    public function getPaymentUser() {
        return $this->hasOne(User::className(),['id'=>'u_id']);
    }
}
