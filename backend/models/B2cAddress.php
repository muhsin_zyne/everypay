<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "b2c_address".
 *
 * @property integer $id
 * @property integer $u_id
 * @property string $type
 * @property string $address
 * @property string $pincode
 * @property string $status
 */
class B2cAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b2c_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'address', 'status'], 'required'],
            [['u_id'], 'integer'],
            [['localtion'],'safe'],
            [['type', 'address', 'status'], 'string'],
            [['pincode'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'type' => 'Type',
            'address' => 'Address',
            'pincode' => 'Pincode',
            'status' => 'Status',
        ];
    }
}
