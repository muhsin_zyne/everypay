<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "admin_data".
 *
 * @property integer $id
 * @property integer $u_id
 * @property string $signature
 */
class AdminData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'signature'], 'required'],
            [['u_id'], 'integer'],
            [['signature'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'signature' => 'Signature',
        ];
    }
}
