<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "investment_accounts".
 *
 * @property integer $id
 * @property integer $investment_id
 * @property integer $investment_due_account_id
 * @property integer $u_id
 * @property double $amount
 * @property string $type
 * @property integer $account_agent
 * @property string $status
 * @property string $generated_at
 * @property string $clearance_at
 * @property double $collected_amount
 * @property double $returned_balance
 */
class InvestmentAccounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investment_accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['investment_id', 'u_id', 'amount', 'type', 'account_agent', 'generated_at', 'clearance_at'], 'required'],
            [['investment_id', 'investment_due_account_id', 'u_id', 'account_agent'], 'integer'],
            [['amount', 'collected_amount', 'returned_balance'], 'number'],
            [['type', 'status'], 'string'],
            [['generated_at', 'clearance_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'investment_id' => 'Investment ID',
            'investment_due_account_id' => 'Investment Due Account ID',
            'u_id' => 'U ID',
            'amount' => 'Amount',
            'type' => 'Type',
            'account_agent' => 'Account Agent',
            'status' => 'Status',
            'generated_at' => 'Generated At',
            'clearance_at' => 'Clearance At',
            'collected_amount' => 'Collected Amount',
            'returned_balance' => 'Returned Balance',
        ];
    }
    public function getAvailableInvestment($id) {
        $data = InvestmentAccounts::find()
        ->where([
            'u_id' => $id,
            'status' => 'success'
        ])
        ->sum('amount');
        return $data ? $data : 0.00;
    }
}
