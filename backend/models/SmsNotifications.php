<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sms_notifications".
 *
 * @property integer $id
 * @property string $to_mobile
 * @property string $sms_type
 * @property string $content
 * @property string $requested_at
 * @property string $category
 * @property string $attempt
 * @property string $current_status
 * @property string $refrence_id
 */
class SmsNotifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_notifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'to_mobile', 'sms_type', 'content', 'category', 'attempt', 'current_status', 'refrence_id'], 'required'],
            [['id'], 'integer'],
            [['sms_type', 'content', 'category', 'current_status'], 'string'],
            [['requested_at'], 'safe'],
            [['to_mobile'], 'string', 'max' => 12],
            [['refrence_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'to_mobile' => 'To Mobile',
            'sms_type' => 'Sms Type',
            'content' => 'Content',
            'requested_at' => 'Requested At',
            'category' => 'Category',
            'attempt' => 'Attempt',
            'current_status' => 'Current Status',
            'refrence_id' => 'Refrence ID',
        ];
    }
}
