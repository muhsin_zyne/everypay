<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "appointment_schedules".
 *
 * @property integer $id
 * @property integer $u_id
 * @property integer $agent_id
 * @property string $scheduled_for
 * @property string $type
 * @property integer $investment_join_id
 * @property integer $scheme_join_id
 * @property integer $investment_due_account_id
 * @property string $status
 * @property string $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $completed_at
 * @property integer $completed_by
 */
class AppointmentSchedules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointment_schedules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'agent_id', 'scheduled_for', 'type', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by', 'completed_at', 'completed_by'], 'required'],
            [['u_id', 'agent_id', 'investment_join_id', 'scheme_join_id', 'investment_due_account_id', 'created_by', 'updated_by', 'completed_by'], 'integer'],
            [['scheduled_for', 'created_at', 'updated_at', 'completed_at'], 'safe'],
            [['type', 'status', 'is_deleted'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'agent_id' => 'Agent ID',
            'scheduled_for' => 'Scheduled For',
            'type' => 'Type',
            'investment_join_id' => 'Investment Join ID',
            'scheme_join_id' => 'Scheme Join ID',
            'investment_due_account_id' => 'Investment Due Account ID',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'completed_at' => 'Completed At',
            'completed_by' => 'Completed By',
        ];
    }
}
