<?php

namespace backend\models;

use Yii;
use backend\models\AdminData;
use backend\models\SchemeJoins;
use backend\models\InvestmentAccounts;
use backend\models\B2cAddress;
use backend\models\CollectionRoots;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $display_name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'display_name', 'auth_key', 'password_hash', 'email', 'mobile', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email','f_name','l_name'], 'string', 'max' => 255],
            [['display_name'], 'string', 'max' => 200],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['mobile'], 'integer'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            ['mobile', 'string', 'max' => 10,'min'=>10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'display_name' => 'Display Name',
            'f_name' => 'First Name',
            'l_name' => 'Last Name',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getAdminData(){
        return $this->hasOne(AdminData::className(),['u_id'=>'id']);
    }

    public function getB2cAddressModel(){
        $data = $this->hasOne(B2cAddress::className(),['u_id'=>'id']);
        return $data;
    }

    public function loadRootList() {
        $list = CollectionRoots::find()
            ->where(['status'=>1])
            ->all();
        return ArrayHelper::map($list, 'id','root_name');
    }

    public function getRandomPassword() {
        $alphabet = '()ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$&*abcdefghijklmnopqrstuvwxyz1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function loadAccountInfo($id) {
        $schemeJoinModel = new SchemeJoins();
        $investmentAccounts = new InvestmentAccounts();
        
        $schemeInvestment = $schemeJoinModel->getShemeInvestment($id);
        $totalInvestment = $investmentAccounts->getAvailableInvestment($id);
        return [
            'schemeInvestment' => $schemeInvestment,
            'totalInvestment' => $totalInvestment,
        ];
    }
    
}
