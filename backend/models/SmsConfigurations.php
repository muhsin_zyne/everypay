<?php

namespace backend\models;

use Yii;
use backend\models\SmsTemplates;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sms_configurations".
 *
 * @property integer $id
 * @property string $label
 * @property integer $templeate_id
 */
class SmsConfigurations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_configurations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['templeate_id'], 'integer'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'templeate_id' => 'Templeate ID',
        ];
    }
    public function getTemplateModel(){
        return $this->hasOne(SmsTemplates::className(),['id'=>'templeate_id']);
    }

    public function loadAll() {
        return SmsConfigurations::find()->all();
    }

    public function getTempleatesList() {
        $data = SmsTemplates::find()
            ->where(['status' => '1'])
            ->all();
        return ArrayHelper::map($data, 'id', 'label');

    }
    public function loadSmsTemplateData($id) {
        return $data = SmsTemplates::find()
            ->where(['id'=> $id])->asArray()->one() ;
    }
}
