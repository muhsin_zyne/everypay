<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scheme_offers".
 *
 * @property integer $id
 * @property string $status
 * @property string $offer_type
 * @property integer $bonus_month
 * @property double $bonus_cash
 * @property string $valid_from
 * @property string $valid_true
 * @property string $created_at
 * @property string $updated_at
 */
class SchemeOffers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheme_offers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'offer_type', 'valid_from', 'valid_true','offer_availing_on','offer_label'], 'required'],
            [['status'], 'string'],
            [['offer_label'],'string','max'=>35],
            [['bonus_month'], 'integer'],
            [['bonus_cash'], 'number'],
            ['bonus_cash', 'required', 'when' => function ($model) {
                                                return $model->offer_type == 'cash';},
                                        'whenClient' => "function (attribute, value) {
                                        return $('#schemeoffers-offer_type').val() == 'cash'; }"
            ],
            ['bonus_month', 'required', 'when' => function ($model) {
                                                return $model->offer_type == 'month';},
                                        'whenClient' => "function (attribute, value) {
                                        return $('#schemeoffers-offer_type').val() == 'month'; }"
            ],
            ['bonus_gold', 'required', 'when' => function ($model) {
                                                return $model->offer_type == 'gold';},
                                        'whenClient' => "function (attribute, value) {
                                        return $('#schemeoffers-offer_type').val() == 'gold'; }"
            ],
            //[['bonus_cash'], 'required', 'when' => function($model){ return $model->offer_type=="cash"; }],
            [['created_at','updated_at'],'safe'],
            [['valid_from', 'valid_true', 'created_at', 'updated_at'], 'safe'],
            [['offer_type'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'offer_type' => 'Offer Type',
            'bonus_month' => 'Bonus Month',
            'bonus_cash' => 'Bonus Cash',
            'valid_from' => 'Valid From',
            'valid_true' => 'Valid True',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
            'offer_label' => 'Offer Label',
        ];
    }

    public function getSchemeOffers() {
        return SchemeOffers::find()->where(['status'=>'0'])->all();
    }

    public function generateSchemeOffersLists() {
        $schemeOffers = $this->getSchemeOffers();
        return ArrayHelper::map($schemeOffers, 'id','offer_label');

    }
    
}
