<?php

namespace backend\models;

use Yii;
use backend\models\PaymentRecords;

/**
 * This is the model class for table "payment_offline".
 *
 * @property integer $id
 * @property integer $u_id
 * @property string $type
 * @property integer $payment_record_id
 * @property double $amount
 * @property double $gst
 * @property double $grand_total
 * @property double $discount
 * @property double $collected_amount
 * @property double $return_balance
 * @property double $balance_due
 * @property string $status
 * @property integer $collection_agent_id
 * @property string $collection_agent_note
 * @property string $collection_agent_signature
 */
class PaymentOffline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_offline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'type', 'payment_record_id', 'amount', 'gst', 'grand_total', 'discount', 'collected_amount', 'status', 'collection_agent_id', 'collection_agent_note'], 'required'],
            [['u_id', 'payment_record_id', 'collection_agent_id'], 'integer'],
            [['type', 'status', 'collection_agent_note', 'collection_agent_signature'], 'string'],
            [['amount', 'gst', 'grand_total', 'discount', 'collected_amount', 'return_balance', 'balance_due'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'type' => 'Type',
            'payment_record_id' => 'Payment Record ID',
            'amount' => 'Amount',
            'gst' => 'Gst',
            'grand_total' => 'Grand Total',
            'discount' => 'Discount',
            'collected_amount' => 'Collected Amount',
            'return_balance' => 'Return Balance',
            'balance_due' => 'Balance Due',
            'status' => 'Status',
            'collection_agent_id' => 'Collection Agent',
            'collection_agent_note' => 'Collection Agent Note',
            'collection_agent_signature' => 'Collection Agent Signature',
        ];
    }


    public function getPaymentRecord() {
        return $this->hasOne(PaymentRecords::className(),['id'=>'payment_record_id']);
    }

    public function findCollectionAgent() {
        $activeUser = yii::$app->controller->getActiveUser();
        $responce = [];
        if($activeUser!=null) {
            $responce = [$activeUser->id => $activeUser->f_name.' '.$activeUser->l_name];
        }
        
        return $responce;
    }
}
