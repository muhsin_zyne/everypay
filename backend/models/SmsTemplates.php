<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sms_templates".
 *
 * @property integer $id
 * @property string $label
 * @property string $content
 * @property string $status
 */
class SmsTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'content', 'status'], 'required'],
            [['content', 'status'], 'string'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => 'Label',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }
}
