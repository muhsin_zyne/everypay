<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "collection_roots".
 *
 * @property integer $id
 * @property string $root_name
 * @property string $status
 */
class CollectionRoots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection_roots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root_name', 'status'], 'required'],
            [['status'], 'string'],
            [['root_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root_name' => 'Root Name',
            'status' => 'Status',
        ];
    }
}
