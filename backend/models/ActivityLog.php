<?php

namespace backend\models;

use Yii;
use backend\models\Activities;

/**
 * This is the model class for table "activity_log".
 *
 * @property integer $id
 * @property string $log
 * @property string $time
 * @property integer $log_refrence
 * @property integer $logged_by
 */
class ActivityLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log', 'time', 'log_refrence', 'logged_by'], 'required'],
            [['log'], 'string'],
            [['time','model_refrence'], 'safe'],
            [['log_refrence', 'logged_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'log' => 'Log',
            'time' => 'Time',
            'log_refrence' => 'Log Refrence',
            'logged_by' => 'Logged By',
        ];
    }

    public function newLog($id,$modelName,$modelRefrence) {
        $model = new ActivityLog();
        $log_record = Activities::findOne($id)->activity_data;

        $model->log = $log_record;
        $model->model_refrence = $modelName;
        $model->log_refrence = $modelRefrence;
        $model->time = $this->getNowTime();
        $model->logged_by =  $this->getActiveUser();
        $model->save();
               
        
        //$log->log
    }

    protected function getActiveUser() {
        
        if(isset(yii::$app->user->id)) {
            $activeUser = \common\models\User::find()->where(['id' =>yii::$app->user->id])->one();
            return $activeUser->id;
        }
        
    }

    public function getNowTime() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('Y-m-d H:i:s');
        return $dateTime;
    }
}
