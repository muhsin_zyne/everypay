<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "investment_bill_daily_log".
 *
 * @property integer $id
 * @property string $exicuted_date
 * @property string $status
 * @property string $exicuted_at
 */
class InvestmentBillDailyLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investment_bill_daily_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exicuted_date', 'status'], 'required'],
            [['exicuted_date', 'exicuted_at'], 'safe'],
            [['status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exicuted_date' => 'Exicuted Date',
            'status' => 'Status',
            'exicuted_at' => 'Exicuted At',
        ];
    }
}
