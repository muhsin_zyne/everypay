<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\InvestmentDueAccounts;

/**
 * InvestmentDueAccountsSearch represents the model behind the search form about `backend\models\InvestmentDueAccounts`.
 */
class InvestmentDueAccountsSearch extends InvestmentDueAccounts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'u_id', 'investment_id'], 'integer'],
            [['amount', 'actual_amount'], 'number'],
            [['generated_at', 'status', 'collected_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestmentDueAccounts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'u_id' => $this->u_id,
            'investment_id' => $this->investment_id,
            'amount' => $this->amount,
            'actual_amount' => $this->actual_amount,
            'generated_at' => $this->generated_at,
            'collected_at' => $this->collected_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
