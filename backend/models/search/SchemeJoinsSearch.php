<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SchemeJoins;

/**
 * SchemeJoinsSearch represents the model behind the search form about `backend\models\SchemeJoins`.
 */
class SchemeJoinsSearch extends SchemeJoins
{
    /**
     * @inheritdoc
     */
    public $f_name;
    public $l_name;

    public function rules()
    {
        return [
            [['id', 'duration', 'bonus_month'], 'integer'],
            [['plan_amount', 'bonus_cash', 'bonus_gold'], 'number'],
            [['created_at', 'valid_from', 'valid_true', 'offer_type', 'offer_label', 'offer_valid_from', 'offer_valid_true', 'offer_availing_on','plan_label','paid_duration','total_investment','status','u_id','f_name','l_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function seachForWeb() {

        $query = SchemeJoins::find();
        $dataProvider = new ActiveDataProvider([
            'query'     => $query,
             /* 'pagination' => [
                'pageSize' => 5,
            ],*/
        ]);
        
        return $dataProvider;
    }

    public function search($params)
    {
        $query = SchemeJoins::find();
        $query->joinWith('schemeUser');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
           /* 'pagination' => [
                'pageSize' => 5,
            ],*/
        ]);

        $this->load($params);
        if($this->created_at!='') {
            $dateRange =  explode('-',$this->created_at);
            foreach ($dateRange as $key => $value) {
                $dateRange[$key] = date('Y-m-d',strtotime($value));
            }
        }
        else {
            $dateRange[0] = '2010-01-01';
            $dateRange[1] = '2050-01-01';
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'scheme_joins.id' => $this->id,
        ]);
        
        $query->andFilterWhere(['>=', 'scheme_joins.created_at', $dateRange[0]])
              ->andFilterWhere(['<', 'scheme_joins.created_at', $dateRange[1]]);

        $query->andFilterWhere(['like', 'offer_type', $this->offer_type])
            ->andFilterWhere(['like', 'offer_label', $this->offer_label])
            ->andFilterWhere(['like','plan_amount',$this->plan_amount])
            ->andFilterWhere(['like','total_investment',$this->total_investment])
            ->andFilterWhere(['like','paid_duration',$this->paid_duration])
            ->andFilterWhere(['like','plan_label',$this->plan_label])
            ->andFilterWhere(['like','scheme_joins.status',$this->status])
            ->andFilterWhere(['like', 'offer_availing_on', $this->offer_availing_on]);
        
        $query->andFilterWhere(['like','User.f_name',$this->f_name])
            ->andFilterWhere(['like','User.l_name',$this->l_name]);

        return $dataProvider;
    }
}
