<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PaymentRecords;

/**
 * PaymentRecordsSearch represents the model behind the search form about `backend\models\PaymentRecords`.
 */
class PaymentRecordsSearch extends PaymentRecords
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'u_id', 'scheme_join_id', 'collection_agent_id', 'online_payment_id'], 'integer'],
            [['billing_for', 'created_at', 'collection_type', 'collection_agent_note', 'status', 'payment_received_at'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentRecords::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'u_id' => $this->u_id,
            'scheme_join_id' => $this->scheme_join_id,
            'billing_for' => $this->billing_for,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
            'collection_agent_id' => $this->collection_agent_id,
            'online_payment_id' => $this->online_payment_id,
            'payment_received_at' => $this->payment_received_at,
        ]);

        $query->andFilterWhere(['like', 'collection_type', $this->collection_type])
            ->andFilterWhere(['like', 'collection_agent_note', $this->collection_agent_note])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

    public function pendingPaymentCount($u_id,$scheme_join_id) {
        return PaymentRecords::find()->where(['scheme_join_id' => $scheme_join_id,'u_id'=>$u_id,'collection_type'=>'not_set'])->count();
    }
    public function sumOfPendingPayment($u_id,$scheme_join_id) {
        return PaymentRecords::find()->where(['scheme_join_id' => $scheme_join_id,'u_id'=>$u_id,'collection_type'=>'not_set'])->sum('amount');
    }
}
