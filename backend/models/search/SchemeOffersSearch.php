<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SchemeOffers;

/**
 * SchemeOffersSearch represents the model behind the search form about `backend\models\SchemeOffers`.
 */
class SchemeOffersSearch extends SchemeOffers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bonus_month'], 'integer'],
            [['status', 'offer_type', 'valid_from', 'valid_true', 'created_at', 'updated_at'], 'safe'],
            [['bonus_cash'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SchemeOffers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bonus_month' => $this->bonus_month,
            'bonus_cash' => $this->bonus_cash,
            'valid_from' => $this->valid_from,
            'valid_true' => $this->valid_true,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'offer_type', $this->offer_type]);

        return $dataProvider;
    }
}
