<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\User;
use kartik\daterange\DateRangeBehavior;


/**
 * UserSearch represents the model behind the search form about `backend\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public $date_range;
    public $join_start, $join_end;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'date_range',
                'dateStartAttribute' => 'join_start',
                'dateEndAttribute' => 'join_end',
                'dateStartFormat' => 'd-m-Y',
                'dateEndFormat' => 'd-m-Y',
            ]
        ];
    }


    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at', 'level', 'created_by'], 'integer'],
            [['username', 'display_name', 'l_name', 'f_name', 'user_type', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'image', 'mobile', 'date_range','join_start', 'join_end'], 'safe'],
            [['date_range'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $this->join_start = date('Y-m-d', strtotime($this->join_start));
        $this->join_end = date('Y-m-d', strtotime($this->join_end));
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'level' => $this->level,
            'created_by' => $this->created_by,
        ]);



        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'display_name', $this->display_name])
            ->andFilterWhere(['like', 'l_name', $this->l_name])
            ->andFilterWhere(['like', 'f_name', $this->f_name])
            ->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'mobile', $this->mobile]);

        if($this->date_range != '' && $this->join_start != '' && $this->join_end != '') {
            $query->andFilterWhere(['between', 'created_at', strtotime($this->join_start), strtotime($this->join_end)]);
           
        }
        return $dataProvider;
    }

    public function attributeLabels() {
        return [
            'date_range' => 'Join Date Range',
            'f_name' => 'First Name', 
            'l_name' => 'Last Name',
            'mobile' => 'Mobile Number',
        ];

    }
}
