<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\InvestmentJoins;

/**
 * InvestmentJoinsSearch represents the model behind the search form about `backend\models\InvestmentJoins`.
 */
class InvestmentJoinsSearch extends InvestmentJoins
{
    /**
     * @inheritdoc
     */
    public $f_name;
    public $l_name;

    public function rules()
    {
        return [
            [['id', 'u_id'], 'integer'],
            [['guid', 'plan_label', 'created_at', 'valid_from', 'valid_true', 'status', 'reccoring_type','f_name','l_name'], 'safe'],
            [['plan_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestmentJoins::find();
        $query->joinWith('investmentUser');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'investment_joins.id' => $this->id,
            'investment_joins.u_id' => $this->u_id,
            'investment_joins.plan_amount' => $this->plan_amount,
            'investment_joins.created_at' => $this->created_at,
            'investment_joins.valid_from' => $this->valid_from,
            'investment_joins.valid_true' => $this->valid_true,
        ]);

        $query->andFilterWhere(['like', 'investment_joins.guid', $this->guid])
            ->andFilterWhere(['like', 'investment_joins.plan_label', $this->plan_label])
            ->andFilterWhere(['like', 'investment_joins.status', $this->status])
            ->andFilterWhere(['like', 'investment_joins.reccoring_type', $this->reccoring_type]);

         $query->andFilterWhere(['like','User.f_name',$this->f_name])
            ->andFilterWhere(['like','User.l_name',$this->l_name]);
        return $dataProvider;
    }
}
