<?php

namespace backend\models;

use Yii;
use backend\models\SchemeJoins;
use backend\models\PaymentOnline;
use backend\models\PaymentOffline;

/**
 * This is the model class for table "payment_records".
 *
 * @property integer $id
 * @property integer $u_id
 * @property integer $scheme_join_id
 * @property string $billing_for
 * @property double $amount
 * @property string $created_at
 * @property string $collection_type
 * @property integer $collection_agent_id
 * @property string $collection_agent_note
 * @property integer $online_payment_id
 * @property string $status
 * @property string $payment_received_at
 */
class PaymentRecords extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'scheme_join_id', 'billing_for', 'amount', 'created_at'], 'required'],
            [['u_id', 'scheme_join_id', 'collection_agent_id', 'online_payment_id'], 'integer'],
            [['billing_for', 'created_at', 'payment_received_at','periode','is_startup_payment'], 'safe'],
            [['amount'], 'number'],
            [['collection_type', 'collection_agent_note','status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'scheme_join_id' => 'Scheme Join ID',
            'billing_for' => 'Billing For',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'collection_type' => 'Collection Type',
            'collection_agent_id' => 'Collection Agent ID',
            'collection_agent_note' => 'Collection Agent Note',
            'online_payment_id' => 'Online Payment ID',
            'status' => 'Status',
            'payment_received_at' => 'Payment Received At',
        ];
    }

    public function getSchemeJoins(){
        return $this->hasOne(SchemeJoins::className(),['id'=>'scheme_join_id']);
    }

    public function getPaymentOnlineModel() {
        return $this->hasOne(PaymentOnline::className(),['id'=>'online_payment_id']);
    }
    
    public function getPaymentRecordOnline() {
        return $this->hasOne(PaymentOnline::className(),['payment_record_id'=>'id']);
    }
    
    public function getPaymentOfflineModel() {
        return $this->hasOne(PaymentOffline::className(),['id'=>'offline_payment_id']);
    }

    public function getAllCountOfpayment($status,$u_id) {
        $paymentRecordsArray = PaymentRecords::find()
            ->where(['status'=>$status,'u_id'=>$u_id])
            ->all();
        return count($paymentRecordsArray);
    }

    public function getAllSumOfPaymentRecords($status,$u_id) {
        $paymentRecordsArray = PaymentRecords::find()
            ->where(['status'=>$status,'u_id'=>$u_id])
            ->all();
        $responce = 0;
        foreach ($paymentRecordsArray as $value) {
            $responce = $responce + $value->amount;

        }
        return $responce;
    }

}
