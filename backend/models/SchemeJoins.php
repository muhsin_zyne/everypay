<?php

namespace backend\models;

use Yii;
use backend\models\User;
use backend\models\PaymentRecords;
/**
 * This is the model class for table "scheme_joins".
 *
 * @property integer $id
 * @property integer $u_id
 * @property double $plan_amount
 * @property integer $duration
 * @property string $created_at
 * @property string $valid_from
 * @property string $valid_true
 * @property string $offer_type
 * @property string $offer_label
 * @property integer $bonus_month
 * @property double $bonus_cash
 * @property double $bonus_gold
 * @property string $offer_valid_from
 * @property string $offer_valid_true
 * @property string $offer_availing_on
 */
class SchemeJoins extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheme_joins';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'plan_amount', 'duration', 'created_at', 'valid_from', 'valid_true', 'offer_type', 'offer_label', 'offer_valid_from', 'offer_valid_true', 'offer_availing_on'], 'required'],
            [['total_investment','paid_duration','startup_payment','status','plan_label','guid'],'safe'],
            [['u_id', 'duration', 'bonus_month'], 'integer'],
            [['plan_amount', 'bonus_cash', 'bonus_gold'], 'number'],
            [['created_at', 'valid_from', 'valid_true', 'offer_valid_from', 'offer_valid_true'], 'safe'],
            [['offer_type'], 'string', 'max' => 30],
            [['offer_label'], 'string', 'max' => 50],
            [['offer_availing_on'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'plan_amount' => 'Plan Amount',
            'duration' => 'Duration',
            'created_at' => 'Created At',
            'valid_from' => 'Valid From',
            'valid_true' => 'Valid True',
            'offer_type' => 'Offer Type',
            'offer_label' => 'Offer Label',
            'bonus_month' => 'Bonus Month',
            'bonus_cash' => 'Bonus Cash',
            'bonus_gold' => 'Bonus Gold',
            'offer_valid_from' => 'Offer Valid From',
            'offer_valid_true' => 'Offer Valid True',
            'offer_availing_on' => 'Offer Availing On',
        ];
    }

    public function getShemeJoins($id) {
        return SchemeJoins::findOne($id);
    }

    public function getSchemeUser(){
        return $this->hasOne(User::className(),['id'=>'u_id']);
    }

    public function getShemeInvestment($u_id) {
        $result = SchemeJoins::find()
            ->where([
                'u_id' => $u_id,
                'status' => 'active',
            ])
            ->sum('total_investment');

        return $result==null ? 0 : $result;
    }

    public function loadSumofPayment($collection_type) {
        $amount = PaymentRecords::find()
            ->where([
                'u_id'              => $this->u_id,
                'scheme_join_id'    => $this->id,
                'collection_type'   => $collection_type,
            ])
            ->sum('amount');
        return $amount != null ? $amount : '0.00';
    }

    public function getSumofInvestments($status,$u_id) {
        $SchemeJoinsArray = SchemeJoins::find()
            ->where(['status'=>$status,'u_id'=>$u_id])
            ->all();
        $responce = 0;
        foreach ($SchemeJoinsArray as $value) {
            $responce = $responce + $value->total_investment;
        }
        return $responce;
    }
}
