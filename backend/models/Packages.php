<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "packages".
 *
 * @property integer $id
 * @property string $package_label
 * @property double $package_value
 * @property string $status
 */
class Packages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    

    public static function tableName()
    {
        return 'packages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_label', 'package_value', 'status'], 'required'],
            [['package_value'], 'number'],
            [['status'], 'string'],
            [['package_label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Package ID',
            'package_label' => 'Package Label',
            'package_value' => 'Package Value',
            'status' => 'Status',
        ];
    }

    public function getPackages() {
        return Packages::find()->where(['status'=>'0'])->all();
    }

    public function generatePackageLists() {
        $packages = $this->getPackages();
        return ArrayHelper::map($packages, 'id','package_label');

    }
}
