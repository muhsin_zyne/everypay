<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cms_pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $slug
 * @property string $content
 * @property string $status
 */
class CmsPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'meta_description', 'meta_keyword', 'slug', 'content', 'status'], 'required'],
            [['meta_description', 'meta_keyword', 'slug', 'content', 'status'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'slug' => 'Slug',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }
}
