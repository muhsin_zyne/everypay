<?php
namespace backend\models\forms;
use yii;
use yii\base\Model;


/**
 * Signup form
 */
class UpdateUserDetails extends Model
{
    public $user_id;
    public $roots;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','roots'],'safe'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
}
