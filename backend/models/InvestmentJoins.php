<?php

namespace backend\models;

use Yii;
use backend\models\User;
use backend\models\InvestmentDueAccounts;
use backend\models\InvestmentAccounts;
use yii\db\Query;

/**
 * This is the model class for table "investment_joins".
 *
 * @property integer $id
 * @property string $guid
 * @property integer $u_id
 * @property double $plan_amount
 * @property string $plan_label
 * @property string $created_at
 * @property string $valid_from
 * @property string $valid_true
 * @property string $status
 * @property string $reccoring_type
 */
class InvestmentJoins extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investment_joins';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            if($this->isNewRecord) {
                $this->created_at = yii::$app->controller->getNowTime();
                $this->valid_true = date('Y-m-d', strtotime("+5 years", strtotime($this->valid_from)));
                $this->status = 'active';
                $this->guid = yii::$app->controller->getGuid();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'plan_amount', 'plan_label', 'valid_from', 'reccoring_type'], 'required'],
            [['u_id'], 'integer'],
            [['plan_amount'], 'number'],
            [['created_at', 'valid_from', 'valid_true','guid','created_at','valid_true','status'], 'safe'],
            [['status', 'reccoring_type'], 'string'],
            [['guid'], 'string', 'max' => 36],
            [['plan_label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'guid' => 'Guid',
            'u_id' => 'U ID',
            'plan_amount' => 'Plan Amount',
            'plan_label' => 'Plan Label',
            'created_at' => 'Created At',
            'valid_from' => 'Valid From',
            'valid_true' => 'Valid True',
            'status' => 'Status',
            'reccoring_type' => 'Reccoring Type',
        ];
    }
    // load relation form user table
    public function getInvestmentUser(){
        return $this->hasOne(User::className(),['id'=>'u_id']);
    }
    public function getInvestmentPaymentSummary() {
        $pendingRecordCount = InvestmentDueAccounts::find()
            ->where([
                'u_id' => $this->u_id,
                'investment_id' => $this->id,
            ])
            ->andWhere([
                'not in', 'status', ['success','rejected','canceld']
            ])->count();
        $sucessPayments = InvestmentAccounts::find()
            ->where([
                'u_id' => $this->u_id,
                'investment_id' => $this->id,
                'status' => 'success',
            ])->count();
        $data = [
            'pendingRecord' => $pendingRecordCount,
            'sucessPayments' => $sucessPayments,
        ];
        return $data;
    }

    public function getInvestmentTotal() {
        return $this->hasMany(InvestmentAccounts::className(),['investment_id'=>'id'])
        ->andOnCondition(['status' => 'success'])
        ->sum('amount');
    }


    public function findJoins($status,$type) {
        $result = InvestmentJoins::find()
            ->where([
                'status' => 'active',
                'reccoring_type' => $type,
            ])
            ->andWhere(['<=','valid_from',date('Y-m-d')])
            ->andWhere(['>=','valid_true',date('Y-m-d')])
            ->all();
        return $result;
    }


    public function getPaymentReports($requestParams) {
        $query = new Query;
        $query  ->select([
                    'due_acc.*',
                    'due_acc.collected_at',
                    'due_acc.payment_for',
                    'due_acc.status',
                ])
                ->from('investment_due_accounts as due_acc')
                ->join('LEFT JOIN', 'investment_joins as join', 'due_acc.investment_id = join.id')
                ->where([
                    'join.guid' => $requestParams['guid']
                ])
                ->andWhere(['between','payment_for', $requestParams['dateFrom'], $requestParams['dateTo']])
                ->orderBy(['due_acc.collected_at' => SORT_ASC])
                ->all();
        $command = $query->createCommand();
        $data = $command->queryAll();
        return $data;

    }
}
