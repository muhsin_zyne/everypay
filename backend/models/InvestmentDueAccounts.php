<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "investment_due_accounts".
 *
 * @property integer $id
 * @property integer $u_id
 * @property integer $investment_id
 * @property double $amount
 * @property double $actual_amount
 * @property string $generated_at
 * @property string $status
 * @property string $collected_at
 */
class InvestmentDueAccounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investment_due_accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['u_id', 'investment_id', 'amount', 'generated_at', 'status'], 'required'],
            [['u_id', 'investment_id'], 'integer'],
            [['amount', 'actual_amount'], 'number'],
            [['generated_at', 'collected_at'], 'safe'],
            [['status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'u_id' => 'U ID',
            'investment_id' => 'Investment ID',
            'amount' => 'Amount',
            'actual_amount' => 'Actual Amount',
            'generated_at' => 'Generated At',
            'status' => 'Status',
            'collected_at' => 'Collected At',
        ];
    }
}
