<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

//Models
use backend\models\User;
use backend\models\SchemeJoins;
use backend\models\PaymentRecords;
use backend\models\PaymentOnline;
use backend\models\PaymentOffline;

//Search Models
use backend\models\search\SchemeJoinsSearch;
use backend\models\search\PaymentRecordsSearch;

/**
 * SchemeJoinsController implements the CRUD actions for SchemeJoins model.
 */
class SchemeJoinsController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SchemeJoins models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchemeJoinsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id'=>SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($join_id = null , $join_guid = null) {
        if($join_guid!=null) {
            $schemeJoinsModel = SchemeJoins::find()->where(['guid'=>$join_guid])->one();
        } else {
            $schemeJoinsModel = SchemeJoins::findOne($join_id);  
        }
        if(empty($schemeJoinsModel)) {
            throw new NotFoundHttpException('The requested scheme join dose not exist.');
        }
        $userModel = User::findOne($schemeJoinsModel->u_id);
        $pendingPaymentsArray = PaymentRecords::find()
            ->where([
                'scheme_join_id'    =>  $join_id,
                'u_id'              =>  $userModel->id,
                'status'            =>  'init',

            ])
            ->all();


        
        return $this->render('view',[
            'schemeJoinsModel' => $schemeJoinsModel,
            'userModel' => $userModel,
            'pendingPaymentsArray'=> $pendingPaymentsArray,
        ]);

    }


    public function actionViewPendingPayments($join_id =null , $join_guid = null) {
        
        if($join_guid!=null) {
            $schemeJoinsModel = SchemeJoins::find()->where(['guid'=>$join_guid])->one();
        } else {
            $schemeJoinsModel = SchemeJoins::findOne($join_id);  
        }
        if(empty($schemeJoinsModel)) {
            throw new NotFoundHttpException('The requested scheme join dose not exist.');
        }
        $userModel = User::findOne($schemeJoinsModel->u_id);

        $searchModel = new PaymentRecordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere([
            'scheme_join_id'    =>  $join_id,
            'u_id'              =>  $userModel->id,
            'status'            =>  'init',
        ]);

        return $this->render('view-pending-payments',[
            'dataProvider'      =>  $dataProvider,
            'searchModel'       =>  $searchModel,
            'userModel'         =>  $userModel,
            'schemeJoinsModel'  =>  $schemeJoinsModel,
        ]);

    }



    public function actionScanJoin($join_id) {
        $responce = [];
        $schemeJoinsModel = SchemeJoins::findOne($join_id);
        if(!empty($schemeJoinsModel)) {
            $responce = [
                'joinId' => $schemeJoinsModel->id,
                'status' => true,
            ];
            Yii::$app->session->setFlash('success','found Scheme Details');
        }
        else {
            $responce = [
                'status' => false,
            ];
            Yii::$app->session->setFlash('error','cound not found Scheme Details');
        }
        $encode = json_encode($responce,true);

        return $encode;
    }

    /* Accept Payment from offline through backend admin */
    public function actionAcceptPayment($guid) {
        
        $model = new PaymentOffline();
        
        if ($model->load(Yii::$app->request->post())) {
            $responce = [];
            $model->type = 'c2b';
            $model->status = 'init';
            if($model->save()) {
                if($model->balance_due==0) {
                    $model->status = 'success';
                }
                else {
                    $model->status = 'partiality';
                }

                if ($model->save()) {
                    $model->paymentRecord->collection_type = 'offline';
                    $model->paymentRecord->status = 'success';
                    $model->paymentRecord->offline_payment_id = $model->id;
                    $model->paymentRecord->payment_received_at = $this->getNowTime();
                    
                    if($model->paymentRecord->save()) {
                        $schemeJoinModel = $model->paymentRecord->schemeJoins;
                        $schemeJoinModel->total_investment = $schemeJoinModel->total_investment + $schemeJoinModel->plan_amount;
                        $schemeJoinModel->paid_duration = $schemeJoinModel->paid_duration+1;
                        if($schemeJoinModel->save()) {
                            $model->paymentRecord->periode = $schemeJoinModel->paid_duration.'/'.$schemeJoinModel->duration;
                            $model->paymentRecord->save();
                            Yii::$app->session->setFlash('success','Payment has been recieved ');
                            return $this->redirect(['scheme-joins/view-pending-payments','join_id'=>$schemeJoinModel->id]);

                        }
                    }
                   
                } else {
                    echo'<pre>';  print_r($model->getErrors()); die(); 
                } 


                return json_encode($model,true);       
            }
            else {
                echo'<pre>';  print_r($model->getErrors()); die(); 
            }
        }

        $acceptPaymentModel = PaymentOnline::find()
            ->where([ 'guid'    =>  $guid, 'status' =>  'pending'])->one();

        if($acceptPaymentModel==null) {
            throw new NotFoundHttpException('The requested payment form does not exist.');    
        }
        else {

            return $this->render('accept-payment',[
                'model'                 =>  $model,
                'acceptPaymentModel'    =>  $acceptPaymentModel,
            ]);
        }
        
        

    }


    public function actionApiPaymentCalculations() {
        // to calculate retrun balance and balance due from api
        if(yii::$app->request->post()) {
            $requestData = $_POST['PaymentOffline'];
            $return_balance = 0.00;
            $balance_due = 0.00;
            $responce = [
                'status'    => false,
            ];
            if($requestData['collected_amount'] > $requestData['grand_total']) {
                $return_balance = $requestData['collected_amount'] - $requestData['grand_total'];
                $balance_due = 0.00;
            }
            else {
                $balance_due = $requestData['grand_total'] - $requestData['collected_amount'];
                $return_balance = 0.00;
            }

            $responce = [
                'status'                => true,
                'balance_due'           => $balance_due,
                'return_balance'        => $return_balance,
                'return_balance_m'      => money_format('%i',$return_balance),
                'balance_due_m'         => money_format('%i',$balance_due),
            ];
            return json_encode($responce,true);
        }
    }




    /**
     * Displays a single SchemeJoins model.
     * @param integer $id
     * @return mixed
     */
    
    /**
     * Creates a new SchemeJoins model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    

    /**
     * Updates an existing SchemeJoins model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing SchemeJoins model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    */
    /**
     * Finds the SchemeJoins model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchemeJoins the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchemeJoins::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getPaymentOnlineByRecordId($payment_record_id) {
        $paymentOnline = PaymentOnline::find()
            ->where(['payment_record_id'=>$payment_record_id])
            ->one();
        return $paymentOnline;
    }
}
