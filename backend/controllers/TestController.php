<?php
namespace backend\controllers;

use Yii;
use backend\components\AdminController;

/**
 * Site controller
 */
class TestController extends AdminController
{
    public $uploadThumbnailPath;
    public $uploadSmallPath;
    public $uploadBasePath;
    public function init(){
        $this->uploadThumbnailPath = \Yii::$app->basePath."/../store/images/thumbnail/";
        $this->uploadSmallPath = \Yii::$app->basePath."/../store/images/small/";
        $this->uploadBasePath = \Yii::$app->basePath."/../store/images/base/";
        parent::init();
    }

    public function actionIndex()
    {   
        return $this->render('index');
    }

    public function actionSave() {

        $img = $_POST['base64data']; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $fileName = 'tmp/'.date('Ymdhis').'signature.png';
        file_put_contents($fileName, $data);




                
              /*  $source_image = imagecreatefrompng('tmp/image.png');
                $width = imagesx($source_image);
                $height = imagesy($source_image);

                $fileName ='abcd.png';
                
                $uploadThumbFile = $this->uploadThumbnailPath . $fileName;
                $uploadSmallFile = $this->uploadSmallPath . $fileName;
                $uploadBaseFile = $this->uploadBasePath . $fileName;
               
                $uploadfile = $this->uploadThumbnailPath.$fileName;
                $thumb_width = 200;
                $thumb_height = 200;
                $virtual_image = imagecreatetruecolor($thumb_width, $thumb_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
                $im = imagepng($virtual_image, $uploadThumbFile);*/

                //var_dump($im);die();

                /* Small Image*/

                /*$small_width = 350;
                $small_height = 350;
                $virtual_image = imagecreatetruecolor($small_width, $small_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $small_width, $small_height, $width, $height);
                $im = imagejpeg($virtual_image, $uploadSmallFile); 
*/
                /* Base Image*/

            /*    $base_width = 600;
                $base_height = 600;
                $virtual_image = imagecreatetruecolor($base_width, $base_height);
                
                imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $base_width, $base_height, $width, $height);
                $im = imagejpeg($virtual_image, $uploadBaseFile); 


                $responce = [
                    'status'=>'true',
                ];*/

    }

    
}
