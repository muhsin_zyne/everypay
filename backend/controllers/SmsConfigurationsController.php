<?php

namespace backend\controllers;

use backend\components\AdminController;
use backend\models\SmsConfigurations;
use yii;

class SmsConfigurationsController extends AdminController
{
    public function actionIndex()
    {	
    	$smsConfig = new SmsConfigurations;
    	$smsConfigData = $smsConfig->loadAll();
    	if($smsConfig->load(yii::$app->request->post())) {
    		$updateData = $smsConfig->templeate_id;
    		foreach ($updateData as  $key => $value) {
    			$configItem = SmsConfigurations::findOne($key);
    			$configItem->templeate_id = $value;
    			$configItem->save();
    		}
    		Yii::$app->session->setFlash('success','SMS configuration updated');
    		return $this->redirect(['sms-configurations/index']);
    		
    	}
        return $this->render('index', [
        	'smsConfig' => $smsConfig,
        	'smsConfigData' => $smsConfigData
        ]);
    }

    public function actionViewSmsTemplate($id) {
    	$smsConfig = new SmsConfigurations();
    	$loadResponse = $smsConfig->loadSmsTemplateData($id);
    	if($loadResponse) {
    		$response = [
    			'status' => true,
    			'data' => $loadResponse
    		];
    	} else {
    		$responseresponse = [
    			'status' => false,
    			'data' => [],
    		];
    	}
    	return json_encode($response, JSON_PRETTY_PRINT);
    }

}
