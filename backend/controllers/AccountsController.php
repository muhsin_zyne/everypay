<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\search\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;
use backend\models\search\SchemeJoinsSearch;
use backend\models\SchemeJoins;
use backend\models\InvestmentJoins;
use backend\models\search\InvestmentJoinsSearch;
use backend\models\B2cAddress;
use common\models\SignupForm;
use yii\web\Session;
use kartik\mpdf\Pdf;
/**
 * AccountsController implements the CRUD actions for User model.
 */
class AccountsController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */

    public function actionGetOfferDetails() {
        if(Yii::$app->request->post()) {
            $schemeOffer = new \backend\models\SchemeOffers();
            $offerId = $_POST['offerId'];
            $activeOfferArray = $schemeOffer->findOne($offerId);
            $activeArray = [];
            foreach ($activeOfferArray as $key => $value) {
                   $activeArray [$key] = $value;
            }
            print_r(json_encode($activeArray,true));

        }
    }

    public function actionUpdateUserDetails() {
        //$model = new User();
        if(yii::$app->request->post()) {
            $post = yii::$app->request->post();
            $userData = User::find()
                ->where(['id'=>$post['User']['id']])
                ->one();
                // if(!empty($post['B2cAddress']['roots'])) {
                //     $userData->b2cAddressModel->roots = implode(', ',array_filter($post['B2cAddress']['roots']));  
                // } else {
                //     $userData->b2cAddressModel->roots = '';
                // }
            $userData->b2cAddressModel->roots = $post['B2cAddress']['roots'];
            if($userData->b2cAddressModel->save()) {
                Yii::$app->session->setFlash('success', 'Roots has been Updated');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }


    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['level'=>'4']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $userModel = new User();

        $searchModel = new SchemeJoinsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['u_id'=>$id]);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);
        $dataProvider->pagination = ['pageSize' =>4];
        $investmentSearchModel = new InvestmentJoinsSearch();
        $investmentDataProvider = $investmentSearchModel->search(Yii::$app->request->queryParams);
        $investmentDataProvider->query->andWhere(['u_id'=>$id]);
        $investmentDataProvider->query->orderBy(['id' => SORT_DESC]);
        $investmentDataProvider->pagination = ['pageSize' =>4];

        $userSummaryInfo = $userModel->loadAccountInfo($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'userSummaryInfo' => $userSummaryInfo,
            'investmentSearchModel' => $investmentSearchModel,
            'investmentDataProvider' => $investmentDataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */





    public function actionTest() {
    $session = new Session;
    $session->open();
       print_r($session['lastPasswdUser']);
    }

    public function actionAccountValidate() {

        $model = new \common\models\UserValidation();
        
        if($model->load(Yii::$app->request->post())) {
            if($model->type==0) {
                $validateAccountStatus = User::find()
                    ->where(['status'=>10,'level'=>4])
                    ->andWhere(['mobile'=>$model->mobile])
                    ->one();
                if(!empty($validateAccountStatus)&& count($validateAccountStatus)>0) {
                    Yii::$app->session->setFlash('info', 'found a valid account with customer informations');
                    return $this->redirect(['/accounts/view','id'=>$validateAccountStatus->id]);
                }
                else {
                    Yii::$app->session->setFlash('info', 'please create a new account!');
                    return $this->redirect(['/accounts/create','mobile'=>$model->mobile]);  
                }
            }
            else if($model->type==1) {
                $validateAccountStatus = User::find()
                    ->where(['status'=>10,'level'=>4])
                    ->andWhere(['email'=>$model->email])
                    ->one();
                if(!empty($validateAccountStatus) && count($validateAccountStatus)>0) {
                    Yii::$app->session->setFlash('info', 'found a valid account with customer informations');
                    return $this->redirect(['/accounts/view','id'=>$validateAccountStatus->id]);
                }
                else {
                    Yii::$app->session->setFlash('info', 'please create a new account!');
                    return $this->redirect(['/accounts/create','email'=>$model->email]);  
                }
            }   
        }
        else {

            return $this->renderAjax('account-validate',[
                'model'=>$model,
            ]);

        }
        
    }

    public function actionCreate()
    {   
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                Yii::$app->session->setFlash('success', 'Account has been created');
                return $this->redirect(['/accounts/view','id'=>$user->id]);
            }
        }
        else {

            $mobile = yii::$app->request->get('mobile');
            $email = yii::$app->request->get('email');
            return $this->render('signup', [
                'model' => $model,
                'mobile' => $mobile,
                'email' => $email,
            ]);

        }

       
    }

    public function actionCreateInvestment() {
        $model = new SignupForm();
        $model->is_investment = true;
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->investment_signup()) {
                Yii::$app->session->setFlash('success', 'Account has been created');
                return $this->redirect(['/accounts/view','id'=>$user->id]);
            }
        }   
        else {

            $mobile = yii::$app->request->get('mobile');
            $model->email = $mobile.yii::$app->params['virtualEmailDomain'];
            return $this->render('signup_investment', [
                'model' => $model,
                'mobile' => $mobile
            ]);

        }
    }

    public function actionJoinNewScheme($user_id) {
        
        $model = new SignupForm();
        if($model->load(Yii::$app->request->post())) {
            $newSchemeJoin = $model->newSchemeJoin();
            if($newSchemeJoin=='true') {
                Yii::$app->session->setFlash('success','New Scheme has been registered!');
                return $this->redirect(['/accounts/view', 'id'=>$user_id]);

            }
        }
        else {
            return $this->render('join-new-scheme', [
                'model' => $model,
                'userId'=>$user_id,
            ]);
        }


    }

    public function actionReciptTest() {
        return $this->render('_test');
    }

    public function actionRecipt($join_id) {

        $model = SchemeJoins::findOne($join_id);
        $userModal = User::find()->where(['id'=>$model->u_id])->one();
        $addressModal = B2cAddress::find()->where(['u_id'=>$model->u_id])->one();
        
        
        
        $content = $this->renderPartial('_test',[
            'model'=>$model,
            'userModal' => $userModal,
            'addressModal' => $addressModal,
            'cacheP' => $this->getPrintPasswordVisibility($userModal->id),
        ]);
    
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_LETTER, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,
            'marginTop' => 0,
            'marginRight' => 0,
            'marginLeft' => 0,
            'marginBottom' => 0,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => yii::$app->params['RegistrationPrefix'].$model->id.' ACKNOWLEDGEMENT'],
             // call mPDF methods on the fly
            'methods' => [ 
                //'SetHeader'=>['Powered By EveryPay'], 
               // 'SetFooter'=>['Powered By EveryPay'],
            ]
        ]);
        
    // return the pdf output as per the destination setting
    return $pdf->render(); 
    }











    public function actionPasswordGenerate() 
    {       
        return $this->renderAjax('_password-generate');
    }


    

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at=strtotime(date('y-m-d h:m:s'));
           // echo'<pre>'; print_r($model); die();
            $model->save(false);            
            Yii::$app->session->setFlash('success',' '.$model->username.' Account has been Updated !');
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getPrintPasswordVisibility($userId) {
        $session = new Session;
        $session->open();
        $cachePassword = $session['lastPasswdUser'];
        if($cachePassword['userId']==$userId) {
            return $cachePassword['cacheP'];
        }
        else {
            return 'Not Availabe';
        }
    }
    
}
