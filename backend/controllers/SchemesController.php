<?php

namespace backend\controllers;

use Yii;

use backend\models\search\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

//Models
use backend\models\SchemeJoins;
use backend\models\User;


class SchemesController extends AdminController
{
    
    public function actionView($join_id) {

    	$schemeJoinsModel = SchemeJoins::findOne($join_id);
    	if(empty($schemeJoinsModel)) {
    		throw new NotFoundHttpException('The requested scheme join dose not exist.');
    	}
    	$userModel = User::findOne($schemeJoinsModel->u_id);
    	
    	return $this->render('view',[
    		'schemeJoinsModel' => $schemeJoinsModel,
    		'userModel' => $userModel,
    	]);

    }


    public function actionIndex()
    {
        return $this->render('index');
    }

}
