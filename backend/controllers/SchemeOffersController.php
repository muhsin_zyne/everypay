<?php

namespace backend\controllers;

use Yii;
use backend\models\SchemeOffers;
use backend\models\search\SchemeOffersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;

/**
 * SchemeOffersController implements the CRUD actions for SchemeOffers model.
 */
class SchemeOffersController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SchemeOffers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchemeOffersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SchemeOffers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SchemeOffers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SchemeOffers();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = $this->getNowTime();
            $model->updated_at = $this->getNowTime();
            if($model->offer_type=='month') {
                $model->bonus_cash = null;
                $model->bonus_gold = null;
            }
            else if ($model->offer_type=='cash') {
                $model->bonus_month = null;
                $model->bonus_gold = null;
            }
            else if ($model->offer_type=='gold') {
                $model->bonus_month = null;
                $model->bonus_cash = null;
            }
            $model->save();
            Yii::$app->session->setFlash('success',' New Offer '.$model->offer_label.' has been Created !');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SchemeOffers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = $this->getNowTime();
            if($model->offer_type=='month') {
                $model->bonus_cash = null;
                $model->bonus_gold = null;
            }
            else if ($model->offer_type=='cash') {
                $model->bonus_month = null;
                $model->bonus_gold = null;
            }
            else if ($model->offer_type=='gold') {
                $model->bonus_month = null;
                $model->bonus_cash = null;
            }

            $model->save();
            Yii::$app->session->setFlash('success','Offer '.$model->offer_label.' has been Updated !');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SchemeOffers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success','Offer has been deleted !');
        return $this->redirect(['index']);
    }

    /**
     * Finds the SchemeOffers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchemeOffers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchemeOffers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
