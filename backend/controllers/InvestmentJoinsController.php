<?php

namespace backend\controllers;

use Yii;
use backend\models\InvestmentJoins;
use backend\models\search\InvestmentJoinsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;
use backend\models\User;

/**
 * InvestmentJoinsController implements the CRUD actions for InvestmentJoins model.
 */
class InvestmentJoinsController extends AdminCOntroller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InvestmentJoins models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvestmentJoinsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAccountValidate() {
        $model = new \common\models\UserValidation();
        if($model->load(yii::$app->request->post())) {
            $validateAccountStatus = User::find()
                ->where(['status'=>10,'level'=>4])
                ->andWhere(['mobile'=>$model->mobile])
                ->one();
            if(!empty($validateAccountStatus)&& count($validateAccountStatus)>0) {
                Yii::$app->session->setFlash('info', 'found a valid account with customer informations');
                return $this->redirect(['/accounts/view','id'=>$validateAccountStatus->id]);
            }
            else {
                Yii::$app->session->setFlash('info', 'please create a new account!');
                return $this->redirect(['/accounts/create-investment','mobile'=>$model->mobile]);  
            }
        }
        return $this->renderAjax('_form-account-validate',[
            'model' => $model,
        ]);
    }

    /**
     * Displays a single InvestmentJoins model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($guid)
    {
        return $this->render('view', [
            'model' => $this->findModel($guid),
        ]);
    }

    public function actionGetInvestmentReports() {
        if(yii::$app->request->post() && yii::$app->request->getMethod() == 'POST') {
            $requestData = yii::$app->request->post();
            if($requestData['guid'] == '') {
            }
            if($requestData['dateFrom']=='') {
                // if no date from just assing current year jan 1
                $requestData['dateFrom'] = date('Y-01-01');
            }
            if($requestData['dateTo']=='') {
                $requestData['dateTo']= date('Y-12-31');
            }
            $invest = new InvestmentJoins();
            $rowData  = $invest->getPaymentReports($requestData);
            $report = [];
            $yearDates = [];
            $i = 1;
            while ($i <= 12) {
                $string = $i < 10 ? '0'.$i.'' : $i;
                $yearDates[$string] =  [
                    'due_amount' => 0.00,
                    'actual_paid' => 0.00
                ];
                $i++;
            }
            foreach ($rowData as $key => $record) {
                if($record['status']!='success') {
                    $fetchedDate = date('m');
                    if(array_key_exists($fetchedDate,  $yearDates)) {
                        $yearDates[$fetchedDate]['due_amount'] = $yearDates[$fetchedDate]['due_amount'] + $record['amount']; 
                    }
                } else {
                    $fetchedDate = date('m');
                    if(array_key_exists($fetchedDate,  $yearDates)) {
                        $yearDates[$fetchedDate]['due_amount'] = $yearDates[$fetchedDate]['due_amount'] + $record['amount']; 
                        $yearDates[$fetchedDate]['actual_paid'] = $yearDates[$fetchedDate]['actual_paid'] + $record['actual_amount']; 
                    }

                }
            }
            $responseData  = [
                'month' => [],
                'paidArray' => [],
                'dueAmountArray' => [],
            ];
            foreach ($yearDates as $key => $item) {
                $month = date('F',strtotime(date('Y-'.$key.'-'.'01')));
                $responseData['month'][] = $month;
                $responseData['paidArray'][] = $item['actual_paid'];
                $responseData['dueAmountArray'][] = $item['due_amount'];
            }
            $response['status'] = true;
            $response['data'] = $responseData;
            return json_encode($response, JSON_PRETTY_PRINT);

        }
    }

    /**
     * Creates a new InvestmentJoins model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InvestmentJoins();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InvestmentJoins model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    protected function findModel($guid)
    {   
        $model = InvestmentJoins::find()
            ->where([
                'guid' => $guid
            ])
            ->one();
        if($model) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
