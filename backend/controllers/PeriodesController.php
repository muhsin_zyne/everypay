<?php

namespace backend\controllers;

use Yii;
use backend\models\Periodes;
use backend\models\search\PeriodesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AdminController;
use backend\models\ActivityLog;

/**
 * PeriodesController implements the CRUD actions for Periodes model.
 */
class PeriodesController extends AdminController
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Periodes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PeriodesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Periodes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Periodes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Periodes();
        
        $activityLog = new ActivityLog();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = $this->getNowTime();
            $model->updated_at = $this->getNowTime();
            $model->save();
            //$activityLog->newLog('1','Periodes',$model->id);

            Yii::$app->session->setFlash('success','Periode '.$model->months.' has been Created !');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Periodes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->updated_at = $this->getNowTime();   
            $model->save();
            Yii::$app->session->setFlash('success','Periode '.$model->months.' has been Updated !');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Periodes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        $deleteModel = $this->findModel($id);
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success','Periode '.$deleteModel->months.' has been Deleted !');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Periodes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Periodes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Periodes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
