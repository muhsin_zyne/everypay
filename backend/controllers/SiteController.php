<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\components\AdminController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\AdminData;

/**
 * Site controller
 */
class SiteController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','profile','save-signature'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionProfile() {

        $activeUser = $this->getActiveUser();
        //if not created the adminData
        if($activeUser->adminData==null) {
            $adminData = new AdminData();
            $adminData->u_id = $activeUser->id;
            $adminData->save(false);
        }
        

        return $this->render('profile',[
            'activeUser' => $activeUser,
        ]);
    }

    public function actionSaveSignature() {

        $activeUser = $this->getActiveUser();

        if(yii::$app->request->post()) {
            $img = $_POST['base64data']; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $uid = uniqid(time(), true);
            $fileName = $uid.'_sign.png';
            $fileLocation = '../.././'.yii::$app->params['storePath'].'/admin/signature/';
            if (!file_exists($fileLocation)) {
                mkdir($fileLocation, 0777, true);
            }
            if(file_exists($fileLocation.$activeUser->adminData->signature)) {
                unlink($fileLocation.$activeUser->adminData->signature);
            }
            file_put_contents($fileLocation.$fileName, $data);
            
            $activeUser->adminData->signature = $fileName;
            $activeUser->adminData->save();
           
            return yii::$app->params['rootUrl'].'/'.yii::$app->params['storePath'].'/admin/signature/'.$activeUser->adminData->signature;
        }
        
    }

    public function actionIndex()
    {   
      return $this->render('index');
    }

    public function actionLogin()
    {
        $this->layout='loginLayout';
        if (!Yii::$app->user->isGuest) {
            
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } 
        else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
