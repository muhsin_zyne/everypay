
<?php
if(!is_dir(__DIR__.'/assets')) {
	mkdir(__DIR__.'/assets');
}

// Loading the domain configuration
$config_loader =  config_loader();
if(!file_exists(__DIR__ . '/../../common/config/'.$config_loader)) {
	// if not a valid configuration found exception for loader
	$config_loader = 'main-local.php';
}

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/../../common/config/main-local.php'),
    require(__DIR__ . '/../../common/config/'.$config_loader), //costom config file as per the domain name
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
);

$application = new yii\web\Application($config);
$application->run();


function config_loader() {
	$ignore_naming = ["admin."];
	$config_loader =  $_SERVER['HTTP_HOST'].'-config.php';
	foreach ($ignore_naming as $key => $string) {
		$config_loader = str_replace($string, "", $config_loader);
	}
	return $config_loader;
}