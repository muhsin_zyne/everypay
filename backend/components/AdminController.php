<?php
namespace backend\components;

use yii;
use backend\models\User;

class AdminController extends \yii\web\Controller

{	

	public $title;
    public $activeUser;
    
    public $globalAdminPermission = [
        'Index' => [
                        'site/index',
                        'site/error',
                        'site/logout',
                        'site/login',
                        'site/profile',
                        'site/save-signature',
                    ],
        'Users' => ['users/index', 'site/error', 'site/logout','site/login'],
        'Packages' => ['packages/index', 'packages/create', 'packages/update','packages/view','packages/delete'],
        'Periodes' => ['periodes/index', 'periodes/create', 'periodes/update','periodes/view','periodes/delete'],
        'Scheme-Offers' => ['scheme-offers/index','scheme-offers/create','scheme-offers/update','scheme-offers/view','scheme-offers/delete'],
        
        'Scheme Joins' =>   [
                                'scheme-joins/index',
                                'scheme-joins/scan-join',
                                'scheme-joins/view',
                                'scheme-joins/view-pending-payments',
                                'scheme-joins/accept-payment',
                                'scheme-joins/api-payment-calculations',
                            ],
        'Investment Joins' =>   [
                                'investment-joins/index',
                                'investment-joins/view',
                                'investment-joins/get-investment-reports',
                                'investment-joins/account-validate',
                            ],
        'Customer Accounts' => [
            'accounts/update-user-details',
            'accounts/get-offer-details',
            'accounts/account-validate',
            'accounts/join-new-scheme',
            'accounts/index', 'accounts/create',
            'accounts/view','accounts/update',
            'accounts/password-generate',
            'accounts/recipt',
            'accounts/recipt-test',
            'accounts/test',
            'accounts/create-investment'
        ],
        'Collection Roots' =>   [
                                'collection-roots/index',
                                'collection-roots/create',
                                'collection-roots/update',
                                'collection-roots/view',
                                'collection-roots/delete'
                            ],
        'B2C' => ['b2c-address/index','b2c-address/creat,','b2c-address/update','b2c-address/view','b2c-address/delete'],
        'Configurations' => [],
        'Offers' => [],
        'Test'=>['test/index','test/save'],
        'Schemes' => ['schemes/index','schemes/view'],
        'SMS Configuration' => ['sms-configurations/index','sms-configurations/view-sms-template'],
        'SMS Templates' => ['sms-templates/index','sms-templates/create', 'sms-templates/view', 'sms-templates/update', 'sms-templates/delete'],

        
    ];
    
    public $adminPermission = [
        'Index' => ['site/index', 'site/error', 'site/logout','site/login'],
    ];
    

    


    public $endUserLevels = [
        'admins' => [1,2],
        'endUserAdmin' =>[5],

    ];

    
    public function beforeAction($event) {
        
        if(!yii::$app->user->isGuest) {
            if(!$this->hasPermission(Yii::$app->controller->id."/".Yii::$app->controller->action->id)){
                throw new \yii\web\UnauthorizedHttpException();
            }
        }
        else if (yii::$app->user->isGuest) {   
            if(Yii::$app->controller->id."/".Yii::$app->controller->action->id=='site/login') {
                $this->layout='loginLayout';
            }
            else {
                $this->layout='loginLayout';
                return $this->redirect(['/site/login']);
            }
        }
        //$this->mainMenu= require('../config/Menu.php');
        return parent::beforeAction($event);
    }


    public function isVisible($key,$type=null) {
        if(isset(Yii::$app->user->id)) {
            $activeUser = $this->getActiveUser();
                switch ($activeUser->level) {
                    case '1': // global admin permission gose here
                        if(array_key_exists($key,$this->globalAdminPermission)) {
                            return true;
                        }
                        else {
                                return false;
                        }
                        
                        break;
                    case '2': // master admin permission gose here
                        if(array_key_exists($key,$this->adminPermission)) {
                            return true;
                        }
                        else {
                                return false;
                        }
                        
                        break;
                                       default:
                        return false;
                        break;
                }

        }
        else {
            return false;
        }
    }


    public function hasPermission($event) {
        


        if(isset(Yii::$app->user->id)) {
            $activeUser = $this->getActiveUser();
            
            if($activeUser->level=='1') {
                foreach ($this->globalAdminPermission as  $permission) {
                    if(in_array($event,$permission)) {
                        return true;
                    }
                }
                return false;
            }
            else if($activeUser->level=='2') {
                foreach ($this->adminPermission as  $permission) {
                    if(in_array($event,$permission)) {
                        return true;
                    }
                }
                return false;
            }
                
        }
    } //.has permission ending

    public function getNowTime() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('Y-m-d H:i:s');
        return $dateTime;
    }

    public function getActiveUser() {
        
        if(isset(yii::$app->user->id)) {

            $activeUser = User::find()->where(['id' =>yii::$app->user->id])->one();
            if($activeUser->image==""){
                $activeUser->image = 'avatar-01.png';
            }
            return $activeUser;
        }
        
    }

    protected function getNowDigit() {
        date_default_timezone_set('Asia/Calcutta');
        $dateTime = date('YmdHis');
        return $dateTime;
    }

    protected function valid_formats($type) {
        switch ($type) {
            case 'image':
                $valid_formats = array("jpg", "png", "gif", "jpeg","JPG");
                break;
            
            default:
                $valid_formats = null;
                break;
        }

        return $valid_formats;
    }


    public function printWatermark($exceptionTitle,$exeptionInfo) {

        $html ='';
        $html.='<div class="row">
                    <div class="col-xs-12">
                        <div class="important-watermark">
                            <h1 class="watermark"> '.$exceptionTitle.' </h1>
                            <span> '.$exeptionInfo.' </span>      
                        </div>
                    </div>
                </div>';

        return $html;
    }

    public function getGuid(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                    .substr($charid, 0, 8).$hyphen
                    .substr($charid, 8, 4).$hyphen
                    .substr($charid,12, 4).$hyphen
                    .substr($charid,16, 4).$hyphen
                    .substr($charid,20,12)
                    .chr(125);// "}"
            return trim($uuid, '{}');
            //return $uuid;
        }
    }

} // main class ending
