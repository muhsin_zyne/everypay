<?php
namespace backend\components;


use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CustomPdfRender extends Model
{
    

    public function loadContentToRecipt($paymentRecord) {
        if(empty($paymentRecord)) {
            return null;
        }
        $loader = [];
        if($paymentRecord->collection_type=='offline') {
            $loader = $this->loadPaymentData('offline',$paymentRecord);
        }
        else if ($paymentRecord->collection_type=='online') {
            $loader = $this->loadPaymentData('online',$paymentRecord);
        }

        return $loader;
    }

    protected function loadPaymentData($type,$paymentRecord) {
        $responce = [];
        $activeUser = yii::$app->controller->getActiveUser();
        if($type=='offline') {
            $responce = [

            	'BillingName'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,72,12),
            		'content'	=> 	$activeUser->f_name.' '.$activeUser->l_name,
            	),
            	'BillingEmail'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,76,10),
            		'content'	=> 	$activeUser->email,
            	),
            	'UserName'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,80,10),
            		'content'	=> 	$activeUser->username,
            	),
            	'MobileNumber'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,84.5,10),
            		'content'	=> 	$activeUser->mobile,
            	),
            	'transactionId' => array(
                    'type'      => 'text',
                    'property'  => array(98,94.2,10),
                    'content'   => $paymentRecord->id.'-'.$paymentRecord->paymentOfflineModel->id,
                ),

                'SchemeLabel' => array(
                    'type'      => 'text',
                    'property'  => array(98,99.2,10),
                    'content'   => $paymentRecord->schemeJoins->plan_label,
                ),
                'billingFor' => array(
                    'type'      => 'text',
                    'property'  =>  array(98,104,10),
                    'content'   => date('M d, Y',strtotime($paymentRecord->billing_for)),
                ),
                'Amount' => array(
                    'type'      => 'text',
                    'property'  => array(98,109,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOfflineModel->amount),
                ),
                'GrandTotal' => array(
                    'type'      => 'text',
                    'property'  => array(98,114,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOfflineModel->grand_total),
                ),
                'Discount' => array(
                    'type'      => 'text',
                    'property'  => array(98,119,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOfflineModel->discount),
                ),
                'CollectedAmount' => array(
                    'type'      => 'text',
                    'property'  => array(98,124,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOfflineModel->collected_amount),
                ),
                'AmountDue' => array(
                    'type'      => 'text',
                    'property'  => array(98,129,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOfflineModel->balance_due),
                ),
                'PaymentType' => array(
                    'type'      => 'text',
                    'property'  => array(98,134,10),
                    'content'   => ucwords($paymentRecord->collection_type),
                ),
                'CollectionTime' => array(
                    'type'      => 'text',
                    'property'  => array(98,138.5,10),
                    'content'   => date('M d, Y h:i A',strtotime($paymentRecord->payment_received_at)),
                ),
				'QrCode'		=>	array(
                	'type'		=> 'img',
                	'property'	=> array(150,50),
                	'content'	=>	'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='.$paymentRecord->id.'&file.png',
                ),
               
               
            ];
             
        }

        else if($type=='online') {
        	 $responce = [

            	'BillingName'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,72,12),
            		'content'	=> 	$activeUser->f_name.' '.$activeUser->l_name,
            	),
            	'BillingEmail'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,76,10),
            		'content'	=> 	$activeUser->email,
            	),
            	'UserName'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,80,10),
            		'content'	=> 	$activeUser->username,
            	),
            	'MobileNumber'	=> array(
            		'type'		=>	'text',
            		'property'	=>	array(30,84.5,10),
            		'content'	=> 	$activeUser->mobile,
            	),
            	'transactionId' => array(
                    'type'      => 'text',
                    'property'  => array(98,94.2,10),
                    'content'   => $paymentRecord->id.'-'.$paymentRecord->paymentOnlineModel->id,
                ),

                'SchemeLabel' => array(
                    'type'      => 'text',
                    'property'  => array(98,99.2,10),
                    'content'   => $paymentRecord->schemeJoins->plan_label,
                ),
                'billingFor' => array(
                    'type'      => 'text',
                    'property'  =>  array(98,104,10),
                    'content'   => date('M d, Y',strtotime($paymentRecord->billing_for)),
                ),
                'Amount' => array(
                    'type'      => 'text',
                    'property'  => array(98,109,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOnlineModel->amount),
                ),
                'GrandTotal' => array(
                    'type'      => 'text',
                    'property'  => array(98,114,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOnlineModel->grand_total),
                ),
                'Discount' => array(
                    'type'      => 'text',
                    'property'  => array(98,119,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOnlineModel->discount),
                ),
                'CollectedAmount' => array(
                    'type'      => 'text',
                    'property'  => array(98,124,10),
                    'content'   => money_format('%i',$paymentRecord->paymentOnlineModel->grand_total - $paymentRecord->paymentOnlineModel->discount ),
                ),
                'PaymentType' => array(
                    'type'      => 'text',
                    'property'  => array(98,129,10),
                    'content'   => ucwords($paymentRecord->collection_type),
                ),
                'CollectionTime' => array(
                    'type'      => 'text',
                    'property'  => array(98,133.5,10),
                    'content'   => date('M d, Y h:i A',strtotime($paymentRecord->payment_received_at)),
                ),
				'QrCode'		=>	array(
                	'type'		=> 'img',
                	'property'	=> array(150,50),
                	'content'	=>	'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='.$paymentRecord->id.'&file.png',
                ),
               
               
            ];
        }


        return $responce;
    }

    public function generatePdfConents($pdf,$loaderContents) {
        $pdf->SetFont('Helvetica');
        foreach ($loaderContents as $key => $item) {
            if($item['type']=='text') {
                $pdf->SetXY($item['property'][0],$item['property'][1]);
                $pdf->setFontSize($item['property'][2]);
                $pdf->Write(0, $item['content']);
            }
            if($item['type']=='img') {
            	$pdf->Image($item['content'], $item['property'][0], $item['property'][1]);
            }
        }

        return $pdf;
    }
    
}


?>