<?php 

namespace backend\components\widgets;
use yii\base\Widget;
use yii\helpers\Html;

class MaterialRadioButton extends Widget
{
    public $label;
    public $id;
    public $value;
    public $name;
    public $checked;
    
    public function init()
    {   

        if($this->label===null) {
            $this->label ='Widget Label';
        }
        if($this->id===null) {
            $this->id = 'WidgetId';
        }
        if($this->value===null) {
            $this->value = '';
        }
        if($this->name===null) {
            $this->name = 'widgetName';
        }
        if($this->checked===null||$this->checked=='false') {
            $this->checked ='';
        }
        else if($this->checked=='true') {
            $this->checked ='checked';
        }

        $results = $this->getRadioButton();
        echo $results;
        
    }

    public function run()
    {   
        
    }

    protected function getRadioButton() {
        $html ='';
        $html.='<label class="radio-button radio-button--material">';
        $html.='<input type="radio" class="radio-button__input radio-button--material__input" name="'.$this->name.'" id="'.$this->id.'" checked="'.$this->checked.'" value="'.$this->value.'">';
        $html.='<div class="radio-button__checkmark radio-button--material__checkmark"></div>
            '.$this->label.'
            </label>';

        return $html;

    }
}


?>