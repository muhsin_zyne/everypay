<?php

namespace backend\components;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ViewFunction extends Model
{
    public function bootsrapLabel($type = 'string' , $value='active') {
        if($type=='string') {
            switch ($value) {
                case 'active':
                    return '<span class="badge bg-green">'.ucwords($value).'</span>';
                    break;
                case 'expired':
                    return '<span class="badge bg-red">'.ucwords($value).'</span>';
                    break;
                 case 'suspended':
                    return '<span class="badge bg-yellow">'.ucwords($value).'</span>';
                    break;                
                default:
                    # code...
                    break;
            }
        }
        if($type=='number') {
            switch ($value) {
                case '0':
                    return '<span class="badge bg-red"> Disabled </span>';
                    break;
                case '1':
                    return '<span class="badge bg-green"> Active </span>';
                    break;               
                default:
                    # code...
                    break;
            }
        }

    } //bootstrap status label redm green yellow etc

    public function appendMonth($number=0) {
        return $number==1|| $number==0 ? 'Month' : 'Months';
    }

    public function printWatermark($exceptionTitle,$exeptionInfo) {

        $html ='';
        $html.='<div class="row">
                    <div class="col-xs-12">
                        <div class="important-watermark">
                            <h1 class="watermark"> '.$exceptionTitle.' </h1>
                            <span> '.$exeptionInfo.' </span>      
                        </div>
                    </div>
                </div>';

        return $html;
    }



    
}
