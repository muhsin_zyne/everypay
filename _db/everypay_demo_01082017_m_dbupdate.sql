-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2017 at 04:33 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `everypay_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activity_data` text NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activity_data`, `location`) VALUES
(1, 'a new period created', 'packages'),
(2, 'Package updated', 'packages');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `log` text NOT NULL,
  `time` datetime NOT NULL,
  `model_refrence` varchar(55) NOT NULL,
  `log_refrence` int(10) NOT NULL,
  `logged_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log`, `time`, `model_refrence`, `log_refrence`, `logged_by`) VALUES
(1, 'a new period created', '2017-07-26 22:38:42', 'Periodes', 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `b2c_address`
--

CREATE TABLE `b2c_address` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `type` enum('0','1') NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `pincode` varchar(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `b2c_address`
--

INSERT INTO `b2c_address` (`id`, `u_id`, `type`, `address`, `pincode`, `status`) VALUES
(1, 61, '0', 'test full address gose here', '895172', '0');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495384146),
('m130524_201442_init', 1495384148);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package_label` varchar(255) NOT NULL,
  `package_value` float(100,2) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_label`, `package_value`, `status`) VALUES
(21, 'Regular 5,000.00', 5000.00, '0'),
(22, 'Regular Advanced 10,000.00', 10000.00, '0'),
(23, 'VIP', 50000.00, '0'),
(24, 'VIP Premium', 100000.00, '0');

-- --------------------------------------------------------

--
-- Table structure for table `periodes`
--

CREATE TABLE `periodes` (
  `id` int(11) NOT NULL,
  `months` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periodes`
--

INSERT INTO `periodes` (`id`, `months`, `status`, `created_at`, `updated_at`) VALUES
(22, 12, '0', '2017-07-29 16:17:28', '2017-07-29 16:17:28'),
(23, 24, '0', '2017-07-29 16:17:37', '2017-07-29 16:17:37');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_joins`
--

CREATE TABLE `scheme_joins` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `plan_amount` float(20,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_type` varchar(30) NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(11,2) DEFAULT NULL,
  `offer_valid_from` date NOT NULL,
  `offer_valid_true` date NOT NULL,
  `offer_availing_on` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_joins`
--

INSERT INTO `scheme_joins` (`id`, `u_id`, `plan_amount`, `duration`, `created_at`, `valid_from`, `valid_true`, `offer_type`, `offer_label`, `bonus_month`, `bonus_cash`, `bonus_gold`, `offer_valid_from`, `offer_valid_true`, `offer_availing_on`) VALUES
(1, 61, 5000.00, 12, '2017-08-01 08:00:20', '2017-08-01', '2018-10-01', 'month', '2 Months Free Bonus', 2, NULL, NULL, '2017-07-31', '2017-08-03', 'anytime');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_offers`
--

CREATE TABLE `scheme_offers` (
  `id` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `offer_type` enum('month','cash','gold') NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(10,2) DEFAULT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_availing_on` enum('anytime','end_of') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_offers`
--

INSERT INTO `scheme_offers` (`id`, `status`, `offer_type`, `offer_label`, `bonus_month`, `bonus_cash`, `bonus_gold`, `valid_from`, `valid_true`, `offer_availing_on`, `created_at`, `updated_at`) VALUES
(7, '0', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-07-31', '2018-10-25', 'anytime', '2017-07-30 18:23:51', '2017-07-30 21:53:48'),
(8, '0', 'cash', '2500.00 Cash Bonus', NULL, 2500.85, NULL, '2017-07-30', '2018-10-31', 'end_of', '2017-07-30 19:10:27', '2017-07-31 17:06:52'),
(9, '0', 'gold', '8.00 grams Gold', NULL, NULL, 8.00, '2017-07-30', '2020-07-15', 'end_of', '2017-07-30 19:15:48', '2017-07-30 21:55:36'),
(10, '0', 'month', '2 Months Free Bonus', 2, NULL, NULL, '2017-07-31', '2017-08-03', 'anytime', '2017-07-30 20:07:23', '2017-07-30 21:55:50');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `level` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) DEFAULT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `display_name`, `l_name`, `f_name`, `user_type`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `image`, `status`, `created_at`, `updated_at`, `level`, `created_by`, `mobile`) VALUES
(1, 'admin', 'Muhammed Muhsin', NULL, NULL, 'admin', '4gy_jvkWiDIQmT79sk_D-rEy4LBQu1sR', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', 'z2poo7YCYgTjjEJW2gqQzprNyeoovUkZ_1500817285', 'muhsin.zyne@gmail.com', '', 10, 1487064707, 1500817285, 1, NULL, NULL),
(61, 'demo@61', NULL, 'AMITH', 'MARTIN', 'user', '9cvyjX_4Z6FafXj4weK6q_CpCSs1xhmf', '$2y$13$mcSW.G7aUwmEPvi34AQ7mOkSNwriM6FXnaYrYgNlLfQrXE.NqclW.', NULL, 'martin@gmail.com', '', 10, 1501554619, 1501554619, 4, 1, '9045876992');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b2c_address`
--
ALTER TABLE `b2c_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periodes`
--
ALTER TABLE `periodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `b2c_address`
--
ALTER TABLE `b2c_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `periodes`
--
ALTER TABLE `periodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
