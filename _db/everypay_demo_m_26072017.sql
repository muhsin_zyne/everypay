-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2017 at 07:18 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `everypay_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activity_data` text NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activity_data`, `location`) VALUES
(1, 'a new period created', 'packages'),
(2, 'Package updated', 'packages');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `log` text NOT NULL,
  `time` datetime NOT NULL,
  `model_refrence` varchar(55) NOT NULL,
  `log_refrence` int(10) NOT NULL,
  `logged_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log`, `time`, `model_refrence`, `log_refrence`, `logged_by`) VALUES
(1, 'a new period created', '2017-07-26 22:38:42', 'Periodes', 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `b2c_address`
--

CREATE TABLE `b2c_address` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `type` enum('0','1') NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `pincode` varchar(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495384146),
('m130524_201442_init', 1495384148);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `package_id` int(11) NOT NULL,
  `package_label` varchar(255) NOT NULL,
  `package_value` float(100,2) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`package_id`, `package_label`, `package_value`, `status`) VALUES
(21, 'Regular', 5000.00, '0'),
(22, 'Regular Advanced', 10000.00, '0'),
(23, 'VIP', 50000.00, '0'),
(24, 'VIP Premium', 100000.00, '0');

-- --------------------------------------------------------

--
-- Table structure for table `periodes`
--

CREATE TABLE `periodes` (
  `id` int(11) NOT NULL,
  `months` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periodes`
--

INSERT INTO `periodes` (`id`, `months`, `status`, `created_at`, `updated_at`) VALUES
(21, 11, '0', '2017-07-26 22:43:17', '2017-07-26 22:43:17');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `level` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) DEFAULT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `display_name`, `l_name`, `f_name`, `user_type`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `image`, `status`, `created_at`, `updated_at`, `level`, `created_by`, `mobile`) VALUES
(1, 'admin', 'Muhammed Muhsin', NULL, NULL, 'admin', '4gy_jvkWiDIQmT79sk_D-rEy4LBQu1sR', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', 'z2poo7YCYgTjjEJW2gqQzprNyeoovUkZ_1500817285', 'muhsin.zyne@gmail.com', '', 10, 1487064707, 1500817285, 1, NULL, NULL),
(2, 'ameer', 'Demo User', NULL, NULL, 'user', 'wPfQLGiLmRmKfyXSl6sMMoFoQbONAA_h', '$2y$13$voSNW/D2fGsP2SayvbuUzOABYu/5RR5wiLA9tUVdwPbCorvUvPOdO', NULL, 'ameer@gmail.com', '', 10, 1500308993, 1500308993, 4, NULL, NULL),
(18, 'dsjdshdshd', NULL, NULL, NULL, 'user', 'SHulCeKDsYqxp8z-PlGLfKkEWQ8sZmOu', '$2y$13$noSFwi0FoEGfpMnJbB6/MuqqardTscYG/cjRwEoebt9HNT4pJfU6a', NULL, 'sdsdd@gmail.cim', '', 10, 1500486791, 1500486791, 4, 1, NULL),
(19, 'muhsin@123', NULL, 'muhsin', 'muhammed', 'user', 'HOyNQznScYPge0KmGAgGtPcBWMgHBEqo', '$2y$13$fICf89.Gp0Pc4qtUP2if9eIhLWM4NCPhmUyLRofARPV6bhB.kG3Ce', NULL, 'muhsin@gmail.com', '', 10, 1500487037, 1500487037, 4, 1, '8089435573'),
(26, 'demo@26', NULL, 'jsdsjdsjd', 'sasjsd', 'user', 'IEePvXAGKA5jPIUXGlNOY-SpnX3I7XnP', '$2y$13$fvLbEhutgszhw2RykICCtOWbSQ9UlY5/yXHGA8fTWvQBpQCbNVkCi', NULL, 'hammy@gmail.com', '', 10, 1500488445, 1500488445, 4, 1, '1234567890'),
(27, 'demo@27', NULL, 'jsjdsdj', 'muhammed', 'user', 'hQ1y818yMsrppzDgq7wV_va8NjtbxBPg', '$2y$13$3N4FqAFwZecfcWb2JTQr.eYmeQe/3PsFxf5iZB1azf4CAFSccHoUq', NULL, 'hammysdsd@gmail.com', '', 10, 1500488674, 1500488674, 4, 1, '8089435573'),
(28, 'demo@28', NULL, 'dsdsds', 'muhammed', 'user', 'PQdhqoVS9Yl3-V0vlJNJJkbkDKKI_t0r', '$2y$13$nh4ZRd.LdpAaExFRRcZbIuZihWsCe40dqMDEI.gKk.zObUCJ9igmu', NULL, 'hammyssdsdsdsd@gmail.com', '', 10, 1500488713, 1500488713, 4, 1, '8089435573'),
(29, 'demo@29', NULL, 'dsds', 'sd', 'user', '4tVTcwOzNDHzXEnsY4HrKN4n0OpfNyvR', '$2y$13$KNtDrI68zJTvPebw2Td.QOc67cf3N1/WDRAFqFuzyAIzF0x0FCMHi', NULL, 'hammyssdssddsd@gmail.com', '', 10, 1500490535, 1500490535, 4, 1, '1111111122'),
(30, 'demo@30', NULL, 'cxcxc', 'dfdfdffd', 'user', 'pfOwcWBudhmN4tgIILQq-JankTpl_yBM', '$2y$13$MFu7iCqP35pHOrlGaBY.cOP7Rc/5S/zTVzA3Pp4LDv827qe7M9HS.', NULL, 'fffdfdssddsfjsd@ab.xom', '', 10, 1500490862, 1500490862, 4, 1, '1234567890'),
(31, '174378518', NULL, 'sdsdsds', 'samole', 'user', 'HhKjMLAVVGoNHGmXf7c1E782hxxdL_3q', '$2y$13$sGc0judegV71PX5RzhsFdOF6cemTkJi2y3kg1Y8jadNUIuEQXYiPS', NULL, 'hammsdsdsdysdsd@gmail.com', '', 10, 1500491121, 1500491121, 4, 1, '1111111122'),
(32, '1075952', NULL, 'muhsin', 'muhammed', 'user', '07njd62Nbxs9O_fJ0XZRTE-ekJGNc7BM', '$2y$13$Q2g7bM1VeMoVlvTeKOyZt.t6bWW.jcjzGKMpA0snBWtF6Aqq4Hxay', NULL, 'adminsdsdQ@fjd.com', '', 10, 1500491163, 1500491163, 4, 1, '8089435573'),
(33, '764223133', NULL, 'sdsdsds', 'muhammed', 'user', '9P8Y2cYsLCzHfokSL4ha7vAM3EgblZre', '$2y$13$SBkdvjJ4/6IPO7Rc6a4LSOioZPN4.VItn8oq/A54lIa9C7k3RfI3i', NULL, 'dsdsddsdssdsdhgfhdhs@gamil.com', '', 10, 1500491384, 1500491384, 4, 1, '1234567890'),
(34, 'demo@34', NULL, 'muhsin', 'muhammed', 'user', 'zn-MvCnCJ19pyoQoY9yrc3Ti13OHFPgR', '$2y$13$yTEcB5qha2RZpicA5SeDB.5ZmmzifnekeGiST6HIpQNyx8YFEdXq6', NULL, 'sdsddd@fjd.com', '', 10, 1500491433, 1500491433, 4, 1, '8089435573'),
(35, 'demo@35', NULL, 'muhsinss', 'muhamssmed', 'user', '5F1mC18HvwkBoG65t7ou_eKIohZ4weUF', '$2y$13$SzdQC/Fv3AXqFhexmbCApOPmHsG6ib3duFUivk.04LwOEsJ6621dG', NULL, 'sdsdssdd@fjd.com', '', 10, 1500491453, 1500491453, 4, 1, '8089435573'),
(36, 'demo@36', NULL, 'sdsdsds', 'muhammed', 'user', '9Jrnis0nMUbGMCrpeORBvCmQFZXN2ws4', '$2y$13$VFVI.a7JwJeefkTKv7AYk.s7WyPVx/bzpctbYbFGTShieU8Y6DATe', NULL, 'admsdsdsinsdsdQ@fjd.com', '', 10, 1500491652, 1500491653, 4, 1, '8089435573'),
(37, 'demo@37', NULL, 'sdsdsds', 'ddf', 'user', 'pOXML7VmoVs7QnymRu9JjE-Mh_LkBVqy', '$2y$13$DX8s1B5KlcYvwS.u7f224exhtjC.skpRybV7Jd8a3BRntAL8gxCIa', NULL, 'sampdsdslego@gmail.com', '', 10, 1500491683, 1500491683, 4, 1, '1234567890'),
(38, 'demo@38', NULL, 'dsdsdd', 'sdsdsds', 'user', 'CP2-uSWMStlFVsT6CDGW3zjUb9KgTZOB', '$2y$13$jFC4rfh9Qig8h5vLw4PSjeCqKJqVbNU4Uno0cSIIHAP9XhYQK68Zq', NULL, 'dsdsddsdssdsdssdsdhgfhdhs@gamil.com', '', 10, 1500491927, 1500491927, 4, 1, '8089435573'),
(39, 'demo@39', NULL, 'dsdsds', 'sdsds', 'user', 'Az0E02MAsJ6yN-NGMCvM7dyFAd8xi3OF', '$2y$13$skdedGoAjyRK64x2F8/JfOwqU7AXStb4LoTQOIfB7Vl9H4mIwa/CG', NULL, 'admidsdsdnsdsdQ@fjd.com', '', 10, 1500492001, 1500492001, 4, 1, '8089435573'),
(40, 'demo@40', NULL, 'hdsdhhd', 'shdshdh', 'user', 'uqhhIfVxsxaYnHK0l-2eT0U0AYq58ABc', '$2y$13$mgeLd7KQTQXjq9Hpb5vYBuDxKPmY0ZLaKWmq80snqKTDDK1OeuPXW', NULL, 'hdshddhsdh@gmail.com', '', 10, 1500497870, 1500497870, 4, 1, 'hhsdhddshd'),
(41, 'demo@41', NULL, 'test', 'test', 'user', 'xM6ZlP4x4X7lOnGCt5kS0oI6BSvP_tXw', '$2y$13$skmYeNhh0j.WQVyWxomhBuc7UQQUSfUzmWAdVMrKeDYd.j9RZAte2', NULL, 'test@gmail.comdsdh', '', 10, 1500563753, 1500563753, 4, 1, '3743747374'),
(42, 'demo@42', NULL, 'test', 'test', 'user', 'MKDILVSoXOejbUQ4Hhmh57_OgTA0XusI', '$2y$13$ujKNGN4zkMxg3yEeJ78Id.kykdZpWAqo/Zax0qfGIrsphtE6hBZqK', NULL, 'tesdfdfdt@gmail.comdsdh', '', 10, 1500563806, 1500563806, 4, 1, '3743747374'),
(43, 'demo@43', NULL, 'test', 'test', 'user', 'gos3zMecrng2vgkjXKYjs9h9k8FnjCTZ', '$2y$13$KwWHPxcQk8x3Ehsqr5MnouBXv8YBw0f6rSKaHu/01VRh5RNFJqFT6', NULL, 'test@gmaid.cm', '', 10, 1500567252, 1500567252, 4, 1, '3433434343'),
(44, 'demo@44', NULL, 'dsdsdsd', 'sddsd', 'user', 'Etp_PS2ojCcBc9nGxd1EvG6b80ksDBQx', '$2y$13$38rtwOzYmL.B7VjSy5amSeMzQETFCJJdJVfOkDBEdsOA0y9hbwNIS', NULL, 'sdsdsddsdsdsd@gmail.cim', '', 10, 1500748828, 1500748829, 4, 1, '3e434434');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b2c_address`
--
ALTER TABLE `b2c_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `periodes`
--
ALTER TABLE `periodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `b2c_address`
--
ALTER TABLE `b2c_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `periodes`
--
ALTER TABLE `periodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
