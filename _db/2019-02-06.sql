-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2019 at 05:17 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `everypay_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activity_data` text NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activity_data`, `location`) VALUES
(1, 'a new period created', 'packages'),
(2, 'Package updated', 'packages');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `log` text NOT NULL,
  `time` datetime NOT NULL,
  `model_refrence` varchar(55) NOT NULL,
  `log_refrence` int(10) NOT NULL,
  `logged_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_data`
--

CREATE TABLE `admin_data` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `signature` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_data`
--

INSERT INTO `admin_data` (`id`, `u_id`, `signature`) VALUES
(1, 1, '15104025535a06e9f951f452.78592916_sign.png');

-- --------------------------------------------------------

--
-- Table structure for table `appointment_schedules`
--

CREATE TABLE `appointment_schedules` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `scheduled_for` datetime NOT NULL,
  `type` enum('investment_collection','payment_collection') NOT NULL,
  `investment_id` int(11) DEFAULT NULL,
  `scheme_join_id` int(11) DEFAULT NULL,
  `investment_due_account_id` int(11) DEFAULT NULL,
  `status` enum('scheduled','canceld','completed') NOT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL,
  `completed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment_schedules`
--

INSERT INTO `appointment_schedules` (`id`, `u_id`, `agent_id`, `scheduled_for`, `type`, `investment_id`, `scheme_join_id`, `investment_due_account_id`, `status`, `is_deleted`, `created_at`, `updated_at`, `created_by`, `updated_by`, `completed_at`, `completed_by`) VALUES
(4, 27, 1, '2018-11-01 00:00:00', 'investment_collection', 5, NULL, 5, 'scheduled', '0', '2018-12-14 00:38:02', NULL, 1, NULL, NULL, NULL),
(6, 28, 1, '2018-11-01 00:00:00', 'investment_collection', 6, NULL, 6, 'scheduled', '0', '2018-12-14 00:40:13', NULL, 1, NULL, NULL, NULL),
(7, 29, 1, '2018-11-01 00:00:00', 'investment_collection', 7, NULL, 7, 'scheduled', '0', '2018-12-14 00:40:32', NULL, 1, NULL, NULL, NULL),
(8, 30, 1, '2018-11-01 00:00:00', 'investment_collection', 8, NULL, 8, 'scheduled', '0', '2018-12-14 00:43:32', NULL, 1, NULL, NULL, NULL),
(9, 31, 1, '2018-11-01 00:00:00', 'investment_collection', 9, NULL, 9, 'scheduled', '0', '2018-12-14 00:51:46', NULL, 1, NULL, NULL, NULL),
(10, 32, 1, '2018-11-01 00:00:00', 'investment_collection', 10, NULL, 10, 'scheduled', '0', '2018-12-14 00:51:46', NULL, 1, NULL, NULL, NULL),
(11, 33, 1, '2018-11-01 00:00:00', 'investment_collection', 11, NULL, 11, 'scheduled', '0', '2018-12-14 00:56:49', NULL, 1, NULL, NULL, NULL),
(12, 34, 1, '2018-11-01 00:00:00', 'investment_collection', 12, NULL, 12, 'scheduled', '0', '2018-12-14 00:56:49', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b2c_address`
--

CREATE TABLE `b2c_address` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `type` enum('0','1') NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `location` text,
  `pincode` varchar(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL,
  `roots` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `b2c_address`
--

INSERT INTO `b2c_address` (`id`, `u_id`, `type`, `address`, `location`, `pincode`, `status`, `roots`) VALUES
(25, 27, '0', 'Mothirapeedikakkal house', 'Kallor, Ernakulam Kerala', '679335', '0', '1'),
(26, 28, '0', 'Demo 1 Address', 'Kadavanthara, Ernakulam, Kerala', '123456', '0', '2'),
(27, 29, '0', 'Ajmal ', 'PanappalliNagar, Eranakulam Keralal', '438222', '0', '2'),
(28, 30, '0', 'Michu Address', 'Cusat, Kalamassery Kerala', '458577', '0', ''),
(29, 31, '0', 'Rashid Address', 'Edappalli, Ernakulam Kerala', '334444', '0', '2'),
(30, 32, '0', 'Navy Home Kochi', 'Mattancher, Kochi, Kerala', '343434', '0', ''),
(31, 33, '0', 'Mark Robust', 'Aluva, Ernakulam , Kerala', '472622', '0', '3'),
(32, 34, '0', 'Amal Akbar', 'Pattambi, Palakkad, Kerala', '473737', '0', '2'),
(33, 35, '0', 'Test Address', 'Edappali, Ernakulam Kerala', '223334', '0', ''),
(34, 36, '0', 'Test Address', 'Tharoor, Palakkad, Kerala', '343747', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `billing_generate_log`
--

CREATE TABLE `billing_generate_log` (
  `id` int(11) NOT NULL,
  `exicuted_date` varchar(2) NOT NULL,
  `exicuted_month` varchar(2) NOT NULL,
  `exicuted_year` int(4) NOT NULL,
  `status` enum('success','faild','suspended') NOT NULL,
  `type` enum('payment') NOT NULL,
  `exicuted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `slug` text NOT NULL,
  `content` longtext NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `collection_roots`
--

CREATE TABLE `collection_roots` (
  `id` int(11) NOT NULL,
  `root_name` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collection_roots`
--

INSERT INTO `collection_roots` (`id`, `root_name`, `status`) VALUES
(2, 'Edappalli', '1'),
(3, 'Kacherippadi', '1'),
(5, 'Fort Kochi', '1'),
(6, 'Mattanchery', '1'),
(7, 'Thammanam', '1'),
(8, 'Aluva', '1'),
(9, 'Kakkanad', '1'),
(10, 'Kalamassery', '1'),
(11, 'Varappuzha', '1'),
(12, 'Vytila Hub', '1'),
(13, 'Eramallor', '1'),
(14, 'Paravoor', '1'),
(15, 'Vypin', '1');

-- --------------------------------------------------------

--
-- Table structure for table `investment_accounts`
--

CREATE TABLE `investment_accounts` (
  `id` int(11) NOT NULL,
  `investment_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `amount` float(8,2) NOT NULL,
  `type` enum('dr','cr') NOT NULL,
  `account_agent` int(11) NOT NULL,
  `status` enum('pending','success','canceld','error') NOT NULL DEFAULT 'pending',
  `generated_at` datetime NOT NULL,
  `clearance_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment_bill_daily_log`
--

CREATE TABLE `investment_bill_daily_log` (
  `id` int(11) NOT NULL,
  `exicuted_date` datetime NOT NULL,
  `status` enum('success','error') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment_bill_weekly_log`
--

CREATE TABLE `investment_bill_weekly_log` (
  `id` int(11) NOT NULL,
  `exicuted_date` date NOT NULL,
  `status` enum('success','error') NOT NULL,
  `exicuted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investment_bill_weekly_log`
--

INSERT INTO `investment_bill_weekly_log` (`id`, `exicuted_date`, `status`, `exicuted_at`) VALUES
(6, '0000-00-00', 'success', NULL),
(7, '2018-12-10', 'success', '2018-12-10 22:00:31'),
(8, '2018-12-27', 'success', '2018-12-27 22:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `investment_due_accounts`
--

CREATE TABLE `investment_due_accounts` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `investment_id` int(11) NOT NULL,
  `amount` float(8,2) NOT NULL,
  `actual_amount` float(8,2) DEFAULT NULL,
  `generated_at` datetime NOT NULL,
  `status` enum('success','pending','rejected','canceld','scheduled') NOT NULL,
  `collected_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investment_due_accounts`
--

INSERT INTO `investment_due_accounts` (`id`, `u_id`, `investment_id`, `amount`, `actual_amount`, `generated_at`, `status`, `collected_at`) VALUES
(5, 27, 5, 1200.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(6, 28, 6, 850.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(7, 29, 7, 1500.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(8, 30, 8, 2000.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(9, 31, 9, 1200.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(10, 32, 10, 1800.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(11, 33, 11, 2200.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(12, 34, 12, 5000.00, NULL, '2018-12-10 22:00:31', 'scheduled', NULL),
(13, 27, 5, 1200.00, NULL, '2018-12-27 22:39:13', 'pending', NULL),
(14, 28, 6, 850.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(15, 29, 7, 1500.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(16, 30, 8, 2000.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(17, 31, 9, 1200.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(18, 32, 10, 1800.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(19, 33, 11, 2200.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(20, 34, 12, 5000.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(21, 35, 13, 1200.00, NULL, '2018-12-27 22:39:14', 'pending', NULL),
(22, 36, 14, 800.00, NULL, '2018-12-27 22:39:14', 'pending', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `investment_joins`
--

CREATE TABLE `investment_joins` (
  `id` int(11) NOT NULL,
  `guid` varchar(36) NOT NULL,
  `u_id` int(11) NOT NULL,
  `plan_amount` float(8,2) NOT NULL,
  `plan_label` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `status` enum('active','expired','suspended') NOT NULL,
  `reccoring_type` enum('day','week','month') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investment_joins`
--

INSERT INTO `investment_joins` (`id`, `guid`, `u_id`, `plan_amount`, `plan_label`, `created_at`, `valid_from`, `valid_true`, `status`, `reccoring_type`) VALUES
(5, 'A92DCA83-9F24-563A-FA45-D432F9986891', 27, 1200.00, 'Investment of 1200', '2018-12-10 20:56:49', '2018-12-10', '2023-12-10', 'active', 'week'),
(6, '89410990-1853-A40E-ABC8-CCFF933BDE9F', 28, 850.00, 'Investment of 850', '2018-12-10 20:58:32', '2018-12-10', '2023-12-10', 'active', 'week'),
(7, '81DB5009-7349-1436-3E46-B8F1FDE2B51F', 29, 1500.00, 'Investment of 1500', '2018-12-10 20:59:19', '2018-12-10', '2023-12-10', 'active', 'week'),
(8, '412DCE76-0743-EEF4-32AB-00CEE26154A7', 30, 2000.00, 'Investment of 2000', '2018-12-10 21:00:14', '2018-12-10', '2023-12-10', 'active', 'week'),
(9, 'E2C0A86B-21E2-9355-76D2-9AB63258EED8', 31, 1200.00, 'Investment of 1200', '2018-12-10 21:01:04', '2018-12-10', '2023-12-10', 'active', 'week'),
(10, 'C2EF7EFA-31A5-F62A-E5F7-993F6328D204', 32, 1800.00, 'Investment of 1800', '2018-12-10 21:02:01', '2018-12-10', '2023-12-10', 'active', 'week'),
(11, '63739389-4C91-0137-95F4-DC83544D704C', 33, 2200.00, 'Investment of 2200', '2018-12-10 21:02:50', '2018-12-10', '2023-12-10', 'active', 'week'),
(12, 'B4701CEE-18E3-BBD8-0737-AF8FE7AC1E74', 34, 5000.00, 'Investment of 5000', '2018-12-10 21:03:37', '2018-12-10', '2023-12-10', 'active', 'week'),
(13, '2429E524-4DB2-26B0-7205-31A2F8C1997D', 35, 1200.00, 'Investment of 1200', '2018-12-13 16:25:44', '2018-12-13', '2023-12-13', 'active', 'week'),
(14, '9809B643-1BF4-A2AE-D770-F227B476F23B', 36, 800.00, 'Investment of 800', '2018-12-13 16:27:37', '2018-12-13', '2023-12-13', 'active', 'week');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495384146),
('m130524_201442_init', 1495384148);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package_label` varchar(255) NOT NULL,
  `package_value` float(100,2) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_label`, `package_value`, `status`) VALUES
(25, 'Regular 5,000.00', 5000.00, '0'),
(26, 'Advanced 10,000.00', 10000.00, '0'),
(27, 'VIP 50,000.00', 50000.00, '0'),
(28, 'VIP Premium 1,000,000.00', 1000000.00, '0'),
(29, 'test pack', 1240.85, '0'),
(30, 'regular 1200', 1250.55, '0');

-- --------------------------------------------------------

--
-- Table structure for table `payment_offline`
--

CREATE TABLE `payment_offline` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `type` enum('c2b') NOT NULL,
  `payment_record_id` int(11) NOT NULL,
  `amount` float(8,2) NOT NULL,
  `gst` float(8,2) NOT NULL,
  `grand_total` float(8,2) NOT NULL,
  `discount` float(8,2) NOT NULL,
  `collected_amount` float(8,2) NOT NULL,
  `return_balance` float(8,2) NOT NULL DEFAULT '0.00',
  `balance_due` float(8,2) NOT NULL DEFAULT '0.00',
  `status` enum('success','partiality','init') NOT NULL,
  `collection_agent_id` int(11) NOT NULL,
  `collection_agent_note` text NOT NULL,
  `collection_agent_signature` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_online`
--

CREATE TABLE `payment_online` (
  `id` int(11) NOT NULL,
  `guid` varchar(255) NOT NULL,
  `type` enum('c2b') NOT NULL,
  `u_id` int(11) NOT NULL,
  `payment_record_id` int(11) NOT NULL,
  `amount` float(20,2) NOT NULL,
  `gst` float(20,2) NOT NULL,
  `grand_total` float(20,2) NOT NULL,
  `discount` float(20,2) NOT NULL,
  `status` enum('pending','success','suspend','rejected','offline') NOT NULL,
  `label` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `valid_true` date NOT NULL,
  `billing_mobile` varchar(10) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_first_name` varchar(255) DEFAULT NULL,
  `billing_last_name` varchar(255) DEFAULT NULL,
  `payment_gateway` enum('braintree','eWay','ccAvenue','PayPal') DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `payment_collected_at` datetime DEFAULT NULL,
  `card_number` varchar(20) DEFAULT NULL,
  `refund_status` enum('0','requested','success') DEFAULT NULL,
  `refund_amount` float(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_records`
--

CREATE TABLE `payment_records` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `scheme_join_id` int(11) NOT NULL,
  `billing_for` date NOT NULL,
  `amount` float(20,2) NOT NULL,
  `periode` text,
  `created_at` datetime NOT NULL,
  `collection_type` enum('online','offline','not_set') DEFAULT 'not_set',
  `collection_agent_id` int(11) DEFAULT NULL,
  `collection_agent_note` text,
  `online_payment_id` int(11) DEFAULT NULL,
  `offline_payment_id` int(11) DEFAULT NULL,
  `status` enum('pending','processing','success','suspended','init') NOT NULL DEFAULT 'pending',
  `payment_received_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periodes`
--

CREATE TABLE `periodes` (
  `id` int(11) NOT NULL,
  `months` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periodes`
--

INSERT INTO `periodes` (`id`, `months`, `status`, `created_at`, `updated_at`) VALUES
(24, 11, '0', '2017-08-01 22:57:59', '2017-08-01 22:57:59');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_joins`
--

CREATE TABLE `scheme_joins` (
  `id` int(11) NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `u_id` int(11) NOT NULL,
  `plan_amount` float(20,2) NOT NULL,
  `plan_label` varchar(300) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `total_investment` float(20,2) DEFAULT '0.00',
  `paid_duration` int(11) NOT NULL DEFAULT '0',
  `startup_payment` enum('paid','paid_updated') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_type` varchar(30) NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(11,2) DEFAULT NULL,
  `offer_valid_from` date NOT NULL,
  `offer_valid_true` date NOT NULL,
  `offer_availing_on` varchar(12) NOT NULL,
  `status` enum('active','expired','suspended') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scheme_offers`
--

CREATE TABLE `scheme_offers` (
  `id` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `offer_type` enum('month','cash','gold') NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(10,2) DEFAULT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_availing_on` enum('anytime','end_of') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_offers`
--

INSERT INTO `scheme_offers` (`id`, `status`, `offer_type`, `offer_label`, `bonus_month`, `bonus_cash`, `bonus_gold`, `valid_from`, `valid_true`, `offer_availing_on`, `created_at`, `updated_at`) VALUES
(12, '0', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:00:28', '2018-01-30 14:53:18'),
(13, '0', 'month', '2 Months Free Bonus', 2, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:00:57', '2017-08-01 23:00:57'),
(14, '0', 'cash', '6,894.99 Cash Bonus', NULL, 6894.99, NULL, '2017-08-01', '2018-12-31', 'anytime', '2017-08-01 23:02:03', '2017-08-01 23:02:03'),
(15, '0', 'cash', '8,599.00 Cash Bonus', NULL, 8599.00, NULL, '2017-08-01', '2018-12-31', 'anytime', '2017-08-01 23:02:48', '2017-08-01 23:02:48'),
(16, '0', 'gold', '8.00 grams Gold', NULL, NULL, 8.00, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:03:44', '2017-08-03 22:00:08');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `level` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) DEFAULT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` text COLLATE utf8_unicode_ci,
  `token_expires` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `display_name`, `l_name`, `f_name`, `user_type`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `image`, `status`, `created_at`, `updated_at`, `level`, `created_by`, `mobile`, `access_token`, `token_expires`) VALUES
(1, 'admin', 'ADMIN DEMO', 'Muhammed', 'muhsin', 'admin', '', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', NULL, 'muhsin.zyne@gmail.com', '', 10, 1487064707, 1487064707, 1, NULL, NULL, 'IjwuBwD-MUSEz5wW3IWNMBD3nEr9okLx', 1548331006),
(27, 'demo@27', NULL, 'Muhsin', 'Muhammed', 'user', 'O7DndB38YbvO0Ek9zraW2om3on1rmPdL', '$2y$13$eGAU8pbNlfbMwu58RE7T0uBHAaYVvwIV2Bv/8QCbeey73esHDIU5m', NULL, '8089435573@demo.everypay.local', '', 10, 1544455609, 1544455609, 4, 1, '8089435573', NULL, NULL),
(28, 'demo@28', NULL, 'Demmy', 'Demo 1', 'user', '8kp6TVWSJpstPQVF1b_0_ou058GbrZgz', '$2y$13$/B98VEboui8KqRQ/FH7axeru8.nJKAOivEzMm.re1.EzDYyFv.qGS', NULL, '8089435571@demo.everypay.local', '', 10, 1544455712, 1544455712, 4, 1, '8089435571', NULL, NULL),
(29, 'demo@29', NULL, 'Mark', 'Ajmal', 'user', 'vMQwsqkAL9_LA2_TnzlTDqrEmR2vmqFD', '$2y$13$kacsbiAcBul77wu1PbLeL.jLh19FQfT8V9Hq84JMxFQHQQknhd04u', NULL, '8089435574@demo.everypay.local', '', 10, 1544455759, 1544455759, 4, 1, '8089435574', NULL, NULL),
(30, 'demo@30', NULL, 'Doll', 'Michu', 'user', '-19dTu4MoSVioF44o4UcsnHoDlGCvSpy', '$2y$13$aUfWzwGgRezHrzzqgCOvxe2u8vXwjG1yEGjRLv84pXIckguAZqKDC', NULL, '9988776633@demo.everypay.local', '', 10, 1544455814, 1544455814, 4, 1, '9988776633', NULL, NULL),
(31, 'demo@31', NULL, 'Mohammed', 'Rashid', 'user', 'NNOQrxH-CxavQACY2DsaaoYmoTqIjYpD', '$2y$13$rDqEGfz1NMIlnm1SqVf63uFx6YHi1h0wN6kkSAE01zHndPrw3HYLK', NULL, '4555444444@demo.everypay.local', '', 10, 1544455864, 1544455864, 4, 1, '4555444444', NULL, NULL),
(32, 'demo@32', NULL, 'Movie', 'Anarkali', 'user', 'y8WARbFeQoUIT81etk7ahMS28aK7mlv2', '$2y$13$eySD5ExW./haAIRsHsXGVe4/XBalZ6DS9fDFlxOYncc25mah/nErC', NULL, '2111222111@demo.everypay.local', '', 10, 1544455921, 1544455921, 4, 1, '2111222111', NULL, NULL),
(33, 'demo@33', NULL, 'Robust', 'Mark', 'user', 'arJ_a_jw06OutyDCTUfoG9XF3w1gd473', '$2y$13$uud/7WyuFkcgjrjtKqDdGOeePqvAMsSqIYdDsmSRiDsj61U.UGlXK', NULL, '7356367322@demo.everypay.local', '', 10, 1544455970, 1544455970, 4, 1, '7356367322', NULL, NULL),
(34, 'demo@34', NULL, 'Afsal', 'Amal', 'user', '7yEVzZoBZju8xTEgZr0h_-WsqamSzxd2', '$2y$13$LGniIeXhsDt0w/O3TnzLjOtZacF78ien03N9bgz5FC8E/ts2w3llC', NULL, '5533332111@demo.everypay.local', '', 10, 1544456017, 1544456017, 4, 1, '5533332111', NULL, NULL),
(35, 'demo@35', NULL, 'Muhammed', 'Mishab', 'user', 'CSkRff-vWYeDehHeC8J4EvPgc2g5U6SO', '$2y$13$awp/yEKXXFv/6P.fF3IrpuwPpmfNNW8SVAXHkMIEJTcV5Xg7DEQWW', NULL, '7737377222@demo.everypay.local', '', 10, 1544698544, 1544698544, 4, 1, '7737377222', NULL, NULL),
(36, 'demo@36', NULL, 'K', 'Amal', 'user', 'RIVjvJu_0zJSF48m4q0TNWggEs30FUsa', '$2y$13$ud6eeGUyPTgxLA8/Am/Fa.2Ml69Z2mgAeja1XLQPlvtq2.p0QAybK', NULL, '5773333744@demo.everypay.local', '', 10, 1544698657, 1544698657, 4, 1, '5773333744', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_data`
--
ALTER TABLE `admin_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appointment_schedules`
--
ALTER TABLE `appointment_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b2c_address`
--
ALTER TABLE `b2c_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_generate_log`
--
ALTER TABLE `billing_generate_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collection_roots`
--
ALTER TABLE `collection_roots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_accounts`
--
ALTER TABLE `investment_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_bill_daily_log`
--
ALTER TABLE `investment_bill_daily_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_bill_weekly_log`
--
ALTER TABLE `investment_bill_weekly_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_due_accounts`
--
ALTER TABLE `investment_due_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_joins`
--
ALTER TABLE `investment_joins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_offline`
--
ALTER TABLE `payment_offline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_online`
--
ALTER TABLE `payment_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_records`
--
ALTER TABLE `payment_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periodes`
--
ALTER TABLE `periodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_data`
--
ALTER TABLE `admin_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `appointment_schedules`
--
ALTER TABLE `appointment_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `b2c_address`
--
ALTER TABLE `b2c_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `billing_generate_log`
--
ALTER TABLE `billing_generate_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collection_roots`
--
ALTER TABLE `collection_roots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `investment_accounts`
--
ALTER TABLE `investment_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment_bill_daily_log`
--
ALTER TABLE `investment_bill_daily_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment_bill_weekly_log`
--
ALTER TABLE `investment_bill_weekly_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `investment_due_accounts`
--
ALTER TABLE `investment_due_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `investment_joins`
--
ALTER TABLE `investment_joins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `payment_offline`
--
ALTER TABLE `payment_offline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_online`
--
ALTER TABLE `payment_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_records`
--
ALTER TABLE `payment_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `periodes`
--
ALTER TABLE `periodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
