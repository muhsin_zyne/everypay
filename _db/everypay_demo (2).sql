-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2018 at 10:12 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `everypay_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activity_data` text NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activity_data`, `location`) VALUES
(1, 'a new period created', 'packages'),
(2, 'Package updated', 'packages');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `log` text NOT NULL,
  `time` datetime NOT NULL,
  `model_refrence` varchar(55) NOT NULL,
  `log_refrence` int(10) NOT NULL,
  `logged_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_data`
--

CREATE TABLE `admin_data` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `signature` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_data`
--

INSERT INTO `admin_data` (`id`, `u_id`, `signature`) VALUES
(1, 1, '15104025535a06e9f951f452.78592916_sign.png');

-- --------------------------------------------------------

--
-- Table structure for table `b2c_address`
--

CREATE TABLE `b2c_address` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `type` enum('0','1') NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `pincode` varchar(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `b2c_address`
--

INSERT INTO `b2c_address` (`id`, `u_id`, `type`, `address`, `pincode`, `status`) VALUES
(8, 9, '0', 'muhammed@gmail.com', '679335', '0'),
(9, 10, '0', 'sdsdsdsds', '333333', '0'),
(10, 11, '0', 'sample full address', '679335', '0'),
(11, 12, '0', 'xdsdsds', '343434', '0');

-- --------------------------------------------------------

--
-- Table structure for table `billing_generate_log`
--

CREATE TABLE `billing_generate_log` (
  `id` int(11) NOT NULL,
  `exicuted_date` varchar(2) NOT NULL,
  `exicuted_month` varchar(2) NOT NULL,
  `exicuted_year` int(4) NOT NULL,
  `status` enum('success','faild','suspended') NOT NULL,
  `type` enum('payment') NOT NULL,
  `exicuted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `slug` text NOT NULL,
  `content` longtext NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495384146),
('m130524_201442_init', 1495384148);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package_label` varchar(255) NOT NULL,
  `package_value` float(100,2) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_label`, `package_value`, `status`) VALUES
(25, 'Regular 5,000.00', 5000.00, '0'),
(26, 'Advanced 10,000.00', 10000.00, '0'),
(27, 'VIP 50,000.00', 50000.00, '0'),
(28, 'VIP Premium 1,000,000.00', 1000000.00, '0'),
(29, 'test pack', 1240.85, '0'),
(30, 'regular 1200', 1250.55, '0');

-- --------------------------------------------------------

--
-- Table structure for table `payment_offline`
--

CREATE TABLE `payment_offline` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `type` enum('c2b') NOT NULL,
  `payment_record_id` int(11) NOT NULL,
  `amount` float(8,2) NOT NULL,
  `gst` float(8,2) NOT NULL,
  `grand_total` float(8,2) NOT NULL,
  `discount` float(8,2) NOT NULL,
  `collected_amount` float(8,2) NOT NULL,
  `return_balance` float(8,2) NOT NULL DEFAULT '0.00',
  `balance_due` float(8,2) NOT NULL DEFAULT '0.00',
  `status` enum('success','partiality','init') NOT NULL,
  `collection_agent_id` int(11) DEFAULT NULL,
  `collection_agent_note` text,
  `collection_agent_signature` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_online`
--

CREATE TABLE `payment_online` (
  `id` int(11) NOT NULL,
  `guid` varchar(255) NOT NULL,
  `type` enum('c2b') NOT NULL,
  `u_id` int(11) NOT NULL,
  `payment_record_id` int(11) NOT NULL,
  `amount` float(20,2) NOT NULL,
  `gst` float(20,2) NOT NULL,
  `grand_total` float(20,2) NOT NULL,
  `discount` float(20,2) NOT NULL,
  `status` enum('pending','success','suspend','rejected','offline') NOT NULL,
  `label` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `valid_true` date NOT NULL,
  `billing_mobile` varchar(10) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_first_name` varchar(255) DEFAULT NULL,
  `billing_last_name` varchar(255) DEFAULT NULL,
  `payment_gateway` enum('braintree','eWay','ccAvenue','PayPal') DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `payment_collected_at` datetime DEFAULT NULL,
  `card_number` varchar(20) DEFAULT NULL,
  `refund_status` enum('0','requested','success') DEFAULT NULL,
  `refund_amount` float(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_records`
--

CREATE TABLE `payment_records` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `scheme_join_id` int(11) NOT NULL,
  `billing_for` date NOT NULL,
  `amount` float(20,2) NOT NULL,
  `periode` text,
  `created_at` datetime NOT NULL,
  `collection_type` enum('online','offline','not_set') DEFAULT 'not_set',
  `collection_agent_id` int(11) DEFAULT NULL,
  `collection_agent_note` text,
  `online_payment_id` int(11) DEFAULT NULL,
  `offline_payment_id` int(11) DEFAULT NULL,
  `status` enum('pending','processing','success','suspended','init') NOT NULL DEFAULT 'pending',
  `payment_received_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_records`
--

INSERT INTO `payment_records` (`id`, `u_id`, `scheme_join_id`, `billing_for`, `amount`, `periode`, `created_at`, `collection_type`, `collection_agent_id`, `collection_agent_note`, `online_payment_id`, `offline_payment_id`, `status`, `payment_received_at`) VALUES
(1, 9, 1, '2018-01-29', 5000.00, '1/11', '2018-01-29 08:47:40', 'offline', NULL, 'advanced payment of first month', NULL, NULL, 'success', '2018-01-29 08:47:40');

-- --------------------------------------------------------

--
-- Table structure for table `periodes`
--

CREATE TABLE `periodes` (
  `id` int(11) NOT NULL,
  `months` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scheme_joins`
--

CREATE TABLE `scheme_joins` (
  `id` int(11) NOT NULL,
  `guid` varchar(36) DEFAULT NULL,
  `u_id` int(11) NOT NULL,
  `plan_amount` float(20,2) NOT NULL,
  `plan_label` varchar(300) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `total_investment` float(20,2) DEFAULT '0.00',
  `paid_duration` int(11) NOT NULL DEFAULT '0',
  `startup_payment` enum('paid','paid_updated') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_type` varchar(30) NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(11,2) DEFAULT NULL,
  `offer_valid_from` date NOT NULL,
  `offer_valid_true` date NOT NULL,
  `offer_availing_on` varchar(12) NOT NULL,
  `status` enum('active','expired','suspended') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_joins`
--

INSERT INTO `scheme_joins` (`id`, `guid`, `u_id`, `plan_amount`, `plan_label`, `duration`, `total_investment`, `paid_duration`, `startup_payment`, `created_at`, `valid_from`, `valid_true`, `offer_type`, `offer_label`, `bonus_month`, `bonus_cash`, `bonus_gold`, `offer_valid_from`, `offer_valid_true`, `offer_availing_on`, `status`) VALUES
(1, NULL, 9, 5000.00, 'Regular 5,000.00', 11, 5000.00, 1, 'paid_updated', '2018-01-29 08:21:59', '2018-01-29', '2019-01-29', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', 'active'),
(2, NULL, 9, 10000.00, 'Advanced 10,000.00', 11, 0.00, 0, 'paid', '2018-01-29 08:22:19', '2018-01-29', '2019-03-01', 'month', '2 Months Free Bonus', 2, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', 'active'),
(3, NULL, 10, 5000.00, 'Regular 5,000.00', 11, 0.00, 0, 'paid', '2018-01-29 08:22:48', '2018-01-29', '2019-01-29', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_offers`
--

CREATE TABLE `scheme_offers` (
  `id` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `offer_type` enum('month','cash','gold') NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(10,2) DEFAULT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_availing_on` enum('anytime','end_of') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_offers`
--

INSERT INTO `scheme_offers` (`id`, `status`, `offer_type`, `offer_label`, `bonus_month`, `bonus_cash`, `bonus_gold`, `valid_from`, `valid_true`, `offer_availing_on`, `created_at`, `updated_at`) VALUES
(12, '0', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:00:28', '2017-08-01 23:00:28'),
(13, '0', 'month', '2 Months Free Bonus', 2, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:00:57', '2017-08-01 23:00:57'),
(14, '0', 'cash', '6,894.99 Cash Bonus', NULL, 6894.99, NULL, '2017-08-01', '2018-12-31', 'anytime', '2017-08-01 23:02:03', '2017-08-01 23:02:03'),
(15, '0', 'cash', '8,599.00 Cash Bonus', NULL, 8599.00, NULL, '2017-08-01', '2018-12-31', 'anytime', '2017-08-01 23:02:48', '2017-08-01 23:02:48'),
(16, '0', 'gold', '8.00 grams Gold', NULL, NULL, 8.00, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:03:44', '2017-08-03 22:00:08');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `level` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) DEFAULT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `display_name`, `l_name`, `f_name`, `user_type`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `image`, `status`, `created_at`, `updated_at`, `level`, `created_by`, `mobile`) VALUES
(1, 'admin', 'ADMIN DEMO', 'Muhammed', 'muhsin', 'admin', '', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', NULL, 'muhsin.zyne@gmail.com', '', 10, 1487064707, 1487064707, 1, NULL, NULL),
(9, 'demo@9', NULL, 'MUHSIN', 'MUHAMMED', 'user', 'z0r0hCsjdfXg5XVKbeQuoJf3fTIjWFb8', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', NULL, 'muhsin@gmail.com', '', 10, 1504537071, 1504537071, 4, 1, '8089435573'),
(10, 'demo@10', NULL, 'dsd', 'dsds', 'user', 'jqNMji17U987f5-Ql3tIGtPme2joo_Oi', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', NULL, 'sdsdsddsdsddddsd@gmail.cim', '', 10, 1504610827, 1504610827, 4, 1, '7067670706'),
(11, 'demo@11', NULL, 'Mothirapeed', 'Bareera', 'user', 'mlutIqYOVeOOTia9JrH1FMYnBAIQjU77', '$2y$13$VjIwd2CvOC2YpxsyDeGrv.om0Ke37e/JXLjSwJfzrIzcRCqyckk5i', NULL, 'bareera.zyne@gmail.com', '', 10, 1509641144, 1509641144, 4, 1, '9600392238'),
(12, 'demo@12', NULL, 'dsdsd', 'dsdsdsdsds', 'user', '8iIRBX-W5KiNiIcME_pm39lGkuRfII6v', '$2y$13$P42UGTsKM4hqtywPnp3DdO5ofgazTcGXKUE9o9bbiwuoEaKJFELD6', NULL, 'dsdsds@gmail.com', '', 10, 1510682968, 1510682968, 4, 1, '8895555555');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_data`
--
ALTER TABLE `admin_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b2c_address`
--
ALTER TABLE `b2c_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing_generate_log`
--
ALTER TABLE `billing_generate_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_offline`
--
ALTER TABLE `payment_offline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_online`
--
ALTER TABLE `payment_online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_records`
--
ALTER TABLE `payment_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periodes`
--
ALTER TABLE `periodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_data`
--
ALTER TABLE `admin_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `b2c_address`
--
ALTER TABLE `b2c_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `billing_generate_log`
--
ALTER TABLE `billing_generate_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `payment_offline`
--
ALTER TABLE `payment_offline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_online`
--
ALTER TABLE `payment_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_records`
--
ALTER TABLE `payment_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `periodes`
--
ALTER TABLE `periodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
