-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2017 at 06:18 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `everypay_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activity_data` text NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activity_data`, `location`) VALUES
(1, 'a new period created', 'packages'),
(2, 'Package updated', 'packages');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `log` text NOT NULL,
  `time` datetime NOT NULL,
  `model_refrence` varchar(55) NOT NULL,
  `log_refrence` int(10) NOT NULL,
  `logged_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log`, `time`, `model_refrence`, `log_refrence`, `logged_by`) VALUES
(1, 'a new period created', '2017-07-26 22:38:42', 'Periodes', 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `b2c_address`
--

CREATE TABLE `b2c_address` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `type` enum('0','1') NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `pincode` varchar(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `b2c_address`
--

INSERT INTO `b2c_address` (`id`, `u_id`, `type`, `address`, `pincode`, `status`) VALUES
(2, 62, '0', 'MOTHIRAPPEEDIKAKKAL HOUSE MARAYAMANGALAM POST ', '679335', '0'),
(3, 63, '0', 'mo mo mo', '123456', '0'),
(4, 65, '0', 'sdsd', '123456', '0'),
(5, 67, '0', 'sdsd', '123456', '0'),
(6, 68, '0', 'sdsds', '123456', '0');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1495384146),
('m130524_201442_init', 1495384148);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `package_label` varchar(255) NOT NULL,
  `package_value` float(100,2) NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_label`, `package_value`, `status`) VALUES
(25, 'Regular 5,000.00', 5000.00, '0'),
(26, 'Advanced 10,000.00', 10000.00, '0'),
(27, 'VIP 50,000.00', 50000.00, '0'),
(28, 'VIP Premium 1,000,000.00', 1000000.00, '0');

-- --------------------------------------------------------

--
-- Table structure for table `periodes`
--

CREATE TABLE `periodes` (
  `id` int(11) NOT NULL,
  `months` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periodes`
--

INSERT INTO `periodes` (`id`, `months`, `status`, `created_at`, `updated_at`) VALUES
(24, 11, '0', '2017-08-01 22:57:59', '2017-08-01 22:57:59'),
(25, 22, '0', '2017-08-01 22:58:10', '2017-08-01 22:58:10');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_joins`
--

CREATE TABLE `scheme_joins` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `plan_amount` float(20,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_type` varchar(30) NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(11,2) DEFAULT NULL,
  `offer_valid_from` date NOT NULL,
  `offer_valid_true` date NOT NULL,
  `offer_availing_on` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_joins`
--

INSERT INTO `scheme_joins` (`id`, `u_id`, `plan_amount`, `duration`, `created_at`, `valid_from`, `valid_true`, `offer_type`, `offer_label`, `bonus_month`, `bonus_cash`, `bonus_gold`, `offer_valid_from`, `offer_valid_true`, `offer_availing_on`) VALUES
(2, 62, 5000.00, 11, '2017-08-01 23:25:17', '2017-08-01', '2018-08-01', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of'),
(3, 63, 5000.00, 11, '2017-08-01 23:39:05', '2017-08-01', '2018-08-01', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of'),
(4, 65, 5000.00, 11, '2017-08-01 23:59:19', '2017-08-01', '2018-08-01', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of'),
(5, 67, 5000.00, 11, '2017-08-02 00:02:20', '2017-08-01', '2018-08-01', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of'),
(6, 68, 5000.00, 11, '2017-08-02 00:05:34', '2017-08-02', '2018-08-02', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_offers`
--

CREATE TABLE `scheme_offers` (
  `id` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `offer_type` enum('month','cash','gold') NOT NULL,
  `offer_label` varchar(50) NOT NULL,
  `bonus_month` int(11) DEFAULT NULL,
  `bonus_cash` float(11,2) DEFAULT NULL,
  `bonus_gold` float(10,2) DEFAULT NULL,
  `valid_from` date NOT NULL,
  `valid_true` date NOT NULL,
  `offer_availing_on` enum('anytime','end_of') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_offers`
--

INSERT INTO `scheme_offers` (`id`, `status`, `offer_type`, `offer_label`, `bonus_month`, `bonus_cash`, `bonus_gold`, `valid_from`, `valid_true`, `offer_availing_on`, `created_at`, `updated_at`) VALUES
(12, '0', 'month', '1 Month Free Bonus', 1, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:00:28', '2017-08-01 23:00:28'),
(13, '0', 'month', '2 Months Free Bonus', 2, NULL, NULL, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:00:57', '2017-08-01 23:00:57'),
(14, '0', 'cash', '6,894.99 Cash Bonus', NULL, 6894.99, NULL, '2017-08-01', '2018-12-31', 'anytime', '2017-08-01 23:02:03', '2017-08-01 23:02:03'),
(15, '0', 'cash', '8,599.00 Cash Bonus', NULL, 8599.00, NULL, '2017-08-01', '2018-12-31', 'anytime', '2017-08-01 23:02:48', '2017-08-01 23:02:48'),
(16, '0', 'gold', '8.00 grams Gold', NULL, NULL, 80.15, '2017-08-01', '2020-12-31', 'end_of', '2017-08-01 23:03:44', '2017-08-01 23:03:44'),
(17, '0', 'month', '11', 1, NULL, NULL, '2017-08-01', '2017-08-29', 'end_of', '2017-08-01 23:46:30', '2017-08-01 23:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` enum('admin','user') COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `level` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) DEFAULT NULL,
  `mobile` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `display_name`, `l_name`, `f_name`, `user_type`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `image`, `status`, `created_at`, `updated_at`, `level`, `created_by`, `mobile`) VALUES
(1, 'admin', 'Muhammed Muhsin', NULL, NULL, 'admin', '4gy_jvkWiDIQmT79sk_D-rEy4LBQu1sR', '$2y$13$R.59U/mgX4buFAV7DEUhV.4n75WF0kH1Ie/kAuJj57SLkj0bM1USy', 'z2poo7YCYgTjjEJW2gqQzprNyeoovUkZ_1500817285', 'muhsin.zyne@gmail.com', '', 10, 1487064707, 1500817285, 1, NULL, NULL),
(62, 'demo@62', NULL, 'muhsin', 'muhammed', 'user', 'qVXozi08kiDNJL9ofIRHz2vgR7S7DGl7', '$2y$13$dMFxvEAUn2NELD29NlpsbOC1ss7fu6ijBHfyugBxg/sbw6i8rogi6', NULL, 'muhsin@gmail.com', '', 10, 1501610117, 1501610117, 4, 1, '8089435573'),
(63, 'demo@63', NULL, 'las', 'Limsha', 'user', 'u_r5Jmav4JgzBOSLaiUWVub8UksWsq5k', '$2y$13$bhSY4ZH7cO7.z.pnEEzQlOvfwNykOSYN7aN1bVqhCXZ/Spnuoor6.', NULL, 'limshalas@gmail.com', '', 10, 1501610944, 1501610945, 4, 1, '8089435573'),
(65, 'demo@65', NULL, 'dsdsds', 'muhammed', 'user', 'OOxI4HuTZ1nbnVZGxVHlQsL8n7wkRUD1', '$2y$13$bwu5cWqHOXBR2RT4YRQ9b.7UamalfxoXQbbISlwQ.DbG..DNXJ4nC', NULL, 'testemail@gmail.com', '', 10, 1501612159, 1501612159, 4, 1, '1234567890'),
(67, 'demo@67', NULL, 'dsdsds', 'muhammed', 'user', 'nsYaVxubT7cWYdyhZKCSuif5X8Fj3BMU', '$2y$13$qAdZsSqDCIrP/8fB7BJNF.VF4VI/TWw/flAoYS0XTJ9Bv2.Zjs0ha', NULL, 'testemsssail@gmail.com', '', 10, 1501612339, 1501612339, 4, 1, '1234567890'),
(68, 'demo@68', NULL, 'HA', 'MUHAMMD', 'user', '0H6wuXDHnKV1sDchjihp1L1c7qYsCtxw', '$2y$13$ODANHoZZlT9XJQcivrnJMuf1HilpQHIQ58qe2Wn7TmYdi/aBcc8s2', NULL, 'dubaii@gmail.com', '', 10, 1501612533, 1501567730, 4, 1, '8089435573');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `b2c_address`
--
ALTER TABLE `b2c_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periodes`
--
ALTER TABLE `periodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `b2c_address`
--
ALTER TABLE `b2c_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `periodes`
--
ALTER TABLE `periodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `scheme_joins`
--
ALTER TABLE `scheme_joins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `scheme_offers`
--
ALTER TABLE `scheme_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
